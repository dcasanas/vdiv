// add_9.sv: A "patched-up" version of add_8 to accomodate an extra bit.
// Used in 8-bit integer division.

module add_9
#(
	parameter P_LEN = 9
)
(
	input 	logic [P_LEN - 1:0] 	op_a,
	input 	logic [P_LEN - 1:0] 	op_b,
	input 	logic					in_c,

	output 	logic [P_LEN - 1:0] 	sum,
	output 	logic 					out_c
);

	logic out_c_w;

	assign sum[8] 	= op_a[8] ^ op_b[8] ^ out_c_w;
	assign out_c 	= (op_a[8] & op_b[8]) | out_c_w & (op_a[8] ^ op_b[8]);

	add_8 add_8_inst
	(
		.source_element_0 	(op_a[7:0]),
		.source_element_1 	(op_b[7:0]),
		.input_carry 		(in_c),
	
		.target_element 	(sum[7:0]),
		.output_carry 		(out_c_w)	
	);

endmodule 
