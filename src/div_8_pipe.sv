// div_8_pipe: Pipelined, 8-bit divisor.
// Author: Daniel Casañas.

// clk: Main clock.
// nrst: Active-low reset signal.
// dnd: Dividend.
// div: Divisor.
// sng: Signed or unsigned signal (1 = signed, 0 = unsigned).
// q: Quotient.
// r: Remainder.

module div_8_pipe
(
	input 	logic 		clk,
	input 	logic		nrst,

	input 	logic [7:0] dnd,
	input 	logic [7:0] div,
	input 	logic 		sgn,

	output 	logic [7:0] q,
	output 	logic [7:0] r
);
	
	// Resultant q and r buses.
	logic [7:0] q_w;
	logic [7:0] r_w;
	
	// 2's complement versions of the input operands.
	logic [7:0] dnd_2c;
	logic [7:0] div_2c;
	
	// 2's complement versions of q and r.
	logic [7:0] q_2c;
	logic [7:0] r_2c;

	// Double-width version of dnd with an extra bit for the sign.
	logic [16:0] dnd_eff;
	
	// Divisor with an extra bit for the sign.
	logic [8:0] div_eff;

	// Convert operands to 2's complement.
	assign dnd_2c = ~dnd + 8'd1;
	assign div_2c = ~div + 8'd1;

	// Convert q and r to their 2's complement versions.
	assign q_2c = ~q_w + 8'd1;
	assign r_2c = ~r_w + 8'd1;

	// Zero-extend the effective operands' containers.
	assign dnd_eff[16:8] 	= '0;
	assign div_eff[8]		= '0;

	// Select between normal and 2's complement versions of the input
	// operands.
	// If we need signed division and the operands are negative, let
	// through a 2's complement version of the operands, thus the
	// division process deals with unsigned numbers in every instance.
	// q and r will be converted accordingly when the process is done.
	assign dnd_eff[7:0] = ((sgn && dnd[7]) ? dnd_2c : dnd);
	assign div_eff[7:0] = ((sgn && div[7]) ? div_2c : div);
	
	// Assign q and r's final values according to the input operands'
	// original signs and sgn.
	assign q = (sgn && (dnd[7] ^ div[7]) ? q_2c : q_w);
	assign r = (sgn && dnd[7] ? r_2c : r_w);
	
	// Module instances:
	// Pipelined, non-restoring, 8-bit division algorithm implementation.
	nrdiv nrdiv_inst
	(
		.clk 	(clk),
		.nrst 	(nrst),
		
		.a 		(dnd_eff),
		.b 		(div_eff),
		
		.q 		(q_w),
		.r 		(r_w)
	);

endmodule
