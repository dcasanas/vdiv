// nrdiv_step: Implements the common 2A +/- 1 step, present in the
// non-restoring division algorithm for 8-bit operands.

// a_part: Partial, double-width dividend.
// b_part: Partial, single-width divisor.
// add_sub: Signals if addition or subtraction is needed at this stage.
//			It also serves as a carry input signal.
// rq_part: Concatenation of partial remainder and quotient.
// nxt_add_sub: Signals the next add-sub operation.

module nrdiv_step
#(
	parameter IP_LEN 	= 8,
	parameter OP_LEN 	= IP_LEN * 2
)
(
	input 	logic [IP_LEN * 2:0] 	a_part,
	input 	logic [IP_LEN:0] 		b_part,
	input 	logic 					add_sub, 
	
	output 	logic [OP_LEN:0] 		rq_part,
	output 	logic 					nxt_add_sub
);

	// Container for 2A
	logic [(IP_LEN * 2):0] 	ax2;	

	// Container for B or B's 1's complement
	logic [IP_LEN:0] 		b_final;

	// Container for the partial remainder
	logic [IP_LEN:0] 		r_part;	
	
	// Implement 2A
	assign ax2 = a_part << 1;
	
	// Select B's proper version for addition or subtraction.
	assign b_final = (add_sub ? ~b_part : b_part);
	
	// Form rq_part by performing the algorithm's final step.
	// rp_part's least-significant-bit depends on r_part's sign.
	assign rq_part = {r_part, ax2[7:1], ~r_part[8]};
	
	// Decide the next operation by watching r_part's sign.
	assign nxt_add_sub = ~r_part[8];

	// Module instances.
	// 9-bit adder.
	add_9 add_9_inst
	(
		.op_a 	(ax2[16:8]),
		.op_b 	(b_final),
		.in_c 	(add_sub),
		
		.sum 	(r_part),
		.out_c 	()
	);
	
endmodule
