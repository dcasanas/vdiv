// nrdiv.sv: 8-Bit, pipelined implementation of the Non-Restoring
// Division algorithm.
// Author: Daniel Casañas.

module nrdiv
#(
	parameter P_LEN 		= 8,
	parameter N_INST 		= 8
)
(
	input 	logic 					clk,
	input 	logic 					nrst,
	
	input 	logic [(P_LEN * 2):0] 	a,
	input 	logic [P_LEN:0] 		b,
	
	output 	logic [P_LEN - 1:0] 	q,
	output 	logic [P_LEN - 1:0] 	r 
);
	
	logic [(P_LEN * 2):0] 	rq_part_ff 		[N_INST - 1:0];
	logic [P_LEN :0] 		b_ff			[N_INST - 1:0];
	logic [N_INST - 1:0] 	nxt_add_sub_ff;
	
	logic [(P_LEN * 2):0] 	rq_part_w 		[N_INST - 1:0];
	logic [N_INST - 1:0] 	nxt_add_sub_w;
	
	// Bus for r's correction factor (b has to be added to r if negative)
	logic [P_LEN:0] 		r_corr_w;
	
	logic [P_LEN:0]			r_w;
	
	// Take the proper part of the very last bus as the final q.
	assign q = rq_part_w[7][7:0];
	assign r = r_w[7:0];
	
	// If r's sign is negative, let through b for its correction.
	assign r_corr_w = (rq_part_w[7][16] ? b_ff[7] : '0);
	
	// Manage inter-stage ff's
	always_ff @ (posedge clk, negedge nrst)
		if (!nrst)
			for (int i = 0; i < N_INST; i++)
				begin
					rq_part_ff[i] 		<= '0;
					nxt_add_sub_ff[i] 	<= '0;
					b_ff[i] 			<= '0;
				end
				
		else
			begin
				rq_part_ff[0] 		<= a;
				b_ff[0] 			<= b;
				nxt_add_sub_ff[0] 	<= 1'b1;
			
				for (int i = 1; i < N_INST; i++)
					begin
						rq_part_ff[i] 		<= rq_part_w[i - 1];
						b_ff[i]				<= b_ff[i - 1];
						nxt_add_sub_ff[i] 	<= nxt_add_sub_w[i - 1];
					end
			end
	
	// Instantiate nrdiv_step modules iteratively.
	generate
		genvar i;
		
		for (i = 0; i < N_INST; i++)
			begin : gen_loop
				nrdiv_step nrdiv_step_gen
				(
					.a_part 		(rq_part_ff[i]),
					.b_part 		(b_ff[i]),
					.add_sub 		(nxt_add_sub_ff[i]),
					
					.rq_part 		(rq_part_w[i]),
					.nxt_add_sub 	(nxt_add_sub_w[i])
				);
			end
	endgenerate
	
	// 9-bit adder instance for remainder correction.
	add_9 add_9_inst_0
	(
		.op_a 	(rq_part_w[7][16:8]),
		.op_b 	(r_corr_w),
		.in_c 	(1'b0),
		
		.sum 	(r_w),
		.out_c 	()
	);
	
endmodule
