// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See nrdiv.h for the primary calling header

#include "verilated.h"

#include "nrdiv__Syms.h"
#include "nrdiv_nrdiv_step.h"

void nrdiv_nrdiv_step___ctor_var_reset(nrdiv_nrdiv_step* vlSelf);

nrdiv_nrdiv_step::nrdiv_nrdiv_step(nrdiv__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    nrdiv_nrdiv_step___ctor_var_reset(this);
}

void nrdiv_nrdiv_step::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

nrdiv_nrdiv_step::~nrdiv_nrdiv_step() {
}
