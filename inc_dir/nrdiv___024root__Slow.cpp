// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See nrdiv.h for the primary calling header

#include "verilated.h"

#include "nrdiv__Syms.h"
#include "nrdiv___024root.h"

void nrdiv___024root___ctor_var_reset(nrdiv___024root* vlSelf);

nrdiv___024root::nrdiv___024root(nrdiv__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    nrdiv___024root___ctor_var_reset(this);
}

void nrdiv___024root::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

nrdiv___024root::~nrdiv___024root() {
}
