// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See nrdiv.h for the primary calling header

#include "verilated.h"

#include "nrdiv__Syms.h"
#include "nrdiv_nrdiv_step.h"

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((1U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [0U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [0U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [0U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [0U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [0U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [0U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [0U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [0U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [0U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [0U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [0U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                    & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          | ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42))) 
                                                         << 4U)))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [0U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                  & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                              << 8U)))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [0U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((2U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [1U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [1U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [1U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [1U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [1U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [1U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [1U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [1U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [1U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [1U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [1U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 1U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 1U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 1U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0xfffffff0U 
                                                            & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                                << 3U) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [1U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 7U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [1U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((4U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [2U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [2U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [2U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [2U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [2U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [2U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [2U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [2U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [2U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [2U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [2U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 2U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 2U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 2U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0xfffffff0U 
                                                            & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                                << 2U) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [2U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 6U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [2U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((8U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [3U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [3U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [3U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [3U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [3U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [3U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [3U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [3U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [3U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [3U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [3U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 3U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 3U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 3U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0xfffffff0U 
                                                            & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                                << 1U) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [3U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 5U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [3U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((0x10U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [4U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [4U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [4U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [4U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [4U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [4U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [4U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [4U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [4U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [4U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [4U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 4U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 4U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 4U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0xfffffff0U 
                                                            & ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [4U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 4U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [4U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((0x20U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [5U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [5U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [5U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [5U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [5U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [5U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [5U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [5U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [5U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [5U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [5U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 5U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 5U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 5U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0x7ffffff0U 
                                                            & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                                >> 1U) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [5U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 3U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [5U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((0x40U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [6U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [6U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [6U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [6U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [6U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [6U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [6U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [6U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [6U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [6U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [6U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 6U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 6U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 6U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0x3ffffff0U 
                                                            & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                                >> 2U) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [6U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 2U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [6U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}

VL_INLINE_OPT void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+        nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0\n"); );
    // Body
    vlSelf->__PVT__b_final = (0x1ffU & ((0x80U & (IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff))
                                         ? (~ vlSymsp->TOP.nrdiv__DOT__b_ff
                                            [7U]) : 
                                        vlSymsp->TOP.nrdiv__DOT__b_ff
                                        [7U]));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                     [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                        [7U] >> 7U) & (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((0x1ffU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [7U] >> 7U)) ^ (IData)(vlSelf->__PVT__b_final))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0x1feU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [7U] >> 7U)) ^ (0xfffffffeU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0x1fcU & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [7U] >> 7U)) ^ (0xfffffffcU 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0x1f8U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                               [7U] >> 7U)) ^ (0xfffffff8U 
                                               & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0x1f0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [7U] >> 7U)) ^ (0xfffffff0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0x1e0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [7U] >> 7U)) ^ (0xffffffe0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0x1c0U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [7U] >> 7U)) ^ (0xffffffc0U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0x180U & (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                  [7U] >> 7U)) ^ (0xffffff80U 
                                                  & (IData)(vlSelf->__PVT__b_final)))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 
        = (1U & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                 | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                     >> 7U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 
        = (IData)((3U == (3U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
           | (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
               >> 7U) & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21) 
           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                          << 2U) | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                     << 1U) | (1U & 
                                               ((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                >> 7U))))));
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                                        & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                                           & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                          << 4U) 
                                                         | (0x1ffffff0U 
                                                            & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                                >> 3U) 
                                                               & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                                  << 4U))))))));
    vlSelf->__PVT__r_part = ((0x100U & (((0x100U & 
                                          (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                           [7U] >> 7U)) 
                                         ^ (0xffffff00U 
                                            & (IData)(vlSelf->__PVT__b_final))) 
                                        ^ ((((0xffffff00U 
                                              & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  << 1U) 
                                                 | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     << 2U) 
                                                    & ((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                       << 1U)))) 
                                             | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                 & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                                                << 8U)) 
                                            | (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                               << 8U)) 
                                           | (0xffffff00U 
                                              & (((IData)(vlSymsp->TOP.nrdiv__DOT__nxt_add_sub_ff) 
                                                  << 1U) 
                                                 & (((IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                     & (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                                    << 8U)))))) 
                             | (0xffU & (((0x1ffU & 
                                           (vlSymsp->TOP.nrdiv__DOT__rq_part_ff
                                            [7U] >> 7U)) 
                                          ^ (IData)(vlSelf->__PVT__b_final)) 
                                         ^ (IData)(vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry))));
}
