// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See nrdiv.h for the primary calling header

#ifndef VERILATED_NRDIV___024ROOT_H_
#define VERILATED_NRDIV___024ROOT_H_  // guard

#include "verilated.h"

class nrdiv__Syms;
class nrdiv_nrdiv_step;


class nrdiv___024root final : public VerilatedModule {
  public:
    // CELLS
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step* __PVT__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen;

    // DESIGN SPECIFIC STATE
    VL_IN8(clk,0,0);
    VL_IN8(nrst,0,0);
    VL_OUT8(q,7,0);
    VL_OUT8(r,7,0);
    CData/*7:0*/ nrdiv__DOT__nxt_add_sub_ff;
    CData/*7:0*/ nrdiv__DOT__nxt_add_sub_w;
    CData/*7:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry;
    CData/*7:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g;
    CData/*7:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82;
    CData/*0:0*/ nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current;
    CData/*0:0*/ __Vclklast__TOP__clk;
    CData/*0:0*/ __Vclklast__TOP__nrst;
    VL_IN16(b,8,0);
    SData/*8:0*/ nrdiv__DOT__r_corr_w;
    SData/*8:0*/ nrdiv__DOT__r_w;
    VL_IN(a,16,0);
    IData/*31:0*/ nrdiv__DOT__unnamedblk2__DOT__i;
    IData/*31:0*/ nrdiv__DOT__unnamedblk1__DOT__i;
    VlUnpacked<IData/*16:0*/, 8> nrdiv__DOT__rq_part_ff;
    VlUnpacked<SData/*8:0*/, 8> nrdiv__DOT__b_ff;
    VlUnpacked<IData/*16:0*/, 8> nrdiv__DOT__rq_part_w;
    VlUnpacked<CData/*0:0*/, 2> __Vm_traceActivity;

    // INTERNAL VARIABLES
    nrdiv__Syms* const vlSymsp;

    // CONSTRUCTORS
    nrdiv___024root(nrdiv__Syms* symsp, const char* name);
    ~nrdiv___024root();
    VL_UNCOPYABLE(nrdiv___024root);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
