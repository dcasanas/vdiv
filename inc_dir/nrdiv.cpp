// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Model implementation (design independent parts)

#include "nrdiv.h"
#include "nrdiv__Syms.h"
#include "verilated_vcd_c.h"

//============================================================
// Constructors

nrdiv::nrdiv(VerilatedContext* _vcontextp__, const char* _vcname__)
    : VerilatedModel{*_vcontextp__}
    , vlSymsp{new nrdiv__Syms(contextp(), _vcname__, this)}
    , clk{vlSymsp->TOP.clk}
    , nrst{vlSymsp->TOP.nrst}
    , q{vlSymsp->TOP.q}
    , r{vlSymsp->TOP.r}
    , b{vlSymsp->TOP.b}
    , a{vlSymsp->TOP.a}
    , __PVT__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen}
    , __PVT__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen}
    , rootp{&(vlSymsp->TOP)}
{
    // Register model with the context
    contextp()->addModel(this);
}

nrdiv::nrdiv(const char* _vcname__)
    : nrdiv(Verilated::threadContextp(), _vcname__)
{
}

//============================================================
// Destructor

nrdiv::~nrdiv() {
    delete vlSymsp;
}

//============================================================
// Evaluation loop

void nrdiv___024root___eval_initial(nrdiv___024root* vlSelf);
void nrdiv___024root___eval_settle(nrdiv___024root* vlSelf);
void nrdiv___024root___eval(nrdiv___024root* vlSelf);
#ifdef VL_DEBUG
void nrdiv___024root___eval_debug_assertions(nrdiv___024root* vlSelf);
#endif  // VL_DEBUG
void nrdiv___024root___final(nrdiv___024root* vlSelf);

static void _eval_initial_loop(nrdiv__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    nrdiv___024root___eval_initial(&(vlSymsp->TOP));
    // Evaluate till stable
    vlSymsp->__Vm_activity = true;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Initial loop\n"););
        nrdiv___024root___eval_settle(&(vlSymsp->TOP));
        nrdiv___024root___eval(&(vlSymsp->TOP));
    } while (0);
}

void nrdiv::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate nrdiv::eval_step\n"); );
#ifdef VL_DEBUG
    // Debug assertions
    nrdiv___024root___eval_debug_assertions(&(vlSymsp->TOP));
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    vlSymsp->__Vm_activity = true;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        nrdiv___024root___eval(&(vlSymsp->TOP));
    } while (0);
    // Evaluate cleanup
}

//============================================================
// Utilities

const char* nrdiv::name() const {
    return vlSymsp->name();
}

//============================================================
// Invoke final blocks

VL_ATTR_COLD void nrdiv::final() {
    nrdiv___024root___final(&(vlSymsp->TOP));
}

//============================================================
// Implementations of abstract methods from VerilatedModel

const char* nrdiv::hierName() const { return vlSymsp->name(); }
const char* nrdiv::modelName() const { return "nrdiv"; }
unsigned nrdiv::threads() const { return 1; }
std::unique_ptr<VerilatedTraceConfig> nrdiv::traceConfig() const {
    return std::unique_ptr<VerilatedTraceConfig>{new VerilatedTraceConfig{false, false, false}};
};

//============================================================
// Trace configuration

void nrdiv___024root__trace_init_top(nrdiv___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD static void trace_init(void* voidSelf, VerilatedVcd* tracep, uint32_t code) {
    // Callback from tracep->open()
    nrdiv___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<nrdiv___024root*>(voidSelf);
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (!vlSymsp->_vm_contextp__->calcUnusedSigs()) {
        VL_FATAL_MT(__FILE__, __LINE__, __FILE__,
            "Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    }
    vlSymsp->__Vm_baseCode = code;
    tracep->scopeEscape(' ');
    tracep->pushNamePrefix(std::string{vlSymsp->name()} + ' ');
    nrdiv___024root__trace_init_top(vlSelf, tracep);
    tracep->popNamePrefix();
    tracep->scopeEscape('.');
}

VL_ATTR_COLD void nrdiv___024root__trace_register(nrdiv___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD void nrdiv::trace(VerilatedVcdC* tfp, int levels, int options) {
    if (false && levels && options) {}  // Prevent unused
    tfp->spTrace()->addModel(this);
    tfp->spTrace()->addInitCb(&trace_init, &(vlSymsp->TOP));
    nrdiv___024root__trace_register(&(vlSymsp->TOP), tfp->spTrace());
}
