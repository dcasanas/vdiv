// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table implementation internals

#include "nrdiv__Syms.h"
#include "nrdiv.h"
#include "nrdiv___024root.h"
#include "nrdiv_nrdiv_step.h"

// FUNCTIONS
nrdiv__Syms::~nrdiv__Syms()
{
}

nrdiv__Syms::nrdiv__Syms(VerilatedContext* contextp, const char* namep, nrdiv* modelp)
    : VerilatedSyms{contextp}
    // Setup internal state of the Syms class
    , __Vm_modelp{modelp}
    // Setup module instances
    , TOP{this, namep}
    , TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[0].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[1].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[2].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[3].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[4].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[5].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[6].nrdiv_step_gen")}
    , TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen{this, Verilated::catName(namep, "nrdiv.gen_loop[7].nrdiv_step_gen")}
{
    // Configure time unit / time precision
    _vm_contextp__->timeunit(-12);
    _vm_contextp__->timeprecision(-12);
    // Setup each module's pointers to their submodules
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen;
    TOP.__PVT__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen = &TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen;
    // Setup each module's pointer back to symbol table (for public functions)
    TOP.__Vconfigure(true);
    TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__Vconfigure(true);
    TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
    TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
    TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
    TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
    TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
    TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
    TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__Vconfigure(false);
}
