// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See div_8_pipe.h for the primary calling header

#ifndef VERILATED_DIV_8_PIPE_NRDIV_STEP_H_
#define VERILATED_DIV_8_PIPE_NRDIV_STEP_H_  // guard

#include "verilated.h"

class div_8_pipe__Syms;

class div_8_pipe_nrdiv_step final : public VerilatedModule {
  public:

    // DESIGN SPECIFIC STATE
    VL_IN8(add_sub,0,0);
    VL_OUT8(nxt_add_sub,0,0);
    CData/*7:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry;
    CData/*7:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g;
    CData/*7:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42;
    CData/*0:0*/ __PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82;
    CData/*0:0*/ add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current;
    CData/*0:0*/ add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current;
    VL_IN16(b_part,8,0);
    SData/*8:0*/ __PVT__b_final;
    SData/*8:0*/ __PVT__r_part;
    VL_IN(a_part,16,0);
    VL_OUT(rq_part,16,0);

    // INTERNAL VARIABLES
    div_8_pipe__Syms* const vlSymsp;

    // CONSTRUCTORS
    div_8_pipe_nrdiv_step(div_8_pipe__Syms* symsp, const char* name);
    ~div_8_pipe_nrdiv_step();
    VL_UNCOPYABLE(div_8_pipe_nrdiv_step);

    // INTERNAL METHODS
    void __Vconfigure(bool first);
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);


#endif  // guard
