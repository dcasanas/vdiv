// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See nrdiv.h for the primary calling header

#include "verilated.h"

#include "nrdiv___024root.h"

VL_ATTR_COLD void nrdiv___024root___eval_initial(nrdiv___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root___eval_initial\n"); );
    // Body
    vlSelf->__Vclklast__TOP__clk = vlSelf->clk;
    vlSelf->__Vclklast__TOP__nrst = vlSelf->nrst;
}

VL_ATTR_COLD void nrdiv___024root___final(nrdiv___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root___final\n"); );
}

VL_ATTR_COLD void nrdiv___024root___ctor_var_reset(nrdiv___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root___ctor_var_reset\n"); );
    // Body
    vlSelf->clk = VL_RAND_RESET_I(1);
    vlSelf->nrst = VL_RAND_RESET_I(1);
    vlSelf->a = VL_RAND_RESET_I(17);
    vlSelf->b = VL_RAND_RESET_I(9);
    vlSelf->q = VL_RAND_RESET_I(8);
    vlSelf->r = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        vlSelf->nrdiv__DOT__rq_part_ff[__Vi0] = VL_RAND_RESET_I(17);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        vlSelf->nrdiv__DOT__b_ff[__Vi0] = VL_RAND_RESET_I(9);
    }
    vlSelf->nrdiv__DOT__nxt_add_sub_ff = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        vlSelf->nrdiv__DOT__rq_part_w[__Vi0] = VL_RAND_RESET_I(17);
    }
    vlSelf->nrdiv__DOT__nxt_add_sub_w = VL_RAND_RESET_I(8);
    vlSelf->nrdiv__DOT__r_corr_w = VL_RAND_RESET_I(9);
    vlSelf->nrdiv__DOT__r_w = VL_RAND_RESET_I(9);
    vlSelf->nrdiv__DOT__unnamedblk2__DOT__i = 0;
    vlSelf->nrdiv__DOT__unnamedblk1__DOT__i = 0;
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry = VL_RAND_RESET_I(8);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g = VL_RAND_RESET_I(8);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p = VL_RAND_RESET_I(8);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 = VL_RAND_RESET_I(1);
    vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        vlSelf->__Vm_traceActivity[__Vi0] = VL_RAND_RESET_I(1);
    }
}
