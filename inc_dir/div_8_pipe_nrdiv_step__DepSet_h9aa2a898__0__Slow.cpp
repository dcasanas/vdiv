// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See div_8_pipe.h for the primary calling header

#include "verilated.h"

#include "div_8_pipe_nrdiv_step.h"

VL_ATTR_COLD void div_8_pipe_nrdiv_step___ctor_var_reset(div_8_pipe_nrdiv_step* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+          div_8_pipe_nrdiv_step___ctor_var_reset\n"); );
    // Body
    vlSelf->a_part = VL_RAND_RESET_I(17);
    vlSelf->b_part = VL_RAND_RESET_I(9);
    vlSelf->add_sub = VL_RAND_RESET_I(1);
    vlSelf->rq_part = VL_RAND_RESET_I(17);
    vlSelf->nxt_add_sub = VL_RAND_RESET_I(1);
    vlSelf->__PVT__b_final = VL_RAND_RESET_I(9);
    vlSelf->__PVT__r_part = VL_RAND_RESET_I(9);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry = VL_RAND_RESET_I(8);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g = VL_RAND_RESET_I(8);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p = VL_RAND_RESET_I(8);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42 = VL_RAND_RESET_I(1);
    vlSelf->__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 = VL_RAND_RESET_I(1);
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current = VL_RAND_RESET_I(1);
    vlSelf->add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current = VL_RAND_RESET_I(1);
}
