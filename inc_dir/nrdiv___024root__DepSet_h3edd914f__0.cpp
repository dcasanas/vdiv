// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See nrdiv.h for the primary calling header

#include "verilated.h"

#include "nrdiv___024root.h"

VL_INLINE_OPT void nrdiv___024root___sequent__TOP__0(nrdiv___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root___sequent__TOP__0\n"); );
    // Init
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v0;
    CData/*0:0*/ __Vdlyvset__nrdiv__DOT__rq_part_ff__v0;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v1;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v2;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v3;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v4;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v5;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v6;
    IData/*16:0*/ __Vdlyvval__nrdiv__DOT__rq_part_ff__v7;
    CData/*0:0*/ __Vdlyvset__nrdiv__DOT__rq_part_ff__v8;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v0;
    CData/*0:0*/ __Vdlyvset__nrdiv__DOT__b_ff__v0;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v1;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v2;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v3;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v4;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v5;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v6;
    SData/*8:0*/ __Vdlyvval__nrdiv__DOT__b_ff__v7;
    CData/*0:0*/ __Vdlyvset__nrdiv__DOT__b_ff__v8;
    // Body
    __Vdlyvset__nrdiv__DOT__rq_part_ff__v0 = 0U;
    __Vdlyvset__nrdiv__DOT__rq_part_ff__v8 = 0U;
    if ((1U & (~ (IData)(vlSelf->nrst)))) {
        vlSelf->nrdiv__DOT__unnamedblk1__DOT__i = 8U;
    }
    __Vdlyvset__nrdiv__DOT__b_ff__v0 = 0U;
    __Vdlyvset__nrdiv__DOT__b_ff__v8 = 0U;
    if (vlSelf->nrst) {
        vlSelf->nrdiv__DOT__unnamedblk2__DOT__i = 8U;
        vlSelf->nrdiv__DOT__nxt_add_sub_ff = ((0xf8U 
                                               & (IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff)) 
                                              | (1U 
                                                 | ((4U 
                                                     & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_w) 
                                                        << 1U)) 
                                                    | (2U 
                                                       & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_w) 
                                                          << 1U)))));
        vlSelf->nrdiv__DOT__nxt_add_sub_ff = ((7U & (IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff)) 
                                              | (0xf8U 
                                                 & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_w) 
                                                    << 1U)));
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v0 = vlSelf->a;
        __Vdlyvset__nrdiv__DOT__rq_part_ff__v0 = 1U;
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v1 = vlSelf->nrdiv__DOT__rq_part_w
            [0U];
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v2 = vlSelf->nrdiv__DOT__rq_part_w
            [1U];
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v3 = vlSelf->nrdiv__DOT__rq_part_w
            [2U];
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v4 = vlSelf->nrdiv__DOT__rq_part_w
            [3U];
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v5 = vlSelf->nrdiv__DOT__rq_part_w
            [4U];
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v6 = vlSelf->nrdiv__DOT__rq_part_w
            [5U];
        __Vdlyvval__nrdiv__DOT__rq_part_ff__v7 = vlSelf->nrdiv__DOT__rq_part_w
            [6U];
        __Vdlyvval__nrdiv__DOT__b_ff__v0 = vlSelf->b;
        __Vdlyvset__nrdiv__DOT__b_ff__v0 = 1U;
        __Vdlyvval__nrdiv__DOT__b_ff__v1 = vlSelf->nrdiv__DOT__b_ff
            [0U];
        __Vdlyvval__nrdiv__DOT__b_ff__v2 = vlSelf->nrdiv__DOT__b_ff
            [1U];
        __Vdlyvval__nrdiv__DOT__b_ff__v3 = vlSelf->nrdiv__DOT__b_ff
            [2U];
        __Vdlyvval__nrdiv__DOT__b_ff__v4 = vlSelf->nrdiv__DOT__b_ff
            [3U];
        __Vdlyvval__nrdiv__DOT__b_ff__v5 = vlSelf->nrdiv__DOT__b_ff
            [4U];
        __Vdlyvval__nrdiv__DOT__b_ff__v6 = vlSelf->nrdiv__DOT__b_ff
            [5U];
        __Vdlyvval__nrdiv__DOT__b_ff__v7 = vlSelf->nrdiv__DOT__b_ff
            [6U];
    } else {
        vlSelf->nrdiv__DOT__nxt_add_sub_ff = 0U;
        __Vdlyvset__nrdiv__DOT__rq_part_ff__v8 = 1U;
        __Vdlyvset__nrdiv__DOT__b_ff__v8 = 1U;
    }
    if (__Vdlyvset__nrdiv__DOT__rq_part_ff__v0) {
        vlSelf->nrdiv__DOT__rq_part_ff[0U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v0;
        vlSelf->nrdiv__DOT__rq_part_ff[1U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v1;
        vlSelf->nrdiv__DOT__rq_part_ff[2U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v2;
        vlSelf->nrdiv__DOT__rq_part_ff[3U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v3;
        vlSelf->nrdiv__DOT__rq_part_ff[4U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v4;
        vlSelf->nrdiv__DOT__rq_part_ff[5U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v5;
        vlSelf->nrdiv__DOT__rq_part_ff[6U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v6;
        vlSelf->nrdiv__DOT__rq_part_ff[7U] = __Vdlyvval__nrdiv__DOT__rq_part_ff__v7;
    }
    if (__Vdlyvset__nrdiv__DOT__rq_part_ff__v8) {
        vlSelf->nrdiv__DOT__rq_part_ff[0U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[1U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[2U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[3U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[4U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[5U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[6U] = 0U;
        vlSelf->nrdiv__DOT__rq_part_ff[7U] = 0U;
    }
    if (__Vdlyvset__nrdiv__DOT__b_ff__v0) {
        vlSelf->nrdiv__DOT__b_ff[0U] = __Vdlyvval__nrdiv__DOT__b_ff__v0;
        vlSelf->nrdiv__DOT__b_ff[1U] = __Vdlyvval__nrdiv__DOT__b_ff__v1;
        vlSelf->nrdiv__DOT__b_ff[2U] = __Vdlyvval__nrdiv__DOT__b_ff__v2;
        vlSelf->nrdiv__DOT__b_ff[3U] = __Vdlyvval__nrdiv__DOT__b_ff__v3;
        vlSelf->nrdiv__DOT__b_ff[4U] = __Vdlyvval__nrdiv__DOT__b_ff__v4;
        vlSelf->nrdiv__DOT__b_ff[5U] = __Vdlyvval__nrdiv__DOT__b_ff__v5;
        vlSelf->nrdiv__DOT__b_ff[6U] = __Vdlyvval__nrdiv__DOT__b_ff__v6;
        vlSelf->nrdiv__DOT__b_ff[7U] = __Vdlyvval__nrdiv__DOT__b_ff__v7;
    }
    if (__Vdlyvset__nrdiv__DOT__b_ff__v8) {
        vlSelf->nrdiv__DOT__b_ff[0U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[1U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[2U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[3U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[4U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[5U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[6U] = 0U;
        vlSelf->nrdiv__DOT__b_ff[7U] = 0U;
    }
}

#ifdef VL_DEBUG
void nrdiv___024root___eval_debug_assertions(nrdiv___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root___eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((vlSelf->clk & 0xfeU))) {
        Verilated::overWidthError("clk");}
    if (VL_UNLIKELY((vlSelf->nrst & 0xfeU))) {
        Verilated::overWidthError("nrst");}
    if (VL_UNLIKELY((vlSelf->a & 0xfffe0000U))) {
        Verilated::overWidthError("a");}
    if (VL_UNLIKELY((vlSelf->b & 0xfe00U))) {
        Verilated::overWidthError("b");}
}
#endif  // VL_DEBUG
