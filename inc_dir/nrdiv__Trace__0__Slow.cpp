// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "nrdiv__Syms.h"


VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBit(c+613,"clk", false,-1);
    tracep->declBit(c+614,"nrst", false,-1);
    tracep->declBus(c+615,"a", false,-1, 16,0);
    tracep->declBus(c+616,"b", false,-1, 8,0);
    tracep->declBus(c+617,"q", false,-1, 7,0);
    tracep->declBus(c+618,"r", false,-1, 7,0);
    tracep->pushNamePrefix("nrdiv ");
    tracep->declBus(c+619,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+619,"N_INST", false,-1, 31,0);
    tracep->declBit(c+613,"clk", false,-1);
    tracep->declBit(c+614,"nrst", false,-1);
    tracep->declBus(c+615,"a", false,-1, 16,0);
    tracep->declBus(c+616,"b", false,-1, 8,0);
    tracep->declBus(c+617,"q", false,-1, 7,0);
    tracep->declBus(c+618,"r", false,-1, 7,0);
    for (int i = 0; i < 8; ++i) {
        tracep->declBus(c+1+i*1,"rq_part_ff", true,(i+0), 16,0);
    }
    for (int i = 0; i < 8; ++i) {
        tracep->declBus(c+9+i*1,"b_ff", true,(i+0), 8,0);
    }
    tracep->declBus(c+17,"nxt_add_sub_ff", false,-1, 7,0);
    for (int i = 0; i < 8; ++i) {
        tracep->declBus(c+18+i*1,"rq_part_w", true,(i+0), 16,0);
    }
    tracep->declBus(c+26,"nxt_add_sub_w", false,-1, 7,0);
    tracep->declBus(c+27,"r_corr_w", false,-1, 8,0);
    tracep->declBus(c+28,"r_w", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst_0 ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+29,"op_a", false,-1, 8,0);
    tracep->declBus(c+27,"op_b", false,-1, 8,0);
    tracep->declBit(c+621,"in_c", false,-1);
    tracep->declBus(c+28,"sum", false,-1, 8,0);
    tracep->declBit(c+30,"out_c", false,-1);
    tracep->declBit(c+31,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+32,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+33,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+621,"input_carry", false,-1);
    tracep->declBus(c+34,"target_element", false,-1, 7,0);
    tracep->declBit(c+31,"output_carry", false,-1);
    tracep->declBus(c+35,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+32,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+33,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+621,"input_carry", false,-1);
    tracep->declBus(c+35,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+31,"output_carry", false,-1);
    tracep->declBus(c+36,"g", false,-1, 7,0);
    tracep->declBus(c+37,"p", false,-1, 7,0);
    tracep->declBit(c+621,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+621,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+38,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+38,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+39,"g_20_21", false,-1);
    tracep->declBit(c+40,"p_20_21", false,-1);
    tracep->declBit(c+39,"g_20_41", false,-1);
    tracep->declBit(c+40,"p_20_41", false,-1);
    tracep->declBit(c+41,"g_30_31", false,-1);
    tracep->declBit(c+42,"p_30_31", false,-1);
    tracep->declBit(c+41,"g_30_51", false,-1);
    tracep->declBit(c+42,"p_30_51", false,-1);
    tracep->declBit(c+43,"g_40_41", false,-1);
    tracep->declBit(c+44,"p_40_41", false,-1);
    tracep->declBit(c+43,"g_40_61", false,-1);
    tracep->declBit(c+44,"p_40_61", false,-1);
    tracep->declBit(c+45,"g_50_51", false,-1);
    tracep->declBit(c+46,"p_50_51", false,-1);
    tracep->declBit(c+45,"g_50_71", false,-1);
    tracep->declBit(c+46,"p_50_71", false,-1);
    tracep->declBit(c+47,"g_60_61", false,-1);
    tracep->declBit(c+48,"p_60_61", false,-1);
    tracep->declBit(c+47,"g_60_81", false,-1);
    tracep->declBit(c+48,"p_60_81", false,-1);
    tracep->declBit(c+49,"g_70_71", false,-1);
    tracep->declBit(c+50,"p_70_71", false,-1);
    tracep->declBit(c+51,"g_80_81", false,-1);
    tracep->declBit(c+52,"p_80_81", false,-1);
    tracep->declBit(c+621,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+621,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+53,"g_41_42", false,-1);
    tracep->declBit(c+54,"p_41_42", false,-1);
    tracep->declBit(c+38,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+55,"g_51_52", false,-1);
    tracep->declBit(c+56,"p_51_52", false,-1);
    tracep->declBit(c+39,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+57,"g_61_62", false,-1);
    tracep->declBit(c+58,"p_61_62", false,-1);
    tracep->declBit(c+59,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+60,"g_71_72", false,-1);
    tracep->declBit(c+61,"p_71_72", false,-1);
    tracep->declBit(c+53,"g_41_82", false,-1);
    tracep->declBit(c+54,"p_41_82", false,-1);
    tracep->declBit(c+62,"g_81_82", false,-1);
    tracep->declBit(c+63,"p_81_82", false,-1);
    tracep->declBit(c+621,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+31,"g_82_83", false,-1);
    tracep->declBit(c+64,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+621,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+38,"g_current", false,-1);
    tracep->declBit(c+65,"p_current", false,-1);
    tracep->declBit(c+38,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+38,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+38,"g_previous", false,-1);
    tracep->declBit(c+65,"p_previous", false,-1);
    tracep->declBit(c+66,"g_current", false,-1);
    tracep->declBit(c+67,"p_current", false,-1);
    tracep->declBit(c+39,"g_combined_current", false,-1);
    tracep->declBit(c+40,"p_combined_current", false,-1);
    tracep->declBit(c+39,"g_combined_next", false,-1);
    tracep->declBit(c+40,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+621,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+39,"g_current", false,-1);
    tracep->declBit(c+40,"p_current", false,-1);
    tracep->declBit(c+39,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+39,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+66,"g_previous", false,-1);
    tracep->declBit(c+67,"p_previous", false,-1);
    tracep->declBit(c+68,"g_current", false,-1);
    tracep->declBit(c+69,"p_current", false,-1);
    tracep->declBit(c+41,"g_combined_current", false,-1);
    tracep->declBit(c+42,"p_combined_current", false,-1);
    tracep->declBit(c+41,"g_combined_next", false,-1);
    tracep->declBit(c+42,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+38,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+41,"g_current", false,-1);
    tracep->declBit(c+42,"p_current", false,-1);
    tracep->declBit(c+59,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+59,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+68,"g_previous", false,-1);
    tracep->declBit(c+69,"p_previous", false,-1);
    tracep->declBit(c+70,"g_current", false,-1);
    tracep->declBit(c+71,"p_current", false,-1);
    tracep->declBit(c+43,"g_combined_current", false,-1);
    tracep->declBit(c+44,"p_combined_current", false,-1);
    tracep->declBit(c+43,"g_combined_next", false,-1);
    tracep->declBit(c+44,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+39,"g_previous", false,-1);
    tracep->declBit(c+40,"p_previous", false,-1);
    tracep->declBit(c+43,"g_current", false,-1);
    tracep->declBit(c+44,"p_current", false,-1);
    tracep->declBit(c+53,"g_combined_current", false,-1);
    tracep->declBit(c+54,"p_combined_current", false,-1);
    tracep->declBit(c+53,"g_combined_next", false,-1);
    tracep->declBit(c+54,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+621,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+53,"g_current", false,-1);
    tracep->declBit(c+54,"p_current", false,-1);
    tracep->declBit(c+53,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+53,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+70,"g_previous", false,-1);
    tracep->declBit(c+71,"p_previous", false,-1);
    tracep->declBit(c+72,"g_current", false,-1);
    tracep->declBit(c+73,"p_current", false,-1);
    tracep->declBit(c+45,"g_combined_current", false,-1);
    tracep->declBit(c+46,"p_combined_current", false,-1);
    tracep->declBit(c+45,"g_combined_next", false,-1);
    tracep->declBit(c+46,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+41,"g_previous", false,-1);
    tracep->declBit(c+42,"p_previous", false,-1);
    tracep->declBit(c+45,"g_current", false,-1);
    tracep->declBit(c+46,"p_current", false,-1);
    tracep->declBit(c+55,"g_combined_current", false,-1);
    tracep->declBit(c+56,"p_combined_current", false,-1);
    tracep->declBit(c+55,"g_combined_next", false,-1);
    tracep->declBit(c+56,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+38,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+55,"g_current", false,-1);
    tracep->declBit(c+56,"p_current", false,-1);
    tracep->declBit(c+74,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+74,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+72,"g_previous", false,-1);
    tracep->declBit(c+73,"p_previous", false,-1);
    tracep->declBit(c+75,"g_current", false,-1);
    tracep->declBit(c+76,"p_current", false,-1);
    tracep->declBit(c+47,"g_combined_current", false,-1);
    tracep->declBit(c+48,"p_combined_current", false,-1);
    tracep->declBit(c+47,"g_combined_next", false,-1);
    tracep->declBit(c+48,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+43,"g_previous", false,-1);
    tracep->declBit(c+44,"p_previous", false,-1);
    tracep->declBit(c+47,"g_current", false,-1);
    tracep->declBit(c+48,"p_current", false,-1);
    tracep->declBit(c+57,"g_combined_current", false,-1);
    tracep->declBit(c+58,"p_combined_current", false,-1);
    tracep->declBit(c+57,"g_combined_next", false,-1);
    tracep->declBit(c+58,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+39,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+57,"g_current", false,-1);
    tracep->declBit(c+58,"p_current", false,-1);
    tracep->declBit(c+77,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+77,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+75,"g_previous", false,-1);
    tracep->declBit(c+76,"p_previous", false,-1);
    tracep->declBit(c+78,"g_current", false,-1);
    tracep->declBit(c+79,"p_current", false,-1);
    tracep->declBit(c+49,"g_combined_current", false,-1);
    tracep->declBit(c+50,"p_combined_current", false,-1);
    tracep->declBit(c+49,"g_combined_next", false,-1);
    tracep->declBit(c+50,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+45,"g_previous", false,-1);
    tracep->declBit(c+46,"p_previous", false,-1);
    tracep->declBit(c+49,"g_current", false,-1);
    tracep->declBit(c+50,"p_current", false,-1);
    tracep->declBit(c+60,"g_combined_current", false,-1);
    tracep->declBit(c+61,"p_combined_current", false,-1);
    tracep->declBit(c+60,"g_combined_next", false,-1);
    tracep->declBit(c+61,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+59,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+60,"g_current", false,-1);
    tracep->declBit(c+61,"p_current", false,-1);
    tracep->declBit(c+80,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+80,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+78,"g_previous", false,-1);
    tracep->declBit(c+79,"p_previous", false,-1);
    tracep->declBit(c+81,"g_current", false,-1);
    tracep->declBit(c+82,"p_current", false,-1);
    tracep->declBit(c+51,"g_combined_current", false,-1);
    tracep->declBit(c+52,"p_combined_current", false,-1);
    tracep->declBit(c+51,"g_combined_next", false,-1);
    tracep->declBit(c+52,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+47,"g_previous", false,-1);
    tracep->declBit(c+48,"p_previous", false,-1);
    tracep->declBit(c+51,"g_current", false,-1);
    tracep->declBit(c+52,"p_current", false,-1);
    tracep->declBit(c+62,"g_combined_current", false,-1);
    tracep->declBit(c+63,"p_combined_current", false,-1);
    tracep->declBit(c+62,"g_combined_next", false,-1);
    tracep->declBit(c+63,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+53,"g_previous", false,-1);
    tracep->declBit(c+54,"p_previous", false,-1);
    tracep->declBit(c+62,"g_current", false,-1);
    tracep->declBit(c+63,"p_current", false,-1);
    tracep->declBit(c+31,"g_combined_current", false,-1);
    tracep->declBit(c+64,"p_combined_current", false,-1);
    tracep->declBit(c+31,"g_combined_next", false,-1);
    tracep->declBit(c+64,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+621,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+31,"g_current", false,-1);
    tracep->declBit(c+64,"p_current", false,-1);
    tracep->declBit(c+31,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+31,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+621,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+621,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+621,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+621,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+621,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+621,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+621,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+621,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+621,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+38,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+38,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+38,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
    tracep->pushNamePrefix("unnamedblk1 ");
    tracep->declBus(c+83,"i", false,-1, 31,0);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("unnamedblk2 ");
    tracep->declBus(c+84,"i", false,-1, 31,0);
    tracep->popNamePrefix(2);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+85,"a_part", false,-1, 16,0);
    tracep->declBus(c+86,"b_part", false,-1, 8,0);
    tracep->declBit(c+87,"add_sub", false,-1);
    tracep->declBus(c+88,"rq_part", false,-1, 16,0);
    tracep->declBit(c+89,"nxt_add_sub", false,-1);
    tracep->declBus(c+90,"ax2", false,-1, 16,0);
    tracep->declBus(c+91,"b_final", false,-1, 8,0);
    tracep->declBus(c+92,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+93,"op_a", false,-1, 8,0);
    tracep->declBus(c+91,"op_b", false,-1, 8,0);
    tracep->declBit(c+87,"in_c", false,-1);
    tracep->declBus(c+92,"sum", false,-1, 8,0);
    tracep->declBit(c+94,"out_c", false,-1);
    tracep->declBit(c+95,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+96,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+97,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+87,"input_carry", false,-1);
    tracep->declBus(c+98,"target_element", false,-1, 7,0);
    tracep->declBit(c+95,"output_carry", false,-1);
    tracep->declBus(c+99,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+96,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+97,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+87,"input_carry", false,-1);
    tracep->declBus(c+99,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+95,"output_carry", false,-1);
    tracep->declBus(c+100,"g", false,-1, 7,0);
    tracep->declBus(c+101,"p", false,-1, 7,0);
    tracep->declBit(c+87,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+87,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+102,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+102,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+103,"g_20_21", false,-1);
    tracep->declBit(c+104,"p_20_21", false,-1);
    tracep->declBit(c+103,"g_20_41", false,-1);
    tracep->declBit(c+104,"p_20_41", false,-1);
    tracep->declBit(c+105,"g_30_31", false,-1);
    tracep->declBit(c+106,"p_30_31", false,-1);
    tracep->declBit(c+105,"g_30_51", false,-1);
    tracep->declBit(c+106,"p_30_51", false,-1);
    tracep->declBit(c+107,"g_40_41", false,-1);
    tracep->declBit(c+108,"p_40_41", false,-1);
    tracep->declBit(c+107,"g_40_61", false,-1);
    tracep->declBit(c+108,"p_40_61", false,-1);
    tracep->declBit(c+109,"g_50_51", false,-1);
    tracep->declBit(c+110,"p_50_51", false,-1);
    tracep->declBit(c+109,"g_50_71", false,-1);
    tracep->declBit(c+110,"p_50_71", false,-1);
    tracep->declBit(c+111,"g_60_61", false,-1);
    tracep->declBit(c+112,"p_60_61", false,-1);
    tracep->declBit(c+111,"g_60_81", false,-1);
    tracep->declBit(c+112,"p_60_81", false,-1);
    tracep->declBit(c+113,"g_70_71", false,-1);
    tracep->declBit(c+114,"p_70_71", false,-1);
    tracep->declBit(c+115,"g_80_81", false,-1);
    tracep->declBit(c+116,"p_80_81", false,-1);
    tracep->declBit(c+87,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+87,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+117,"g_41_42", false,-1);
    tracep->declBit(c+118,"p_41_42", false,-1);
    tracep->declBit(c+102,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+119,"g_51_52", false,-1);
    tracep->declBit(c+120,"p_51_52", false,-1);
    tracep->declBit(c+121,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+122,"g_61_62", false,-1);
    tracep->declBit(c+123,"p_61_62", false,-1);
    tracep->declBit(c+124,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+125,"g_71_72", false,-1);
    tracep->declBit(c+126,"p_71_72", false,-1);
    tracep->declBit(c+117,"g_41_82", false,-1);
    tracep->declBit(c+118,"p_41_82", false,-1);
    tracep->declBit(c+127,"g_81_82", false,-1);
    tracep->declBit(c+128,"p_81_82", false,-1);
    tracep->declBit(c+87,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+129,"g_82_83", false,-1);
    tracep->declBit(c+130,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+87,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+131,"g_current", false,-1);
    tracep->declBit(c+132,"p_current", false,-1);
    tracep->declBit(c+102,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+102,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+131,"g_previous", false,-1);
    tracep->declBit(c+132,"p_previous", false,-1);
    tracep->declBit(c+133,"g_current", false,-1);
    tracep->declBit(c+134,"p_current", false,-1);
    tracep->declBit(c+103,"g_combined_current", false,-1);
    tracep->declBit(c+104,"p_combined_current", false,-1);
    tracep->declBit(c+103,"g_combined_next", false,-1);
    tracep->declBit(c+104,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+87,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+103,"g_current", false,-1);
    tracep->declBit(c+104,"p_current", false,-1);
    tracep->declBit(c+121,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+121,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+133,"g_previous", false,-1);
    tracep->declBit(c+134,"p_previous", false,-1);
    tracep->declBit(c+135,"g_current", false,-1);
    tracep->declBit(c+136,"p_current", false,-1);
    tracep->declBit(c+105,"g_combined_current", false,-1);
    tracep->declBit(c+106,"p_combined_current", false,-1);
    tracep->declBit(c+105,"g_combined_next", false,-1);
    tracep->declBit(c+106,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+102,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+105,"g_current", false,-1);
    tracep->declBit(c+106,"p_current", false,-1);
    tracep->declBit(c+124,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+124,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+135,"g_previous", false,-1);
    tracep->declBit(c+136,"p_previous", false,-1);
    tracep->declBit(c+137,"g_current", false,-1);
    tracep->declBit(c+138,"p_current", false,-1);
    tracep->declBit(c+107,"g_combined_current", false,-1);
    tracep->declBit(c+108,"p_combined_current", false,-1);
    tracep->declBit(c+107,"g_combined_next", false,-1);
    tracep->declBit(c+108,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+103,"g_previous", false,-1);
    tracep->declBit(c+104,"p_previous", false,-1);
    tracep->declBit(c+107,"g_current", false,-1);
    tracep->declBit(c+108,"p_current", false,-1);
    tracep->declBit(c+117,"g_combined_current", false,-1);
    tracep->declBit(c+118,"p_combined_current", false,-1);
    tracep->declBit(c+117,"g_combined_next", false,-1);
    tracep->declBit(c+118,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+87,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+117,"g_current", false,-1);
    tracep->declBit(c+118,"p_current", false,-1);
    tracep->declBit(c+139,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+139,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+137,"g_previous", false,-1);
    tracep->declBit(c+138,"p_previous", false,-1);
    tracep->declBit(c+140,"g_current", false,-1);
    tracep->declBit(c+141,"p_current", false,-1);
    tracep->declBit(c+109,"g_combined_current", false,-1);
    tracep->declBit(c+110,"p_combined_current", false,-1);
    tracep->declBit(c+109,"g_combined_next", false,-1);
    tracep->declBit(c+110,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+105,"g_previous", false,-1);
    tracep->declBit(c+106,"p_previous", false,-1);
    tracep->declBit(c+109,"g_current", false,-1);
    tracep->declBit(c+110,"p_current", false,-1);
    tracep->declBit(c+119,"g_combined_current", false,-1);
    tracep->declBit(c+120,"p_combined_current", false,-1);
    tracep->declBit(c+119,"g_combined_next", false,-1);
    tracep->declBit(c+120,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+102,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+119,"g_current", false,-1);
    tracep->declBit(c+120,"p_current", false,-1);
    tracep->declBit(c+142,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+142,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+140,"g_previous", false,-1);
    tracep->declBit(c+141,"p_previous", false,-1);
    tracep->declBit(c+143,"g_current", false,-1);
    tracep->declBit(c+144,"p_current", false,-1);
    tracep->declBit(c+111,"g_combined_current", false,-1);
    tracep->declBit(c+112,"p_combined_current", false,-1);
    tracep->declBit(c+111,"g_combined_next", false,-1);
    tracep->declBit(c+112,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+107,"g_previous", false,-1);
    tracep->declBit(c+108,"p_previous", false,-1);
    tracep->declBit(c+111,"g_current", false,-1);
    tracep->declBit(c+112,"p_current", false,-1);
    tracep->declBit(c+122,"g_combined_current", false,-1);
    tracep->declBit(c+123,"p_combined_current", false,-1);
    tracep->declBit(c+122,"g_combined_next", false,-1);
    tracep->declBit(c+123,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+121,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+122,"g_current", false,-1);
    tracep->declBit(c+123,"p_current", false,-1);
    tracep->declBit(c+145,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+145,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+143,"g_previous", false,-1);
    tracep->declBit(c+144,"p_previous", false,-1);
    tracep->declBit(c+146,"g_current", false,-1);
    tracep->declBit(c+147,"p_current", false,-1);
    tracep->declBit(c+113,"g_combined_current", false,-1);
    tracep->declBit(c+114,"p_combined_current", false,-1);
    tracep->declBit(c+113,"g_combined_next", false,-1);
    tracep->declBit(c+114,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+109,"g_previous", false,-1);
    tracep->declBit(c+110,"p_previous", false,-1);
    tracep->declBit(c+113,"g_current", false,-1);
    tracep->declBit(c+114,"p_current", false,-1);
    tracep->declBit(c+125,"g_combined_current", false,-1);
    tracep->declBit(c+126,"p_combined_current", false,-1);
    tracep->declBit(c+125,"g_combined_next", false,-1);
    tracep->declBit(c+126,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+124,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+125,"g_current", false,-1);
    tracep->declBit(c+126,"p_current", false,-1);
    tracep->declBit(c+148,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+148,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+146,"g_previous", false,-1);
    tracep->declBit(c+147,"p_previous", false,-1);
    tracep->declBit(c+149,"g_current", false,-1);
    tracep->declBit(c+150,"p_current", false,-1);
    tracep->declBit(c+115,"g_combined_current", false,-1);
    tracep->declBit(c+116,"p_combined_current", false,-1);
    tracep->declBit(c+115,"g_combined_next", false,-1);
    tracep->declBit(c+116,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+111,"g_previous", false,-1);
    tracep->declBit(c+112,"p_previous", false,-1);
    tracep->declBit(c+115,"g_current", false,-1);
    tracep->declBit(c+116,"p_current", false,-1);
    tracep->declBit(c+127,"g_combined_current", false,-1);
    tracep->declBit(c+128,"p_combined_current", false,-1);
    tracep->declBit(c+127,"g_combined_next", false,-1);
    tracep->declBit(c+128,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+117,"g_previous", false,-1);
    tracep->declBit(c+118,"p_previous", false,-1);
    tracep->declBit(c+127,"g_current", false,-1);
    tracep->declBit(c+128,"p_current", false,-1);
    tracep->declBit(c+129,"g_combined_current", false,-1);
    tracep->declBit(c+130,"p_combined_current", false,-1);
    tracep->declBit(c+129,"g_combined_next", false,-1);
    tracep->declBit(c+130,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+87,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+129,"g_current", false,-1);
    tracep->declBit(c+130,"p_current", false,-1);
    tracep->declBit(c+95,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+95,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+87,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+87,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+87,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+87,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+87,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+87,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+87,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+87,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+87,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+102,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+102,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+102,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+151,"a_part", false,-1, 16,0);
    tracep->declBus(c+152,"b_part", false,-1, 8,0);
    tracep->declBit(c+153,"add_sub", false,-1);
    tracep->declBus(c+154,"rq_part", false,-1, 16,0);
    tracep->declBit(c+155,"nxt_add_sub", false,-1);
    tracep->declBus(c+156,"ax2", false,-1, 16,0);
    tracep->declBus(c+157,"b_final", false,-1, 8,0);
    tracep->declBus(c+158,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+159,"op_a", false,-1, 8,0);
    tracep->declBus(c+157,"op_b", false,-1, 8,0);
    tracep->declBit(c+153,"in_c", false,-1);
    tracep->declBus(c+158,"sum", false,-1, 8,0);
    tracep->declBit(c+160,"out_c", false,-1);
    tracep->declBit(c+161,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+162,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+163,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+153,"input_carry", false,-1);
    tracep->declBus(c+164,"target_element", false,-1, 7,0);
    tracep->declBit(c+161,"output_carry", false,-1);
    tracep->declBus(c+165,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+162,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+163,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+153,"input_carry", false,-1);
    tracep->declBus(c+165,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+161,"output_carry", false,-1);
    tracep->declBus(c+166,"g", false,-1, 7,0);
    tracep->declBus(c+167,"p", false,-1, 7,0);
    tracep->declBit(c+153,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+153,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+168,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+168,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+169,"g_20_21", false,-1);
    tracep->declBit(c+170,"p_20_21", false,-1);
    tracep->declBit(c+169,"g_20_41", false,-1);
    tracep->declBit(c+170,"p_20_41", false,-1);
    tracep->declBit(c+171,"g_30_31", false,-1);
    tracep->declBit(c+172,"p_30_31", false,-1);
    tracep->declBit(c+171,"g_30_51", false,-1);
    tracep->declBit(c+172,"p_30_51", false,-1);
    tracep->declBit(c+173,"g_40_41", false,-1);
    tracep->declBit(c+174,"p_40_41", false,-1);
    tracep->declBit(c+173,"g_40_61", false,-1);
    tracep->declBit(c+174,"p_40_61", false,-1);
    tracep->declBit(c+175,"g_50_51", false,-1);
    tracep->declBit(c+176,"p_50_51", false,-1);
    tracep->declBit(c+175,"g_50_71", false,-1);
    tracep->declBit(c+176,"p_50_71", false,-1);
    tracep->declBit(c+177,"g_60_61", false,-1);
    tracep->declBit(c+178,"p_60_61", false,-1);
    tracep->declBit(c+177,"g_60_81", false,-1);
    tracep->declBit(c+178,"p_60_81", false,-1);
    tracep->declBit(c+179,"g_70_71", false,-1);
    tracep->declBit(c+180,"p_70_71", false,-1);
    tracep->declBit(c+181,"g_80_81", false,-1);
    tracep->declBit(c+182,"p_80_81", false,-1);
    tracep->declBit(c+153,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+153,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+183,"g_41_42", false,-1);
    tracep->declBit(c+184,"p_41_42", false,-1);
    tracep->declBit(c+168,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+185,"g_51_52", false,-1);
    tracep->declBit(c+186,"p_51_52", false,-1);
    tracep->declBit(c+187,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+188,"g_61_62", false,-1);
    tracep->declBit(c+189,"p_61_62", false,-1);
    tracep->declBit(c+190,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+191,"g_71_72", false,-1);
    tracep->declBit(c+192,"p_71_72", false,-1);
    tracep->declBit(c+183,"g_41_82", false,-1);
    tracep->declBit(c+184,"p_41_82", false,-1);
    tracep->declBit(c+193,"g_81_82", false,-1);
    tracep->declBit(c+194,"p_81_82", false,-1);
    tracep->declBit(c+153,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+195,"g_82_83", false,-1);
    tracep->declBit(c+196,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+153,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+197,"g_current", false,-1);
    tracep->declBit(c+198,"p_current", false,-1);
    tracep->declBit(c+168,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+168,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+197,"g_previous", false,-1);
    tracep->declBit(c+198,"p_previous", false,-1);
    tracep->declBit(c+199,"g_current", false,-1);
    tracep->declBit(c+200,"p_current", false,-1);
    tracep->declBit(c+169,"g_combined_current", false,-1);
    tracep->declBit(c+170,"p_combined_current", false,-1);
    tracep->declBit(c+169,"g_combined_next", false,-1);
    tracep->declBit(c+170,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+153,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+169,"g_current", false,-1);
    tracep->declBit(c+170,"p_current", false,-1);
    tracep->declBit(c+187,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+187,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+199,"g_previous", false,-1);
    tracep->declBit(c+200,"p_previous", false,-1);
    tracep->declBit(c+201,"g_current", false,-1);
    tracep->declBit(c+202,"p_current", false,-1);
    tracep->declBit(c+171,"g_combined_current", false,-1);
    tracep->declBit(c+172,"p_combined_current", false,-1);
    tracep->declBit(c+171,"g_combined_next", false,-1);
    tracep->declBit(c+172,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+168,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+171,"g_current", false,-1);
    tracep->declBit(c+172,"p_current", false,-1);
    tracep->declBit(c+190,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+190,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+201,"g_previous", false,-1);
    tracep->declBit(c+202,"p_previous", false,-1);
    tracep->declBit(c+203,"g_current", false,-1);
    tracep->declBit(c+204,"p_current", false,-1);
    tracep->declBit(c+173,"g_combined_current", false,-1);
    tracep->declBit(c+174,"p_combined_current", false,-1);
    tracep->declBit(c+173,"g_combined_next", false,-1);
    tracep->declBit(c+174,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+169,"g_previous", false,-1);
    tracep->declBit(c+170,"p_previous", false,-1);
    tracep->declBit(c+173,"g_current", false,-1);
    tracep->declBit(c+174,"p_current", false,-1);
    tracep->declBit(c+183,"g_combined_current", false,-1);
    tracep->declBit(c+184,"p_combined_current", false,-1);
    tracep->declBit(c+183,"g_combined_next", false,-1);
    tracep->declBit(c+184,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+153,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+183,"g_current", false,-1);
    tracep->declBit(c+184,"p_current", false,-1);
    tracep->declBit(c+205,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+205,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+203,"g_previous", false,-1);
    tracep->declBit(c+204,"p_previous", false,-1);
    tracep->declBit(c+206,"g_current", false,-1);
    tracep->declBit(c+207,"p_current", false,-1);
    tracep->declBit(c+175,"g_combined_current", false,-1);
    tracep->declBit(c+176,"p_combined_current", false,-1);
    tracep->declBit(c+175,"g_combined_next", false,-1);
    tracep->declBit(c+176,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+171,"g_previous", false,-1);
    tracep->declBit(c+172,"p_previous", false,-1);
    tracep->declBit(c+175,"g_current", false,-1);
    tracep->declBit(c+176,"p_current", false,-1);
    tracep->declBit(c+185,"g_combined_current", false,-1);
    tracep->declBit(c+186,"p_combined_current", false,-1);
    tracep->declBit(c+185,"g_combined_next", false,-1);
    tracep->declBit(c+186,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+168,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+185,"g_current", false,-1);
    tracep->declBit(c+186,"p_current", false,-1);
    tracep->declBit(c+208,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+208,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+206,"g_previous", false,-1);
    tracep->declBit(c+207,"p_previous", false,-1);
    tracep->declBit(c+209,"g_current", false,-1);
    tracep->declBit(c+210,"p_current", false,-1);
    tracep->declBit(c+177,"g_combined_current", false,-1);
    tracep->declBit(c+178,"p_combined_current", false,-1);
    tracep->declBit(c+177,"g_combined_next", false,-1);
    tracep->declBit(c+178,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+173,"g_previous", false,-1);
    tracep->declBit(c+174,"p_previous", false,-1);
    tracep->declBit(c+177,"g_current", false,-1);
    tracep->declBit(c+178,"p_current", false,-1);
    tracep->declBit(c+188,"g_combined_current", false,-1);
    tracep->declBit(c+189,"p_combined_current", false,-1);
    tracep->declBit(c+188,"g_combined_next", false,-1);
    tracep->declBit(c+189,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+187,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+188,"g_current", false,-1);
    tracep->declBit(c+189,"p_current", false,-1);
    tracep->declBit(c+211,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+211,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+209,"g_previous", false,-1);
    tracep->declBit(c+210,"p_previous", false,-1);
    tracep->declBit(c+212,"g_current", false,-1);
    tracep->declBit(c+213,"p_current", false,-1);
    tracep->declBit(c+179,"g_combined_current", false,-1);
    tracep->declBit(c+180,"p_combined_current", false,-1);
    tracep->declBit(c+179,"g_combined_next", false,-1);
    tracep->declBit(c+180,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+175,"g_previous", false,-1);
    tracep->declBit(c+176,"p_previous", false,-1);
    tracep->declBit(c+179,"g_current", false,-1);
    tracep->declBit(c+180,"p_current", false,-1);
    tracep->declBit(c+191,"g_combined_current", false,-1);
    tracep->declBit(c+192,"p_combined_current", false,-1);
    tracep->declBit(c+191,"g_combined_next", false,-1);
    tracep->declBit(c+192,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+190,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+191,"g_current", false,-1);
    tracep->declBit(c+192,"p_current", false,-1);
    tracep->declBit(c+214,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+214,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+212,"g_previous", false,-1);
    tracep->declBit(c+213,"p_previous", false,-1);
    tracep->declBit(c+215,"g_current", false,-1);
    tracep->declBit(c+216,"p_current", false,-1);
    tracep->declBit(c+181,"g_combined_current", false,-1);
    tracep->declBit(c+182,"p_combined_current", false,-1);
    tracep->declBit(c+181,"g_combined_next", false,-1);
    tracep->declBit(c+182,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+177,"g_previous", false,-1);
    tracep->declBit(c+178,"p_previous", false,-1);
    tracep->declBit(c+181,"g_current", false,-1);
    tracep->declBit(c+182,"p_current", false,-1);
    tracep->declBit(c+193,"g_combined_current", false,-1);
    tracep->declBit(c+194,"p_combined_current", false,-1);
    tracep->declBit(c+193,"g_combined_next", false,-1);
    tracep->declBit(c+194,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+183,"g_previous", false,-1);
    tracep->declBit(c+184,"p_previous", false,-1);
    tracep->declBit(c+193,"g_current", false,-1);
    tracep->declBit(c+194,"p_current", false,-1);
    tracep->declBit(c+195,"g_combined_current", false,-1);
    tracep->declBit(c+196,"p_combined_current", false,-1);
    tracep->declBit(c+195,"g_combined_next", false,-1);
    tracep->declBit(c+196,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+153,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+195,"g_current", false,-1);
    tracep->declBit(c+196,"p_current", false,-1);
    tracep->declBit(c+161,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+161,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+153,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+153,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+153,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+153,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+153,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+153,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+153,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+153,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+153,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+168,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+168,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+168,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+217,"a_part", false,-1, 16,0);
    tracep->declBus(c+218,"b_part", false,-1, 8,0);
    tracep->declBit(c+219,"add_sub", false,-1);
    tracep->declBus(c+220,"rq_part", false,-1, 16,0);
    tracep->declBit(c+221,"nxt_add_sub", false,-1);
    tracep->declBus(c+222,"ax2", false,-1, 16,0);
    tracep->declBus(c+223,"b_final", false,-1, 8,0);
    tracep->declBus(c+224,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+225,"op_a", false,-1, 8,0);
    tracep->declBus(c+223,"op_b", false,-1, 8,0);
    tracep->declBit(c+219,"in_c", false,-1);
    tracep->declBus(c+224,"sum", false,-1, 8,0);
    tracep->declBit(c+226,"out_c", false,-1);
    tracep->declBit(c+227,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+228,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+229,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+219,"input_carry", false,-1);
    tracep->declBus(c+230,"target_element", false,-1, 7,0);
    tracep->declBit(c+227,"output_carry", false,-1);
    tracep->declBus(c+231,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+228,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+229,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+219,"input_carry", false,-1);
    tracep->declBus(c+231,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+227,"output_carry", false,-1);
    tracep->declBus(c+232,"g", false,-1, 7,0);
    tracep->declBus(c+233,"p", false,-1, 7,0);
    tracep->declBit(c+219,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+219,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+234,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+234,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+235,"g_20_21", false,-1);
    tracep->declBit(c+236,"p_20_21", false,-1);
    tracep->declBit(c+235,"g_20_41", false,-1);
    tracep->declBit(c+236,"p_20_41", false,-1);
    tracep->declBit(c+237,"g_30_31", false,-1);
    tracep->declBit(c+238,"p_30_31", false,-1);
    tracep->declBit(c+237,"g_30_51", false,-1);
    tracep->declBit(c+238,"p_30_51", false,-1);
    tracep->declBit(c+239,"g_40_41", false,-1);
    tracep->declBit(c+240,"p_40_41", false,-1);
    tracep->declBit(c+239,"g_40_61", false,-1);
    tracep->declBit(c+240,"p_40_61", false,-1);
    tracep->declBit(c+241,"g_50_51", false,-1);
    tracep->declBit(c+242,"p_50_51", false,-1);
    tracep->declBit(c+241,"g_50_71", false,-1);
    tracep->declBit(c+242,"p_50_71", false,-1);
    tracep->declBit(c+243,"g_60_61", false,-1);
    tracep->declBit(c+244,"p_60_61", false,-1);
    tracep->declBit(c+243,"g_60_81", false,-1);
    tracep->declBit(c+244,"p_60_81", false,-1);
    tracep->declBit(c+245,"g_70_71", false,-1);
    tracep->declBit(c+246,"p_70_71", false,-1);
    tracep->declBit(c+247,"g_80_81", false,-1);
    tracep->declBit(c+248,"p_80_81", false,-1);
    tracep->declBit(c+219,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+219,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+249,"g_41_42", false,-1);
    tracep->declBit(c+250,"p_41_42", false,-1);
    tracep->declBit(c+234,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+251,"g_51_52", false,-1);
    tracep->declBit(c+252,"p_51_52", false,-1);
    tracep->declBit(c+253,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+254,"g_61_62", false,-1);
    tracep->declBit(c+255,"p_61_62", false,-1);
    tracep->declBit(c+256,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+257,"g_71_72", false,-1);
    tracep->declBit(c+258,"p_71_72", false,-1);
    tracep->declBit(c+249,"g_41_82", false,-1);
    tracep->declBit(c+250,"p_41_82", false,-1);
    tracep->declBit(c+259,"g_81_82", false,-1);
    tracep->declBit(c+260,"p_81_82", false,-1);
    tracep->declBit(c+219,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+261,"g_82_83", false,-1);
    tracep->declBit(c+262,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+219,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+263,"g_current", false,-1);
    tracep->declBit(c+264,"p_current", false,-1);
    tracep->declBit(c+234,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+234,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+263,"g_previous", false,-1);
    tracep->declBit(c+264,"p_previous", false,-1);
    tracep->declBit(c+265,"g_current", false,-1);
    tracep->declBit(c+266,"p_current", false,-1);
    tracep->declBit(c+235,"g_combined_current", false,-1);
    tracep->declBit(c+236,"p_combined_current", false,-1);
    tracep->declBit(c+235,"g_combined_next", false,-1);
    tracep->declBit(c+236,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+219,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+235,"g_current", false,-1);
    tracep->declBit(c+236,"p_current", false,-1);
    tracep->declBit(c+253,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+253,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+265,"g_previous", false,-1);
    tracep->declBit(c+266,"p_previous", false,-1);
    tracep->declBit(c+267,"g_current", false,-1);
    tracep->declBit(c+268,"p_current", false,-1);
    tracep->declBit(c+237,"g_combined_current", false,-1);
    tracep->declBit(c+238,"p_combined_current", false,-1);
    tracep->declBit(c+237,"g_combined_next", false,-1);
    tracep->declBit(c+238,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+234,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+237,"g_current", false,-1);
    tracep->declBit(c+238,"p_current", false,-1);
    tracep->declBit(c+256,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+256,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+267,"g_previous", false,-1);
    tracep->declBit(c+268,"p_previous", false,-1);
    tracep->declBit(c+269,"g_current", false,-1);
    tracep->declBit(c+270,"p_current", false,-1);
    tracep->declBit(c+239,"g_combined_current", false,-1);
    tracep->declBit(c+240,"p_combined_current", false,-1);
    tracep->declBit(c+239,"g_combined_next", false,-1);
    tracep->declBit(c+240,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+235,"g_previous", false,-1);
    tracep->declBit(c+236,"p_previous", false,-1);
    tracep->declBit(c+239,"g_current", false,-1);
    tracep->declBit(c+240,"p_current", false,-1);
    tracep->declBit(c+249,"g_combined_current", false,-1);
    tracep->declBit(c+250,"p_combined_current", false,-1);
    tracep->declBit(c+249,"g_combined_next", false,-1);
    tracep->declBit(c+250,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+219,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+249,"g_current", false,-1);
    tracep->declBit(c+250,"p_current", false,-1);
    tracep->declBit(c+271,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+271,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+269,"g_previous", false,-1);
    tracep->declBit(c+270,"p_previous", false,-1);
    tracep->declBit(c+272,"g_current", false,-1);
    tracep->declBit(c+273,"p_current", false,-1);
    tracep->declBit(c+241,"g_combined_current", false,-1);
    tracep->declBit(c+242,"p_combined_current", false,-1);
    tracep->declBit(c+241,"g_combined_next", false,-1);
    tracep->declBit(c+242,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+237,"g_previous", false,-1);
    tracep->declBit(c+238,"p_previous", false,-1);
    tracep->declBit(c+241,"g_current", false,-1);
    tracep->declBit(c+242,"p_current", false,-1);
    tracep->declBit(c+251,"g_combined_current", false,-1);
    tracep->declBit(c+252,"p_combined_current", false,-1);
    tracep->declBit(c+251,"g_combined_next", false,-1);
    tracep->declBit(c+252,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+234,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+251,"g_current", false,-1);
    tracep->declBit(c+252,"p_current", false,-1);
    tracep->declBit(c+274,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+274,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+272,"g_previous", false,-1);
    tracep->declBit(c+273,"p_previous", false,-1);
    tracep->declBit(c+275,"g_current", false,-1);
    tracep->declBit(c+276,"p_current", false,-1);
    tracep->declBit(c+243,"g_combined_current", false,-1);
    tracep->declBit(c+244,"p_combined_current", false,-1);
    tracep->declBit(c+243,"g_combined_next", false,-1);
    tracep->declBit(c+244,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+239,"g_previous", false,-1);
    tracep->declBit(c+240,"p_previous", false,-1);
    tracep->declBit(c+243,"g_current", false,-1);
    tracep->declBit(c+244,"p_current", false,-1);
    tracep->declBit(c+254,"g_combined_current", false,-1);
    tracep->declBit(c+255,"p_combined_current", false,-1);
    tracep->declBit(c+254,"g_combined_next", false,-1);
    tracep->declBit(c+255,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+253,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+254,"g_current", false,-1);
    tracep->declBit(c+255,"p_current", false,-1);
    tracep->declBit(c+277,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+277,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+275,"g_previous", false,-1);
    tracep->declBit(c+276,"p_previous", false,-1);
    tracep->declBit(c+278,"g_current", false,-1);
    tracep->declBit(c+279,"p_current", false,-1);
    tracep->declBit(c+245,"g_combined_current", false,-1);
    tracep->declBit(c+246,"p_combined_current", false,-1);
    tracep->declBit(c+245,"g_combined_next", false,-1);
    tracep->declBit(c+246,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+241,"g_previous", false,-1);
    tracep->declBit(c+242,"p_previous", false,-1);
    tracep->declBit(c+245,"g_current", false,-1);
    tracep->declBit(c+246,"p_current", false,-1);
    tracep->declBit(c+257,"g_combined_current", false,-1);
    tracep->declBit(c+258,"p_combined_current", false,-1);
    tracep->declBit(c+257,"g_combined_next", false,-1);
    tracep->declBit(c+258,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+256,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+257,"g_current", false,-1);
    tracep->declBit(c+258,"p_current", false,-1);
    tracep->declBit(c+280,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+280,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+278,"g_previous", false,-1);
    tracep->declBit(c+279,"p_previous", false,-1);
    tracep->declBit(c+281,"g_current", false,-1);
    tracep->declBit(c+282,"p_current", false,-1);
    tracep->declBit(c+247,"g_combined_current", false,-1);
    tracep->declBit(c+248,"p_combined_current", false,-1);
    tracep->declBit(c+247,"g_combined_next", false,-1);
    tracep->declBit(c+248,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+243,"g_previous", false,-1);
    tracep->declBit(c+244,"p_previous", false,-1);
    tracep->declBit(c+247,"g_current", false,-1);
    tracep->declBit(c+248,"p_current", false,-1);
    tracep->declBit(c+259,"g_combined_current", false,-1);
    tracep->declBit(c+260,"p_combined_current", false,-1);
    tracep->declBit(c+259,"g_combined_next", false,-1);
    tracep->declBit(c+260,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+249,"g_previous", false,-1);
    tracep->declBit(c+250,"p_previous", false,-1);
    tracep->declBit(c+259,"g_current", false,-1);
    tracep->declBit(c+260,"p_current", false,-1);
    tracep->declBit(c+261,"g_combined_current", false,-1);
    tracep->declBit(c+262,"p_combined_current", false,-1);
    tracep->declBit(c+261,"g_combined_next", false,-1);
    tracep->declBit(c+262,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+219,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+261,"g_current", false,-1);
    tracep->declBit(c+262,"p_current", false,-1);
    tracep->declBit(c+227,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+227,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+219,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+219,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+219,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+219,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+219,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+219,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+219,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+219,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+219,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+234,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+234,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+234,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+283,"a_part", false,-1, 16,0);
    tracep->declBus(c+284,"b_part", false,-1, 8,0);
    tracep->declBit(c+285,"add_sub", false,-1);
    tracep->declBus(c+286,"rq_part", false,-1, 16,0);
    tracep->declBit(c+287,"nxt_add_sub", false,-1);
    tracep->declBus(c+288,"ax2", false,-1, 16,0);
    tracep->declBus(c+289,"b_final", false,-1, 8,0);
    tracep->declBus(c+290,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+291,"op_a", false,-1, 8,0);
    tracep->declBus(c+289,"op_b", false,-1, 8,0);
    tracep->declBit(c+285,"in_c", false,-1);
    tracep->declBus(c+290,"sum", false,-1, 8,0);
    tracep->declBit(c+292,"out_c", false,-1);
    tracep->declBit(c+293,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+294,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+295,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+285,"input_carry", false,-1);
    tracep->declBus(c+296,"target_element", false,-1, 7,0);
    tracep->declBit(c+293,"output_carry", false,-1);
    tracep->declBus(c+297,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+294,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+295,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+285,"input_carry", false,-1);
    tracep->declBus(c+297,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+293,"output_carry", false,-1);
    tracep->declBus(c+298,"g", false,-1, 7,0);
    tracep->declBus(c+299,"p", false,-1, 7,0);
    tracep->declBit(c+285,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+285,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+300,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+300,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+301,"g_20_21", false,-1);
    tracep->declBit(c+302,"p_20_21", false,-1);
    tracep->declBit(c+301,"g_20_41", false,-1);
    tracep->declBit(c+302,"p_20_41", false,-1);
    tracep->declBit(c+303,"g_30_31", false,-1);
    tracep->declBit(c+304,"p_30_31", false,-1);
    tracep->declBit(c+303,"g_30_51", false,-1);
    tracep->declBit(c+304,"p_30_51", false,-1);
    tracep->declBit(c+305,"g_40_41", false,-1);
    tracep->declBit(c+306,"p_40_41", false,-1);
    tracep->declBit(c+305,"g_40_61", false,-1);
    tracep->declBit(c+306,"p_40_61", false,-1);
    tracep->declBit(c+307,"g_50_51", false,-1);
    tracep->declBit(c+308,"p_50_51", false,-1);
    tracep->declBit(c+307,"g_50_71", false,-1);
    tracep->declBit(c+308,"p_50_71", false,-1);
    tracep->declBit(c+309,"g_60_61", false,-1);
    tracep->declBit(c+310,"p_60_61", false,-1);
    tracep->declBit(c+309,"g_60_81", false,-1);
    tracep->declBit(c+310,"p_60_81", false,-1);
    tracep->declBit(c+311,"g_70_71", false,-1);
    tracep->declBit(c+312,"p_70_71", false,-1);
    tracep->declBit(c+313,"g_80_81", false,-1);
    tracep->declBit(c+314,"p_80_81", false,-1);
    tracep->declBit(c+285,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+285,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+315,"g_41_42", false,-1);
    tracep->declBit(c+316,"p_41_42", false,-1);
    tracep->declBit(c+300,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+317,"g_51_52", false,-1);
    tracep->declBit(c+318,"p_51_52", false,-1);
    tracep->declBit(c+319,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+320,"g_61_62", false,-1);
    tracep->declBit(c+321,"p_61_62", false,-1);
    tracep->declBit(c+322,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+323,"g_71_72", false,-1);
    tracep->declBit(c+324,"p_71_72", false,-1);
    tracep->declBit(c+315,"g_41_82", false,-1);
    tracep->declBit(c+316,"p_41_82", false,-1);
    tracep->declBit(c+325,"g_81_82", false,-1);
    tracep->declBit(c+326,"p_81_82", false,-1);
    tracep->declBit(c+285,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+327,"g_82_83", false,-1);
    tracep->declBit(c+328,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+285,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+329,"g_current", false,-1);
    tracep->declBit(c+330,"p_current", false,-1);
    tracep->declBit(c+300,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+300,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+329,"g_previous", false,-1);
    tracep->declBit(c+330,"p_previous", false,-1);
    tracep->declBit(c+331,"g_current", false,-1);
    tracep->declBit(c+332,"p_current", false,-1);
    tracep->declBit(c+301,"g_combined_current", false,-1);
    tracep->declBit(c+302,"p_combined_current", false,-1);
    tracep->declBit(c+301,"g_combined_next", false,-1);
    tracep->declBit(c+302,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+285,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+301,"g_current", false,-1);
    tracep->declBit(c+302,"p_current", false,-1);
    tracep->declBit(c+319,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+319,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+331,"g_previous", false,-1);
    tracep->declBit(c+332,"p_previous", false,-1);
    tracep->declBit(c+333,"g_current", false,-1);
    tracep->declBit(c+334,"p_current", false,-1);
    tracep->declBit(c+303,"g_combined_current", false,-1);
    tracep->declBit(c+304,"p_combined_current", false,-1);
    tracep->declBit(c+303,"g_combined_next", false,-1);
    tracep->declBit(c+304,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+300,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+303,"g_current", false,-1);
    tracep->declBit(c+304,"p_current", false,-1);
    tracep->declBit(c+322,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+322,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+333,"g_previous", false,-1);
    tracep->declBit(c+334,"p_previous", false,-1);
    tracep->declBit(c+335,"g_current", false,-1);
    tracep->declBit(c+336,"p_current", false,-1);
    tracep->declBit(c+305,"g_combined_current", false,-1);
    tracep->declBit(c+306,"p_combined_current", false,-1);
    tracep->declBit(c+305,"g_combined_next", false,-1);
    tracep->declBit(c+306,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+301,"g_previous", false,-1);
    tracep->declBit(c+302,"p_previous", false,-1);
    tracep->declBit(c+305,"g_current", false,-1);
    tracep->declBit(c+306,"p_current", false,-1);
    tracep->declBit(c+315,"g_combined_current", false,-1);
    tracep->declBit(c+316,"p_combined_current", false,-1);
    tracep->declBit(c+315,"g_combined_next", false,-1);
    tracep->declBit(c+316,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+285,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+315,"g_current", false,-1);
    tracep->declBit(c+316,"p_current", false,-1);
    tracep->declBit(c+337,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+337,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+335,"g_previous", false,-1);
    tracep->declBit(c+336,"p_previous", false,-1);
    tracep->declBit(c+338,"g_current", false,-1);
    tracep->declBit(c+339,"p_current", false,-1);
    tracep->declBit(c+307,"g_combined_current", false,-1);
    tracep->declBit(c+308,"p_combined_current", false,-1);
    tracep->declBit(c+307,"g_combined_next", false,-1);
    tracep->declBit(c+308,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+303,"g_previous", false,-1);
    tracep->declBit(c+304,"p_previous", false,-1);
    tracep->declBit(c+307,"g_current", false,-1);
    tracep->declBit(c+308,"p_current", false,-1);
    tracep->declBit(c+317,"g_combined_current", false,-1);
    tracep->declBit(c+318,"p_combined_current", false,-1);
    tracep->declBit(c+317,"g_combined_next", false,-1);
    tracep->declBit(c+318,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+300,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+317,"g_current", false,-1);
    tracep->declBit(c+318,"p_current", false,-1);
    tracep->declBit(c+340,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+340,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+338,"g_previous", false,-1);
    tracep->declBit(c+339,"p_previous", false,-1);
    tracep->declBit(c+341,"g_current", false,-1);
    tracep->declBit(c+342,"p_current", false,-1);
    tracep->declBit(c+309,"g_combined_current", false,-1);
    tracep->declBit(c+310,"p_combined_current", false,-1);
    tracep->declBit(c+309,"g_combined_next", false,-1);
    tracep->declBit(c+310,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+305,"g_previous", false,-1);
    tracep->declBit(c+306,"p_previous", false,-1);
    tracep->declBit(c+309,"g_current", false,-1);
    tracep->declBit(c+310,"p_current", false,-1);
    tracep->declBit(c+320,"g_combined_current", false,-1);
    tracep->declBit(c+321,"p_combined_current", false,-1);
    tracep->declBit(c+320,"g_combined_next", false,-1);
    tracep->declBit(c+321,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+319,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+320,"g_current", false,-1);
    tracep->declBit(c+321,"p_current", false,-1);
    tracep->declBit(c+343,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+343,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+341,"g_previous", false,-1);
    tracep->declBit(c+342,"p_previous", false,-1);
    tracep->declBit(c+344,"g_current", false,-1);
    tracep->declBit(c+345,"p_current", false,-1);
    tracep->declBit(c+311,"g_combined_current", false,-1);
    tracep->declBit(c+312,"p_combined_current", false,-1);
    tracep->declBit(c+311,"g_combined_next", false,-1);
    tracep->declBit(c+312,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+307,"g_previous", false,-1);
    tracep->declBit(c+308,"p_previous", false,-1);
    tracep->declBit(c+311,"g_current", false,-1);
    tracep->declBit(c+312,"p_current", false,-1);
    tracep->declBit(c+323,"g_combined_current", false,-1);
    tracep->declBit(c+324,"p_combined_current", false,-1);
    tracep->declBit(c+323,"g_combined_next", false,-1);
    tracep->declBit(c+324,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+322,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+323,"g_current", false,-1);
    tracep->declBit(c+324,"p_current", false,-1);
    tracep->declBit(c+346,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+346,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+344,"g_previous", false,-1);
    tracep->declBit(c+345,"p_previous", false,-1);
    tracep->declBit(c+347,"g_current", false,-1);
    tracep->declBit(c+348,"p_current", false,-1);
    tracep->declBit(c+313,"g_combined_current", false,-1);
    tracep->declBit(c+314,"p_combined_current", false,-1);
    tracep->declBit(c+313,"g_combined_next", false,-1);
    tracep->declBit(c+314,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+309,"g_previous", false,-1);
    tracep->declBit(c+310,"p_previous", false,-1);
    tracep->declBit(c+313,"g_current", false,-1);
    tracep->declBit(c+314,"p_current", false,-1);
    tracep->declBit(c+325,"g_combined_current", false,-1);
    tracep->declBit(c+326,"p_combined_current", false,-1);
    tracep->declBit(c+325,"g_combined_next", false,-1);
    tracep->declBit(c+326,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+315,"g_previous", false,-1);
    tracep->declBit(c+316,"p_previous", false,-1);
    tracep->declBit(c+325,"g_current", false,-1);
    tracep->declBit(c+326,"p_current", false,-1);
    tracep->declBit(c+327,"g_combined_current", false,-1);
    tracep->declBit(c+328,"p_combined_current", false,-1);
    tracep->declBit(c+327,"g_combined_next", false,-1);
    tracep->declBit(c+328,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+285,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+327,"g_current", false,-1);
    tracep->declBit(c+328,"p_current", false,-1);
    tracep->declBit(c+293,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+293,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+285,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+285,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+285,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+285,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+285,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+285,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+285,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+285,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+285,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+300,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+300,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+300,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+349,"a_part", false,-1, 16,0);
    tracep->declBus(c+350,"b_part", false,-1, 8,0);
    tracep->declBit(c+351,"add_sub", false,-1);
    tracep->declBus(c+352,"rq_part", false,-1, 16,0);
    tracep->declBit(c+353,"nxt_add_sub", false,-1);
    tracep->declBus(c+354,"ax2", false,-1, 16,0);
    tracep->declBus(c+355,"b_final", false,-1, 8,0);
    tracep->declBus(c+356,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+357,"op_a", false,-1, 8,0);
    tracep->declBus(c+355,"op_b", false,-1, 8,0);
    tracep->declBit(c+351,"in_c", false,-1);
    tracep->declBus(c+356,"sum", false,-1, 8,0);
    tracep->declBit(c+358,"out_c", false,-1);
    tracep->declBit(c+359,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+360,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+361,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+351,"input_carry", false,-1);
    tracep->declBus(c+362,"target_element", false,-1, 7,0);
    tracep->declBit(c+359,"output_carry", false,-1);
    tracep->declBus(c+363,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+360,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+361,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+351,"input_carry", false,-1);
    tracep->declBus(c+363,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+359,"output_carry", false,-1);
    tracep->declBus(c+364,"g", false,-1, 7,0);
    tracep->declBus(c+365,"p", false,-1, 7,0);
    tracep->declBit(c+351,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+351,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+366,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+366,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+367,"g_20_21", false,-1);
    tracep->declBit(c+368,"p_20_21", false,-1);
    tracep->declBit(c+367,"g_20_41", false,-1);
    tracep->declBit(c+368,"p_20_41", false,-1);
    tracep->declBit(c+369,"g_30_31", false,-1);
    tracep->declBit(c+370,"p_30_31", false,-1);
    tracep->declBit(c+369,"g_30_51", false,-1);
    tracep->declBit(c+370,"p_30_51", false,-1);
    tracep->declBit(c+371,"g_40_41", false,-1);
    tracep->declBit(c+372,"p_40_41", false,-1);
    tracep->declBit(c+371,"g_40_61", false,-1);
    tracep->declBit(c+372,"p_40_61", false,-1);
    tracep->declBit(c+373,"g_50_51", false,-1);
    tracep->declBit(c+374,"p_50_51", false,-1);
    tracep->declBit(c+373,"g_50_71", false,-1);
    tracep->declBit(c+374,"p_50_71", false,-1);
    tracep->declBit(c+375,"g_60_61", false,-1);
    tracep->declBit(c+376,"p_60_61", false,-1);
    tracep->declBit(c+375,"g_60_81", false,-1);
    tracep->declBit(c+376,"p_60_81", false,-1);
    tracep->declBit(c+377,"g_70_71", false,-1);
    tracep->declBit(c+378,"p_70_71", false,-1);
    tracep->declBit(c+379,"g_80_81", false,-1);
    tracep->declBit(c+380,"p_80_81", false,-1);
    tracep->declBit(c+351,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+351,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+381,"g_41_42", false,-1);
    tracep->declBit(c+382,"p_41_42", false,-1);
    tracep->declBit(c+366,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+383,"g_51_52", false,-1);
    tracep->declBit(c+384,"p_51_52", false,-1);
    tracep->declBit(c+385,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+386,"g_61_62", false,-1);
    tracep->declBit(c+387,"p_61_62", false,-1);
    tracep->declBit(c+388,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+389,"g_71_72", false,-1);
    tracep->declBit(c+390,"p_71_72", false,-1);
    tracep->declBit(c+381,"g_41_82", false,-1);
    tracep->declBit(c+382,"p_41_82", false,-1);
    tracep->declBit(c+391,"g_81_82", false,-1);
    tracep->declBit(c+392,"p_81_82", false,-1);
    tracep->declBit(c+351,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+393,"g_82_83", false,-1);
    tracep->declBit(c+394,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+351,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+395,"g_current", false,-1);
    tracep->declBit(c+396,"p_current", false,-1);
    tracep->declBit(c+366,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+366,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+395,"g_previous", false,-1);
    tracep->declBit(c+396,"p_previous", false,-1);
    tracep->declBit(c+397,"g_current", false,-1);
    tracep->declBit(c+398,"p_current", false,-1);
    tracep->declBit(c+367,"g_combined_current", false,-1);
    tracep->declBit(c+368,"p_combined_current", false,-1);
    tracep->declBit(c+367,"g_combined_next", false,-1);
    tracep->declBit(c+368,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+351,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+367,"g_current", false,-1);
    tracep->declBit(c+368,"p_current", false,-1);
    tracep->declBit(c+385,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+385,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+397,"g_previous", false,-1);
    tracep->declBit(c+398,"p_previous", false,-1);
    tracep->declBit(c+399,"g_current", false,-1);
    tracep->declBit(c+400,"p_current", false,-1);
    tracep->declBit(c+369,"g_combined_current", false,-1);
    tracep->declBit(c+370,"p_combined_current", false,-1);
    tracep->declBit(c+369,"g_combined_next", false,-1);
    tracep->declBit(c+370,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+366,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+369,"g_current", false,-1);
    tracep->declBit(c+370,"p_current", false,-1);
    tracep->declBit(c+388,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+388,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+399,"g_previous", false,-1);
    tracep->declBit(c+400,"p_previous", false,-1);
    tracep->declBit(c+401,"g_current", false,-1);
    tracep->declBit(c+402,"p_current", false,-1);
    tracep->declBit(c+371,"g_combined_current", false,-1);
    tracep->declBit(c+372,"p_combined_current", false,-1);
    tracep->declBit(c+371,"g_combined_next", false,-1);
    tracep->declBit(c+372,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+367,"g_previous", false,-1);
    tracep->declBit(c+368,"p_previous", false,-1);
    tracep->declBit(c+371,"g_current", false,-1);
    tracep->declBit(c+372,"p_current", false,-1);
    tracep->declBit(c+381,"g_combined_current", false,-1);
    tracep->declBit(c+382,"p_combined_current", false,-1);
    tracep->declBit(c+381,"g_combined_next", false,-1);
    tracep->declBit(c+382,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+351,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+381,"g_current", false,-1);
    tracep->declBit(c+382,"p_current", false,-1);
    tracep->declBit(c+403,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+403,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+401,"g_previous", false,-1);
    tracep->declBit(c+402,"p_previous", false,-1);
    tracep->declBit(c+404,"g_current", false,-1);
    tracep->declBit(c+405,"p_current", false,-1);
    tracep->declBit(c+373,"g_combined_current", false,-1);
    tracep->declBit(c+374,"p_combined_current", false,-1);
    tracep->declBit(c+373,"g_combined_next", false,-1);
    tracep->declBit(c+374,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+369,"g_previous", false,-1);
    tracep->declBit(c+370,"p_previous", false,-1);
    tracep->declBit(c+373,"g_current", false,-1);
    tracep->declBit(c+374,"p_current", false,-1);
    tracep->declBit(c+383,"g_combined_current", false,-1);
    tracep->declBit(c+384,"p_combined_current", false,-1);
    tracep->declBit(c+383,"g_combined_next", false,-1);
    tracep->declBit(c+384,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+366,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+383,"g_current", false,-1);
    tracep->declBit(c+384,"p_current", false,-1);
    tracep->declBit(c+406,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+406,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+404,"g_previous", false,-1);
    tracep->declBit(c+405,"p_previous", false,-1);
    tracep->declBit(c+407,"g_current", false,-1);
    tracep->declBit(c+408,"p_current", false,-1);
    tracep->declBit(c+375,"g_combined_current", false,-1);
    tracep->declBit(c+376,"p_combined_current", false,-1);
    tracep->declBit(c+375,"g_combined_next", false,-1);
    tracep->declBit(c+376,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+371,"g_previous", false,-1);
    tracep->declBit(c+372,"p_previous", false,-1);
    tracep->declBit(c+375,"g_current", false,-1);
    tracep->declBit(c+376,"p_current", false,-1);
    tracep->declBit(c+386,"g_combined_current", false,-1);
    tracep->declBit(c+387,"p_combined_current", false,-1);
    tracep->declBit(c+386,"g_combined_next", false,-1);
    tracep->declBit(c+387,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+385,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+386,"g_current", false,-1);
    tracep->declBit(c+387,"p_current", false,-1);
    tracep->declBit(c+409,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+409,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+407,"g_previous", false,-1);
    tracep->declBit(c+408,"p_previous", false,-1);
    tracep->declBit(c+410,"g_current", false,-1);
    tracep->declBit(c+411,"p_current", false,-1);
    tracep->declBit(c+377,"g_combined_current", false,-1);
    tracep->declBit(c+378,"p_combined_current", false,-1);
    tracep->declBit(c+377,"g_combined_next", false,-1);
    tracep->declBit(c+378,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+373,"g_previous", false,-1);
    tracep->declBit(c+374,"p_previous", false,-1);
    tracep->declBit(c+377,"g_current", false,-1);
    tracep->declBit(c+378,"p_current", false,-1);
    tracep->declBit(c+389,"g_combined_current", false,-1);
    tracep->declBit(c+390,"p_combined_current", false,-1);
    tracep->declBit(c+389,"g_combined_next", false,-1);
    tracep->declBit(c+390,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+388,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+389,"g_current", false,-1);
    tracep->declBit(c+390,"p_current", false,-1);
    tracep->declBit(c+412,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+412,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+410,"g_previous", false,-1);
    tracep->declBit(c+411,"p_previous", false,-1);
    tracep->declBit(c+413,"g_current", false,-1);
    tracep->declBit(c+414,"p_current", false,-1);
    tracep->declBit(c+379,"g_combined_current", false,-1);
    tracep->declBit(c+380,"p_combined_current", false,-1);
    tracep->declBit(c+379,"g_combined_next", false,-1);
    tracep->declBit(c+380,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+375,"g_previous", false,-1);
    tracep->declBit(c+376,"p_previous", false,-1);
    tracep->declBit(c+379,"g_current", false,-1);
    tracep->declBit(c+380,"p_current", false,-1);
    tracep->declBit(c+391,"g_combined_current", false,-1);
    tracep->declBit(c+392,"p_combined_current", false,-1);
    tracep->declBit(c+391,"g_combined_next", false,-1);
    tracep->declBit(c+392,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+381,"g_previous", false,-1);
    tracep->declBit(c+382,"p_previous", false,-1);
    tracep->declBit(c+391,"g_current", false,-1);
    tracep->declBit(c+392,"p_current", false,-1);
    tracep->declBit(c+393,"g_combined_current", false,-1);
    tracep->declBit(c+394,"p_combined_current", false,-1);
    tracep->declBit(c+393,"g_combined_next", false,-1);
    tracep->declBit(c+394,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+351,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+393,"g_current", false,-1);
    tracep->declBit(c+394,"p_current", false,-1);
    tracep->declBit(c+359,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+359,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+351,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+351,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+351,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+351,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+351,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+351,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+351,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+351,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+351,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+366,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+366,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+366,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+415,"a_part", false,-1, 16,0);
    tracep->declBus(c+416,"b_part", false,-1, 8,0);
    tracep->declBit(c+417,"add_sub", false,-1);
    tracep->declBus(c+418,"rq_part", false,-1, 16,0);
    tracep->declBit(c+419,"nxt_add_sub", false,-1);
    tracep->declBus(c+420,"ax2", false,-1, 16,0);
    tracep->declBus(c+421,"b_final", false,-1, 8,0);
    tracep->declBus(c+422,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+423,"op_a", false,-1, 8,0);
    tracep->declBus(c+421,"op_b", false,-1, 8,0);
    tracep->declBit(c+417,"in_c", false,-1);
    tracep->declBus(c+422,"sum", false,-1, 8,0);
    tracep->declBit(c+424,"out_c", false,-1);
    tracep->declBit(c+425,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+426,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+427,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+417,"input_carry", false,-1);
    tracep->declBus(c+428,"target_element", false,-1, 7,0);
    tracep->declBit(c+425,"output_carry", false,-1);
    tracep->declBus(c+429,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+426,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+427,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+417,"input_carry", false,-1);
    tracep->declBus(c+429,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+425,"output_carry", false,-1);
    tracep->declBus(c+430,"g", false,-1, 7,0);
    tracep->declBus(c+431,"p", false,-1, 7,0);
    tracep->declBit(c+417,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+417,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+432,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+432,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+433,"g_20_21", false,-1);
    tracep->declBit(c+434,"p_20_21", false,-1);
    tracep->declBit(c+433,"g_20_41", false,-1);
    tracep->declBit(c+434,"p_20_41", false,-1);
    tracep->declBit(c+435,"g_30_31", false,-1);
    tracep->declBit(c+436,"p_30_31", false,-1);
    tracep->declBit(c+435,"g_30_51", false,-1);
    tracep->declBit(c+436,"p_30_51", false,-1);
    tracep->declBit(c+437,"g_40_41", false,-1);
    tracep->declBit(c+438,"p_40_41", false,-1);
    tracep->declBit(c+437,"g_40_61", false,-1);
    tracep->declBit(c+438,"p_40_61", false,-1);
    tracep->declBit(c+439,"g_50_51", false,-1);
    tracep->declBit(c+440,"p_50_51", false,-1);
    tracep->declBit(c+439,"g_50_71", false,-1);
    tracep->declBit(c+440,"p_50_71", false,-1);
    tracep->declBit(c+441,"g_60_61", false,-1);
    tracep->declBit(c+442,"p_60_61", false,-1);
    tracep->declBit(c+441,"g_60_81", false,-1);
    tracep->declBit(c+442,"p_60_81", false,-1);
    tracep->declBit(c+443,"g_70_71", false,-1);
    tracep->declBit(c+444,"p_70_71", false,-1);
    tracep->declBit(c+445,"g_80_81", false,-1);
    tracep->declBit(c+446,"p_80_81", false,-1);
    tracep->declBit(c+417,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+417,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+447,"g_41_42", false,-1);
    tracep->declBit(c+448,"p_41_42", false,-1);
    tracep->declBit(c+432,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+449,"g_51_52", false,-1);
    tracep->declBit(c+450,"p_51_52", false,-1);
    tracep->declBit(c+451,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+452,"g_61_62", false,-1);
    tracep->declBit(c+453,"p_61_62", false,-1);
    tracep->declBit(c+454,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+455,"g_71_72", false,-1);
    tracep->declBit(c+456,"p_71_72", false,-1);
    tracep->declBit(c+447,"g_41_82", false,-1);
    tracep->declBit(c+448,"p_41_82", false,-1);
    tracep->declBit(c+457,"g_81_82", false,-1);
    tracep->declBit(c+458,"p_81_82", false,-1);
    tracep->declBit(c+417,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+459,"g_82_83", false,-1);
    tracep->declBit(c+460,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+417,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+461,"g_current", false,-1);
    tracep->declBit(c+462,"p_current", false,-1);
    tracep->declBit(c+432,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+432,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+461,"g_previous", false,-1);
    tracep->declBit(c+462,"p_previous", false,-1);
    tracep->declBit(c+463,"g_current", false,-1);
    tracep->declBit(c+464,"p_current", false,-1);
    tracep->declBit(c+433,"g_combined_current", false,-1);
    tracep->declBit(c+434,"p_combined_current", false,-1);
    tracep->declBit(c+433,"g_combined_next", false,-1);
    tracep->declBit(c+434,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+417,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+433,"g_current", false,-1);
    tracep->declBit(c+434,"p_current", false,-1);
    tracep->declBit(c+451,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+451,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+463,"g_previous", false,-1);
    tracep->declBit(c+464,"p_previous", false,-1);
    tracep->declBit(c+465,"g_current", false,-1);
    tracep->declBit(c+466,"p_current", false,-1);
    tracep->declBit(c+435,"g_combined_current", false,-1);
    tracep->declBit(c+436,"p_combined_current", false,-1);
    tracep->declBit(c+435,"g_combined_next", false,-1);
    tracep->declBit(c+436,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+432,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+435,"g_current", false,-1);
    tracep->declBit(c+436,"p_current", false,-1);
    tracep->declBit(c+454,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+454,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+465,"g_previous", false,-1);
    tracep->declBit(c+466,"p_previous", false,-1);
    tracep->declBit(c+467,"g_current", false,-1);
    tracep->declBit(c+468,"p_current", false,-1);
    tracep->declBit(c+437,"g_combined_current", false,-1);
    tracep->declBit(c+438,"p_combined_current", false,-1);
    tracep->declBit(c+437,"g_combined_next", false,-1);
    tracep->declBit(c+438,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+433,"g_previous", false,-1);
    tracep->declBit(c+434,"p_previous", false,-1);
    tracep->declBit(c+437,"g_current", false,-1);
    tracep->declBit(c+438,"p_current", false,-1);
    tracep->declBit(c+447,"g_combined_current", false,-1);
    tracep->declBit(c+448,"p_combined_current", false,-1);
    tracep->declBit(c+447,"g_combined_next", false,-1);
    tracep->declBit(c+448,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+417,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+447,"g_current", false,-1);
    tracep->declBit(c+448,"p_current", false,-1);
    tracep->declBit(c+469,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+469,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+467,"g_previous", false,-1);
    tracep->declBit(c+468,"p_previous", false,-1);
    tracep->declBit(c+470,"g_current", false,-1);
    tracep->declBit(c+471,"p_current", false,-1);
    tracep->declBit(c+439,"g_combined_current", false,-1);
    tracep->declBit(c+440,"p_combined_current", false,-1);
    tracep->declBit(c+439,"g_combined_next", false,-1);
    tracep->declBit(c+440,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+435,"g_previous", false,-1);
    tracep->declBit(c+436,"p_previous", false,-1);
    tracep->declBit(c+439,"g_current", false,-1);
    tracep->declBit(c+440,"p_current", false,-1);
    tracep->declBit(c+449,"g_combined_current", false,-1);
    tracep->declBit(c+450,"p_combined_current", false,-1);
    tracep->declBit(c+449,"g_combined_next", false,-1);
    tracep->declBit(c+450,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+432,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+449,"g_current", false,-1);
    tracep->declBit(c+450,"p_current", false,-1);
    tracep->declBit(c+472,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+472,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+470,"g_previous", false,-1);
    tracep->declBit(c+471,"p_previous", false,-1);
    tracep->declBit(c+473,"g_current", false,-1);
    tracep->declBit(c+474,"p_current", false,-1);
    tracep->declBit(c+441,"g_combined_current", false,-1);
    tracep->declBit(c+442,"p_combined_current", false,-1);
    tracep->declBit(c+441,"g_combined_next", false,-1);
    tracep->declBit(c+442,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+437,"g_previous", false,-1);
    tracep->declBit(c+438,"p_previous", false,-1);
    tracep->declBit(c+441,"g_current", false,-1);
    tracep->declBit(c+442,"p_current", false,-1);
    tracep->declBit(c+452,"g_combined_current", false,-1);
    tracep->declBit(c+453,"p_combined_current", false,-1);
    tracep->declBit(c+452,"g_combined_next", false,-1);
    tracep->declBit(c+453,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+451,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+452,"g_current", false,-1);
    tracep->declBit(c+453,"p_current", false,-1);
    tracep->declBit(c+475,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+475,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+473,"g_previous", false,-1);
    tracep->declBit(c+474,"p_previous", false,-1);
    tracep->declBit(c+476,"g_current", false,-1);
    tracep->declBit(c+477,"p_current", false,-1);
    tracep->declBit(c+443,"g_combined_current", false,-1);
    tracep->declBit(c+444,"p_combined_current", false,-1);
    tracep->declBit(c+443,"g_combined_next", false,-1);
    tracep->declBit(c+444,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+439,"g_previous", false,-1);
    tracep->declBit(c+440,"p_previous", false,-1);
    tracep->declBit(c+443,"g_current", false,-1);
    tracep->declBit(c+444,"p_current", false,-1);
    tracep->declBit(c+455,"g_combined_current", false,-1);
    tracep->declBit(c+456,"p_combined_current", false,-1);
    tracep->declBit(c+455,"g_combined_next", false,-1);
    tracep->declBit(c+456,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+454,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+455,"g_current", false,-1);
    tracep->declBit(c+456,"p_current", false,-1);
    tracep->declBit(c+478,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+478,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+476,"g_previous", false,-1);
    tracep->declBit(c+477,"p_previous", false,-1);
    tracep->declBit(c+479,"g_current", false,-1);
    tracep->declBit(c+480,"p_current", false,-1);
    tracep->declBit(c+445,"g_combined_current", false,-1);
    tracep->declBit(c+446,"p_combined_current", false,-1);
    tracep->declBit(c+445,"g_combined_next", false,-1);
    tracep->declBit(c+446,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+441,"g_previous", false,-1);
    tracep->declBit(c+442,"p_previous", false,-1);
    tracep->declBit(c+445,"g_current", false,-1);
    tracep->declBit(c+446,"p_current", false,-1);
    tracep->declBit(c+457,"g_combined_current", false,-1);
    tracep->declBit(c+458,"p_combined_current", false,-1);
    tracep->declBit(c+457,"g_combined_next", false,-1);
    tracep->declBit(c+458,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+447,"g_previous", false,-1);
    tracep->declBit(c+448,"p_previous", false,-1);
    tracep->declBit(c+457,"g_current", false,-1);
    tracep->declBit(c+458,"p_current", false,-1);
    tracep->declBit(c+459,"g_combined_current", false,-1);
    tracep->declBit(c+460,"p_combined_current", false,-1);
    tracep->declBit(c+459,"g_combined_next", false,-1);
    tracep->declBit(c+460,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+417,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+459,"g_current", false,-1);
    tracep->declBit(c+460,"p_current", false,-1);
    tracep->declBit(c+425,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+425,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+417,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+417,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+417,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+417,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+417,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+417,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+417,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+417,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+417,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+432,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+432,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+432,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+481,"a_part", false,-1, 16,0);
    tracep->declBus(c+482,"b_part", false,-1, 8,0);
    tracep->declBit(c+483,"add_sub", false,-1);
    tracep->declBus(c+484,"rq_part", false,-1, 16,0);
    tracep->declBit(c+485,"nxt_add_sub", false,-1);
    tracep->declBus(c+486,"ax2", false,-1, 16,0);
    tracep->declBus(c+487,"b_final", false,-1, 8,0);
    tracep->declBus(c+488,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+489,"op_a", false,-1, 8,0);
    tracep->declBus(c+487,"op_b", false,-1, 8,0);
    tracep->declBit(c+483,"in_c", false,-1);
    tracep->declBus(c+488,"sum", false,-1, 8,0);
    tracep->declBit(c+490,"out_c", false,-1);
    tracep->declBit(c+491,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+492,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+493,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+483,"input_carry", false,-1);
    tracep->declBus(c+494,"target_element", false,-1, 7,0);
    tracep->declBit(c+491,"output_carry", false,-1);
    tracep->declBus(c+495,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+492,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+493,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+483,"input_carry", false,-1);
    tracep->declBus(c+495,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+491,"output_carry", false,-1);
    tracep->declBus(c+496,"g", false,-1, 7,0);
    tracep->declBus(c+497,"p", false,-1, 7,0);
    tracep->declBit(c+483,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+483,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+498,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+498,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+499,"g_20_21", false,-1);
    tracep->declBit(c+500,"p_20_21", false,-1);
    tracep->declBit(c+499,"g_20_41", false,-1);
    tracep->declBit(c+500,"p_20_41", false,-1);
    tracep->declBit(c+501,"g_30_31", false,-1);
    tracep->declBit(c+502,"p_30_31", false,-1);
    tracep->declBit(c+501,"g_30_51", false,-1);
    tracep->declBit(c+502,"p_30_51", false,-1);
    tracep->declBit(c+503,"g_40_41", false,-1);
    tracep->declBit(c+504,"p_40_41", false,-1);
    tracep->declBit(c+503,"g_40_61", false,-1);
    tracep->declBit(c+504,"p_40_61", false,-1);
    tracep->declBit(c+505,"g_50_51", false,-1);
    tracep->declBit(c+506,"p_50_51", false,-1);
    tracep->declBit(c+505,"g_50_71", false,-1);
    tracep->declBit(c+506,"p_50_71", false,-1);
    tracep->declBit(c+507,"g_60_61", false,-1);
    tracep->declBit(c+508,"p_60_61", false,-1);
    tracep->declBit(c+507,"g_60_81", false,-1);
    tracep->declBit(c+508,"p_60_81", false,-1);
    tracep->declBit(c+509,"g_70_71", false,-1);
    tracep->declBit(c+510,"p_70_71", false,-1);
    tracep->declBit(c+511,"g_80_81", false,-1);
    tracep->declBit(c+512,"p_80_81", false,-1);
    tracep->declBit(c+483,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+483,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+513,"g_41_42", false,-1);
    tracep->declBit(c+514,"p_41_42", false,-1);
    tracep->declBit(c+498,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+515,"g_51_52", false,-1);
    tracep->declBit(c+516,"p_51_52", false,-1);
    tracep->declBit(c+517,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+518,"g_61_62", false,-1);
    tracep->declBit(c+519,"p_61_62", false,-1);
    tracep->declBit(c+520,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+521,"g_71_72", false,-1);
    tracep->declBit(c+522,"p_71_72", false,-1);
    tracep->declBit(c+513,"g_41_82", false,-1);
    tracep->declBit(c+514,"p_41_82", false,-1);
    tracep->declBit(c+523,"g_81_82", false,-1);
    tracep->declBit(c+524,"p_81_82", false,-1);
    tracep->declBit(c+483,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+525,"g_82_83", false,-1);
    tracep->declBit(c+526,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+483,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+527,"g_current", false,-1);
    tracep->declBit(c+528,"p_current", false,-1);
    tracep->declBit(c+498,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+498,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+527,"g_previous", false,-1);
    tracep->declBit(c+528,"p_previous", false,-1);
    tracep->declBit(c+529,"g_current", false,-1);
    tracep->declBit(c+530,"p_current", false,-1);
    tracep->declBit(c+499,"g_combined_current", false,-1);
    tracep->declBit(c+500,"p_combined_current", false,-1);
    tracep->declBit(c+499,"g_combined_next", false,-1);
    tracep->declBit(c+500,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+483,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+499,"g_current", false,-1);
    tracep->declBit(c+500,"p_current", false,-1);
    tracep->declBit(c+517,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+517,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+529,"g_previous", false,-1);
    tracep->declBit(c+530,"p_previous", false,-1);
    tracep->declBit(c+531,"g_current", false,-1);
    tracep->declBit(c+532,"p_current", false,-1);
    tracep->declBit(c+501,"g_combined_current", false,-1);
    tracep->declBit(c+502,"p_combined_current", false,-1);
    tracep->declBit(c+501,"g_combined_next", false,-1);
    tracep->declBit(c+502,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+498,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+501,"g_current", false,-1);
    tracep->declBit(c+502,"p_current", false,-1);
    tracep->declBit(c+520,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+520,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+531,"g_previous", false,-1);
    tracep->declBit(c+532,"p_previous", false,-1);
    tracep->declBit(c+533,"g_current", false,-1);
    tracep->declBit(c+534,"p_current", false,-1);
    tracep->declBit(c+503,"g_combined_current", false,-1);
    tracep->declBit(c+504,"p_combined_current", false,-1);
    tracep->declBit(c+503,"g_combined_next", false,-1);
    tracep->declBit(c+504,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+499,"g_previous", false,-1);
    tracep->declBit(c+500,"p_previous", false,-1);
    tracep->declBit(c+503,"g_current", false,-1);
    tracep->declBit(c+504,"p_current", false,-1);
    tracep->declBit(c+513,"g_combined_current", false,-1);
    tracep->declBit(c+514,"p_combined_current", false,-1);
    tracep->declBit(c+513,"g_combined_next", false,-1);
    tracep->declBit(c+514,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+483,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+513,"g_current", false,-1);
    tracep->declBit(c+514,"p_current", false,-1);
    tracep->declBit(c+535,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+535,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+533,"g_previous", false,-1);
    tracep->declBit(c+534,"p_previous", false,-1);
    tracep->declBit(c+536,"g_current", false,-1);
    tracep->declBit(c+537,"p_current", false,-1);
    tracep->declBit(c+505,"g_combined_current", false,-1);
    tracep->declBit(c+506,"p_combined_current", false,-1);
    tracep->declBit(c+505,"g_combined_next", false,-1);
    tracep->declBit(c+506,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+501,"g_previous", false,-1);
    tracep->declBit(c+502,"p_previous", false,-1);
    tracep->declBit(c+505,"g_current", false,-1);
    tracep->declBit(c+506,"p_current", false,-1);
    tracep->declBit(c+515,"g_combined_current", false,-1);
    tracep->declBit(c+516,"p_combined_current", false,-1);
    tracep->declBit(c+515,"g_combined_next", false,-1);
    tracep->declBit(c+516,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+498,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+515,"g_current", false,-1);
    tracep->declBit(c+516,"p_current", false,-1);
    tracep->declBit(c+538,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+538,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+536,"g_previous", false,-1);
    tracep->declBit(c+537,"p_previous", false,-1);
    tracep->declBit(c+539,"g_current", false,-1);
    tracep->declBit(c+540,"p_current", false,-1);
    tracep->declBit(c+507,"g_combined_current", false,-1);
    tracep->declBit(c+508,"p_combined_current", false,-1);
    tracep->declBit(c+507,"g_combined_next", false,-1);
    tracep->declBit(c+508,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+503,"g_previous", false,-1);
    tracep->declBit(c+504,"p_previous", false,-1);
    tracep->declBit(c+507,"g_current", false,-1);
    tracep->declBit(c+508,"p_current", false,-1);
    tracep->declBit(c+518,"g_combined_current", false,-1);
    tracep->declBit(c+519,"p_combined_current", false,-1);
    tracep->declBit(c+518,"g_combined_next", false,-1);
    tracep->declBit(c+519,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+517,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+518,"g_current", false,-1);
    tracep->declBit(c+519,"p_current", false,-1);
    tracep->declBit(c+541,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+541,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+539,"g_previous", false,-1);
    tracep->declBit(c+540,"p_previous", false,-1);
    tracep->declBit(c+542,"g_current", false,-1);
    tracep->declBit(c+543,"p_current", false,-1);
    tracep->declBit(c+509,"g_combined_current", false,-1);
    tracep->declBit(c+510,"p_combined_current", false,-1);
    tracep->declBit(c+509,"g_combined_next", false,-1);
    tracep->declBit(c+510,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+505,"g_previous", false,-1);
    tracep->declBit(c+506,"p_previous", false,-1);
    tracep->declBit(c+509,"g_current", false,-1);
    tracep->declBit(c+510,"p_current", false,-1);
    tracep->declBit(c+521,"g_combined_current", false,-1);
    tracep->declBit(c+522,"p_combined_current", false,-1);
    tracep->declBit(c+521,"g_combined_next", false,-1);
    tracep->declBit(c+522,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+520,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+521,"g_current", false,-1);
    tracep->declBit(c+522,"p_current", false,-1);
    tracep->declBit(c+544,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+544,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+542,"g_previous", false,-1);
    tracep->declBit(c+543,"p_previous", false,-1);
    tracep->declBit(c+545,"g_current", false,-1);
    tracep->declBit(c+546,"p_current", false,-1);
    tracep->declBit(c+511,"g_combined_current", false,-1);
    tracep->declBit(c+512,"p_combined_current", false,-1);
    tracep->declBit(c+511,"g_combined_next", false,-1);
    tracep->declBit(c+512,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+507,"g_previous", false,-1);
    tracep->declBit(c+508,"p_previous", false,-1);
    tracep->declBit(c+511,"g_current", false,-1);
    tracep->declBit(c+512,"p_current", false,-1);
    tracep->declBit(c+523,"g_combined_current", false,-1);
    tracep->declBit(c+524,"p_combined_current", false,-1);
    tracep->declBit(c+523,"g_combined_next", false,-1);
    tracep->declBit(c+524,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+513,"g_previous", false,-1);
    tracep->declBit(c+514,"p_previous", false,-1);
    tracep->declBit(c+523,"g_current", false,-1);
    tracep->declBit(c+524,"p_current", false,-1);
    tracep->declBit(c+525,"g_combined_current", false,-1);
    tracep->declBit(c+526,"p_combined_current", false,-1);
    tracep->declBit(c+525,"g_combined_next", false,-1);
    tracep->declBit(c+526,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+483,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+525,"g_current", false,-1);
    tracep->declBit(c+526,"p_current", false,-1);
    tracep->declBit(c+491,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+491,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+483,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+483,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+483,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+483,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+483,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+483,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+483,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+483,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+483,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+498,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+498,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+498,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+619,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+622,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+547,"a_part", false,-1, 16,0);
    tracep->declBus(c+548,"b_part", false,-1, 8,0);
    tracep->declBit(c+549,"add_sub", false,-1);
    tracep->declBus(c+550,"rq_part", false,-1, 16,0);
    tracep->declBit(c+551,"nxt_add_sub", false,-1);
    tracep->declBus(c+552,"ax2", false,-1, 16,0);
    tracep->declBus(c+553,"b_final", false,-1, 8,0);
    tracep->declBus(c+554,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+620,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+555,"op_a", false,-1, 8,0);
    tracep->declBus(c+553,"op_b", false,-1, 8,0);
    tracep->declBit(c+549,"in_c", false,-1);
    tracep->declBus(c+554,"sum", false,-1, 8,0);
    tracep->declBit(c+556,"out_c", false,-1);
    tracep->declBit(c+557,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+558,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+559,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+549,"input_carry", false,-1);
    tracep->declBus(c+560,"target_element", false,-1, 7,0);
    tracep->declBit(c+557,"output_carry", false,-1);
    tracep->declBus(c+561,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+558,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+559,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+549,"input_carry", false,-1);
    tracep->declBus(c+561,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+557,"output_carry", false,-1);
    tracep->declBus(c+562,"g", false,-1, 7,0);
    tracep->declBus(c+563,"p", false,-1, 7,0);
    tracep->declBit(c+549,"g_00_01", false,-1);
    tracep->declBit(c+621,"p_00_01", false,-1);
    tracep->declBit(c+549,"g_00_21", false,-1);
    tracep->declBit(c+621,"p_00_21", false,-1);
    tracep->declBit(c+564,"g_10_11", false,-1);
    tracep->declBit(c+621,"p_10_11", false,-1);
    tracep->declBit(c+564,"g_10_31", false,-1);
    tracep->declBit(c+621,"p_10_31", false,-1);
    tracep->declBit(c+565,"g_20_21", false,-1);
    tracep->declBit(c+566,"p_20_21", false,-1);
    tracep->declBit(c+565,"g_20_41", false,-1);
    tracep->declBit(c+566,"p_20_41", false,-1);
    tracep->declBit(c+567,"g_30_31", false,-1);
    tracep->declBit(c+568,"p_30_31", false,-1);
    tracep->declBit(c+567,"g_30_51", false,-1);
    tracep->declBit(c+568,"p_30_51", false,-1);
    tracep->declBit(c+569,"g_40_41", false,-1);
    tracep->declBit(c+570,"p_40_41", false,-1);
    tracep->declBit(c+569,"g_40_61", false,-1);
    tracep->declBit(c+570,"p_40_61", false,-1);
    tracep->declBit(c+571,"g_50_51", false,-1);
    tracep->declBit(c+572,"p_50_51", false,-1);
    tracep->declBit(c+571,"g_50_71", false,-1);
    tracep->declBit(c+572,"p_50_71", false,-1);
    tracep->declBit(c+573,"g_60_61", false,-1);
    tracep->declBit(c+574,"p_60_61", false,-1);
    tracep->declBit(c+573,"g_60_81", false,-1);
    tracep->declBit(c+574,"p_60_81", false,-1);
    tracep->declBit(c+575,"g_70_71", false,-1);
    tracep->declBit(c+576,"p_70_71", false,-1);
    tracep->declBit(c+577,"g_80_81", false,-1);
    tracep->declBit(c+578,"p_80_81", false,-1);
    tracep->declBit(c+549,"g_01_02", false,-1);
    tracep->declBit(c+621,"p_01_02", false,-1);
    tracep->declBit(c+549,"g_01_42", false,-1);
    tracep->declBit(c+621,"p_01_42", false,-1);
    tracep->declBit(c+579,"g_41_42", false,-1);
    tracep->declBit(c+580,"p_41_42", false,-1);
    tracep->declBit(c+564,"g_11_52", false,-1);
    tracep->declBit(c+621,"p_11_52", false,-1);
    tracep->declBit(c+581,"g_51_52", false,-1);
    tracep->declBit(c+582,"p_51_52", false,-1);
    tracep->declBit(c+583,"g_21_62", false,-1);
    tracep->declBit(c+621,"p_21_62", false,-1);
    tracep->declBit(c+584,"g_61_62", false,-1);
    tracep->declBit(c+585,"p_61_62", false,-1);
    tracep->declBit(c+586,"g_31_72", false,-1);
    tracep->declBit(c+621,"p_31_72", false,-1);
    tracep->declBit(c+587,"g_71_72", false,-1);
    tracep->declBit(c+588,"p_71_72", false,-1);
    tracep->declBit(c+579,"g_41_82", false,-1);
    tracep->declBit(c+580,"p_41_82", false,-1);
    tracep->declBit(c+589,"g_81_82", false,-1);
    tracep->declBit(c+590,"p_81_82", false,-1);
    tracep->declBit(c+549,"g_02_83", false,-1);
    tracep->declBit(c+621,"p_02_83", false,-1);
    tracep->declBit(c+591,"g_82_83", false,-1);
    tracep->declBit(c+592,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+549,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+593,"g_current", false,-1);
    tracep->declBit(c+594,"p_current", false,-1);
    tracep->declBit(c+564,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+564,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+593,"g_previous", false,-1);
    tracep->declBit(c+594,"p_previous", false,-1);
    tracep->declBit(c+595,"g_current", false,-1);
    tracep->declBit(c+596,"p_current", false,-1);
    tracep->declBit(c+565,"g_combined_current", false,-1);
    tracep->declBit(c+566,"p_combined_current", false,-1);
    tracep->declBit(c+565,"g_combined_next", false,-1);
    tracep->declBit(c+566,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+549,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+565,"g_current", false,-1);
    tracep->declBit(c+566,"p_current", false,-1);
    tracep->declBit(c+583,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+583,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+595,"g_previous", false,-1);
    tracep->declBit(c+596,"p_previous", false,-1);
    tracep->declBit(c+597,"g_current", false,-1);
    tracep->declBit(c+598,"p_current", false,-1);
    tracep->declBit(c+567,"g_combined_current", false,-1);
    tracep->declBit(c+568,"p_combined_current", false,-1);
    tracep->declBit(c+567,"g_combined_next", false,-1);
    tracep->declBit(c+568,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+564,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+567,"g_current", false,-1);
    tracep->declBit(c+568,"p_current", false,-1);
    tracep->declBit(c+586,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+586,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+597,"g_previous", false,-1);
    tracep->declBit(c+598,"p_previous", false,-1);
    tracep->declBit(c+599,"g_current", false,-1);
    tracep->declBit(c+600,"p_current", false,-1);
    tracep->declBit(c+569,"g_combined_current", false,-1);
    tracep->declBit(c+570,"p_combined_current", false,-1);
    tracep->declBit(c+569,"g_combined_next", false,-1);
    tracep->declBit(c+570,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+565,"g_previous", false,-1);
    tracep->declBit(c+566,"p_previous", false,-1);
    tracep->declBit(c+569,"g_current", false,-1);
    tracep->declBit(c+570,"p_current", false,-1);
    tracep->declBit(c+579,"g_combined_current", false,-1);
    tracep->declBit(c+580,"p_combined_current", false,-1);
    tracep->declBit(c+579,"g_combined_next", false,-1);
    tracep->declBit(c+580,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+549,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+579,"g_current", false,-1);
    tracep->declBit(c+580,"p_current", false,-1);
    tracep->declBit(c+601,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+601,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+599,"g_previous", false,-1);
    tracep->declBit(c+600,"p_previous", false,-1);
    tracep->declBit(c+602,"g_current", false,-1);
    tracep->declBit(c+603,"p_current", false,-1);
    tracep->declBit(c+571,"g_combined_current", false,-1);
    tracep->declBit(c+572,"p_combined_current", false,-1);
    tracep->declBit(c+571,"g_combined_next", false,-1);
    tracep->declBit(c+572,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+567,"g_previous", false,-1);
    tracep->declBit(c+568,"p_previous", false,-1);
    tracep->declBit(c+571,"g_current", false,-1);
    tracep->declBit(c+572,"p_current", false,-1);
    tracep->declBit(c+581,"g_combined_current", false,-1);
    tracep->declBit(c+582,"p_combined_current", false,-1);
    tracep->declBit(c+581,"g_combined_next", false,-1);
    tracep->declBit(c+582,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+564,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+581,"g_current", false,-1);
    tracep->declBit(c+582,"p_current", false,-1);
    tracep->declBit(c+604,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+604,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+602,"g_previous", false,-1);
    tracep->declBit(c+603,"p_previous", false,-1);
    tracep->declBit(c+605,"g_current", false,-1);
    tracep->declBit(c+606,"p_current", false,-1);
    tracep->declBit(c+573,"g_combined_current", false,-1);
    tracep->declBit(c+574,"p_combined_current", false,-1);
    tracep->declBit(c+573,"g_combined_next", false,-1);
    tracep->declBit(c+574,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+569,"g_previous", false,-1);
    tracep->declBit(c+570,"p_previous", false,-1);
    tracep->declBit(c+573,"g_current", false,-1);
    tracep->declBit(c+574,"p_current", false,-1);
    tracep->declBit(c+584,"g_combined_current", false,-1);
    tracep->declBit(c+585,"p_combined_current", false,-1);
    tracep->declBit(c+584,"g_combined_next", false,-1);
    tracep->declBit(c+585,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+583,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+584,"g_current", false,-1);
    tracep->declBit(c+585,"p_current", false,-1);
    tracep->declBit(c+607,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+607,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+605,"g_previous", false,-1);
    tracep->declBit(c+606,"p_previous", false,-1);
    tracep->declBit(c+608,"g_current", false,-1);
    tracep->declBit(c+609,"p_current", false,-1);
    tracep->declBit(c+575,"g_combined_current", false,-1);
    tracep->declBit(c+576,"p_combined_current", false,-1);
    tracep->declBit(c+575,"g_combined_next", false,-1);
    tracep->declBit(c+576,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+571,"g_previous", false,-1);
    tracep->declBit(c+572,"p_previous", false,-1);
    tracep->declBit(c+575,"g_current", false,-1);
    tracep->declBit(c+576,"p_current", false,-1);
    tracep->declBit(c+587,"g_combined_current", false,-1);
    tracep->declBit(c+588,"p_combined_current", false,-1);
    tracep->declBit(c+587,"g_combined_next", false,-1);
    tracep->declBit(c+588,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+586,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+587,"g_current", false,-1);
    tracep->declBit(c+588,"p_current", false,-1);
    tracep->declBit(c+610,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+610,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+608,"g_previous", false,-1);
    tracep->declBit(c+609,"p_previous", false,-1);
    tracep->declBit(c+611,"g_current", false,-1);
    tracep->declBit(c+612,"p_current", false,-1);
    tracep->declBit(c+577,"g_combined_current", false,-1);
    tracep->declBit(c+578,"p_combined_current", false,-1);
    tracep->declBit(c+577,"g_combined_next", false,-1);
    tracep->declBit(c+578,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+573,"g_previous", false,-1);
    tracep->declBit(c+574,"p_previous", false,-1);
    tracep->declBit(c+577,"g_current", false,-1);
    tracep->declBit(c+578,"p_current", false,-1);
    tracep->declBit(c+589,"g_combined_current", false,-1);
    tracep->declBit(c+590,"p_combined_current", false,-1);
    tracep->declBit(c+589,"g_combined_next", false,-1);
    tracep->declBit(c+590,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+579,"g_previous", false,-1);
    tracep->declBit(c+580,"p_previous", false,-1);
    tracep->declBit(c+589,"g_current", false,-1);
    tracep->declBit(c+590,"p_current", false,-1);
    tracep->declBit(c+591,"g_combined_current", false,-1);
    tracep->declBit(c+592,"p_combined_current", false,-1);
    tracep->declBit(c+591,"g_combined_next", false,-1);
    tracep->declBit(c+592,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+549,"g_previous", false,-1);
    tracep->declBit(c+621,"p_previous", false,-1);
    tracep->declBit(c+591,"g_current", false,-1);
    tracep->declBit(c+592,"p_current", false,-1);
    tracep->declBit(c+557,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+557,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+549,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+549,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+549,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+549,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+549,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+549,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+549,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+549,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+549,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+564,"g_current", false,-1);
    tracep->declBit(c+621,"p_current", false,-1);
    tracep->declBit(c+564,"g_combined_current", false,-1);
    tracep->declBit(c+621,"p_combined_current", false,-1);
    tracep->declBit(c+564,"g_combined_next", false,-1);
    tracep->declBit(c+621,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void nrdiv___024root__trace_init_top(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_init_top\n"); );
    // Body
    nrdiv___024root__trace_init_sub__TOP__0(vlSelf, tracep);
    tracep->pushNamePrefix("nrdiv ");
    tracep->pushNamePrefix("gen_loop[0] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[1] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[2] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[3] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[4] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[5] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[6] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[7] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    nrdiv___024root__trace_init_sub__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(3);
}

VL_ATTR_COLD void nrdiv___024root__trace_full_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp);
void nrdiv___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp);
void nrdiv___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/);

VL_ATTR_COLD void nrdiv___024root__trace_register(nrdiv___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&nrdiv___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&nrdiv___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&nrdiv___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void nrdiv___024root__trace_full_sub_0(nrdiv___024root* vlSelf, VerilatedVcd::Buffer* bufp);

VL_ATTR_COLD void nrdiv___024root__trace_full_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_full_top_0\n"); );
    // Init
    nrdiv___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<nrdiv___024root*>(voidSelf);
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    nrdiv___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void nrdiv___024root__trace_full_sub_0(nrdiv___024root* vlSelf, VerilatedVcd::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullIData(oldp+1,(vlSelf->nrdiv__DOT__rq_part_ff[0]),17);
    bufp->fullIData(oldp+2,(vlSelf->nrdiv__DOT__rq_part_ff[1]),17);
    bufp->fullIData(oldp+3,(vlSelf->nrdiv__DOT__rq_part_ff[2]),17);
    bufp->fullIData(oldp+4,(vlSelf->nrdiv__DOT__rq_part_ff[3]),17);
    bufp->fullIData(oldp+5,(vlSelf->nrdiv__DOT__rq_part_ff[4]),17);
    bufp->fullIData(oldp+6,(vlSelf->nrdiv__DOT__rq_part_ff[5]),17);
    bufp->fullIData(oldp+7,(vlSelf->nrdiv__DOT__rq_part_ff[6]),17);
    bufp->fullIData(oldp+8,(vlSelf->nrdiv__DOT__rq_part_ff[7]),17);
    bufp->fullSData(oldp+9,(vlSelf->nrdiv__DOT__b_ff[0]),9);
    bufp->fullSData(oldp+10,(vlSelf->nrdiv__DOT__b_ff[1]),9);
    bufp->fullSData(oldp+11,(vlSelf->nrdiv__DOT__b_ff[2]),9);
    bufp->fullSData(oldp+12,(vlSelf->nrdiv__DOT__b_ff[3]),9);
    bufp->fullSData(oldp+13,(vlSelf->nrdiv__DOT__b_ff[4]),9);
    bufp->fullSData(oldp+14,(vlSelf->nrdiv__DOT__b_ff[5]),9);
    bufp->fullSData(oldp+15,(vlSelf->nrdiv__DOT__b_ff[6]),9);
    bufp->fullSData(oldp+16,(vlSelf->nrdiv__DOT__b_ff[7]),9);
    bufp->fullCData(oldp+17,(vlSelf->nrdiv__DOT__nxt_add_sub_ff),8);
    bufp->fullIData(oldp+18,(vlSelf->nrdiv__DOT__rq_part_w[0]),17);
    bufp->fullIData(oldp+19,(vlSelf->nrdiv__DOT__rq_part_w[1]),17);
    bufp->fullIData(oldp+20,(vlSelf->nrdiv__DOT__rq_part_w[2]),17);
    bufp->fullIData(oldp+21,(vlSelf->nrdiv__DOT__rq_part_w[3]),17);
    bufp->fullIData(oldp+22,(vlSelf->nrdiv__DOT__rq_part_w[4]),17);
    bufp->fullIData(oldp+23,(vlSelf->nrdiv__DOT__rq_part_w[5]),17);
    bufp->fullIData(oldp+24,(vlSelf->nrdiv__DOT__rq_part_w[6]),17);
    bufp->fullIData(oldp+25,(vlSelf->nrdiv__DOT__rq_part_w[7]),17);
    bufp->fullCData(oldp+26,(vlSelf->nrdiv__DOT__nxt_add_sub_w),8);
    bufp->fullSData(oldp+27,(vlSelf->nrdiv__DOT__r_corr_w),9);
    bufp->fullSData(oldp+28,(vlSelf->nrdiv__DOT__r_w),9);
    bufp->fullSData(oldp+29,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_w
                                        [7U] >> 8U))),9);
    bufp->fullBit(oldp+30,((1U & (((vlSelf->nrdiv__DOT__rq_part_w
                                    [7U] >> 0x10U) 
                                   & ((IData)(vlSelf->nrdiv__DOT__r_corr_w) 
                                      >> 8U)) | (((
                                                   (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     >> 7U) 
                                                    | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                        >> 6U) 
                                                       & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                          >> 7U))) 
                                                   | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                                  | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                     & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                                 & ((vlSelf->nrdiv__DOT__rq_part_w
                                                     [7U] 
                                                     >> 0x10U) 
                                                    ^ 
                                                    ((IData)(vlSelf->nrdiv__DOT__r_corr_w) 
                                                     >> 8U)))))));
    bufp->fullBit(oldp+31,((1U & (((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                  | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                     & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullCData(oldp+32,((0xffU & (vlSelf->nrdiv__DOT__rq_part_w
                                       [7U] >> 8U))),8);
    bufp->fullCData(oldp+33,((0xffU & (IData)(vlSelf->nrdiv__DOT__r_corr_w))),8);
    bufp->fullCData(oldp+34,((0xffU & (((vlSelf->nrdiv__DOT__rq_part_w
                                         [7U] >> 8U) 
                                        ^ (IData)(vlSelf->nrdiv__DOT__r_corr_w)) 
                                       ^ (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+35,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+36,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+37,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+38,((1U & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+39,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+40,((IData)((3U == (3U & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))))));
    bufp->fullBit(oldp+41,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+42,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+43,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+44,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+45,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+46,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+47,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+48,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+49,((1U & (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 5U) 
                                             & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                >> 6U))))));
    bufp->fullBit(oldp+50,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+51,((1U & (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 6U) 
                                             & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                >> 7U))))));
    bufp->fullBit(oldp+52,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+53,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+54,((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                             & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                >> 1U)) & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41))));
    bufp->fullBit(oldp+55,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                            | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                               & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+56,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                            & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+57,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                            | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                               & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+58,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                            & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+59,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+60,((1U & ((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))) 
                                  | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                     & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+61,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                            & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+62,((1U & ((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))) 
                                  | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                     & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+63,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+64,(((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                              & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                 >> 1U)) & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)) 
                            & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+65,((1U & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+66,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 1U))));
    bufp->fullBit(oldp+67,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 1U))));
    bufp->fullBit(oldp+68,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 2U))));
    bufp->fullBit(oldp+69,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 2U))));
    bufp->fullBit(oldp+70,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 3U))));
    bufp->fullBit(oldp+71,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 3U))));
    bufp->fullBit(oldp+72,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 4U))));
    bufp->fullBit(oldp+73,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 4U))));
    bufp->fullBit(oldp+74,((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                            | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                               & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                  & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+75,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 5U))));
    bufp->fullBit(oldp+76,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 5U))));
    bufp->fullBit(oldp+77,((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                            | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
                               & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                  & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+78,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 6U))));
    bufp->fullBit(oldp+79,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 6U))));
    bufp->fullBit(oldp+80,((1U & (((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                  | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                     & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                        & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+81,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 7U))));
    bufp->fullBit(oldp+82,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 7U))));
    bufp->fullIData(oldp+83,(vlSelf->nrdiv__DOT__unnamedblk1__DOT__i),32);
    bufp->fullIData(oldp+84,(vlSelf->nrdiv__DOT__unnamedblk2__DOT__i),32);
    bufp->fullIData(oldp+85,(vlSelf->nrdiv__DOT__rq_part_ff
                             [0U]),17);
    bufp->fullSData(oldp+86,(vlSelf->nrdiv__DOT__b_ff
                             [0U]),9);
    bufp->fullBit(oldp+87,((1U & (IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff))));
    bufp->fullIData(oldp+88,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                               << 8U) | ((0xfeU & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [0U] 
                                                   << 1U)) 
                                         | (1U & (~ 
                                                  ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                   >> 8U)))))),17);
    bufp->fullBit(oldp+89,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                     >> 8U)))));
    bufp->fullIData(oldp+90,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                          [0U] << 1U))),17);
    bufp->fullSData(oldp+91,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+92,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+93,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [0U] >> 7U))),9);
    bufp->fullBit(oldp+94,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U) 
                                           & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [0U] 
                                              >> 0xfU))) 
                                  | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                     & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [0U] 
                                               >> 0xfU)) 
                                        ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                           >> 8U)))))));
    bufp->fullBit(oldp+95,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                  | ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                     & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+96,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                       [0U] >> 7U))),8);
    bufp->fullCData(oldp+97,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+98,((0xffU & (((0x1ffU & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [0U] 
                                                   >> 7U)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                       ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+99,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+100,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+101,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+102,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+103,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+104,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+105,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+106,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+107,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+108,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+109,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+110,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+111,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+112,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+113,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+114,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+115,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+116,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+117,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+118,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+119,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+120,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+121,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+122,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+123,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+124,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+125,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+126,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+127,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+128,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+129,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+130,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+131,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+132,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+133,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+134,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+135,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+136,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+137,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+138,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+139,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+140,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+141,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+142,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+143,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+144,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+145,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+146,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+147,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+148,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+149,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+150,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+151,(vlSelf->nrdiv__DOT__rq_part_ff
                              [1U]),17);
    bufp->fullSData(oldp+152,(vlSelf->nrdiv__DOT__b_ff
                              [1U]),9);
    bufp->fullBit(oldp+153,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 1U))));
    bufp->fullIData(oldp+154,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [1U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+155,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+156,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [1U] << 1U))),17);
    bufp->fullSData(oldp+157,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+158,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+159,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [1U] >> 7U))),9);
    bufp->fullBit(oldp+160,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [1U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 1U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [1U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+161,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 1U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+162,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [1U] >> 7U))),8);
    bufp->fullCData(oldp+163,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+164,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [1U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+165,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+166,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+167,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+168,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+169,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+170,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+171,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+172,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+173,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+174,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+175,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+176,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+177,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+178,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+179,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+180,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+181,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+182,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+183,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+184,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+185,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+186,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+187,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+188,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+189,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+190,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+191,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+192,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+193,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+194,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+195,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+196,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+197,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+198,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+199,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+200,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+201,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+202,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+203,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+204,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+205,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 1U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+206,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+207,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+208,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+209,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+210,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+211,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+212,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+213,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+214,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+215,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+216,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+217,(vlSelf->nrdiv__DOT__rq_part_ff
                              [2U]),17);
    bufp->fullSData(oldp+218,(vlSelf->nrdiv__DOT__b_ff
                              [2U]),9);
    bufp->fullBit(oldp+219,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 2U))));
    bufp->fullIData(oldp+220,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [2U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+221,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+222,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [2U] << 1U))),17);
    bufp->fullSData(oldp+223,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+224,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+225,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [2U] >> 7U))),9);
    bufp->fullBit(oldp+226,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [2U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 2U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [2U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+227,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 2U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+228,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [2U] >> 7U))),8);
    bufp->fullCData(oldp+229,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+230,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [2U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+231,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+232,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+233,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+234,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+235,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+236,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+237,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+238,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+239,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+240,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+241,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+242,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+243,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+244,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+245,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+246,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+247,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+248,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+249,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+250,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+251,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+252,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+253,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+254,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+255,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+256,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+257,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+258,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+259,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+260,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+261,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+262,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+263,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+264,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+265,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+266,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+267,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+268,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+269,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+270,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+271,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 2U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+272,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+273,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+274,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+275,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+276,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+277,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+278,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+279,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+280,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+281,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+282,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+283,(vlSelf->nrdiv__DOT__rq_part_ff
                              [3U]),17);
    bufp->fullSData(oldp+284,(vlSelf->nrdiv__DOT__b_ff
                              [3U]),9);
    bufp->fullBit(oldp+285,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 3U))));
    bufp->fullIData(oldp+286,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [3U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+287,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+288,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [3U] << 1U))),17);
    bufp->fullSData(oldp+289,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+290,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+291,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [3U] >> 7U))),9);
    bufp->fullBit(oldp+292,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [3U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 3U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [3U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+293,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 3U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+294,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [3U] >> 7U))),8);
    bufp->fullCData(oldp+295,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+296,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [3U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+297,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+298,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+299,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+300,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+301,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+302,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+303,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+304,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+305,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+306,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+307,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+308,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+309,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+310,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+311,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+312,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+313,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+314,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+315,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+316,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+317,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+318,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+319,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+320,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+321,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+322,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+323,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+324,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+325,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+326,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+327,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+328,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+329,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+330,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+331,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+332,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+333,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+334,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+335,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+336,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+337,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 3U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+338,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+339,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+340,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+341,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+342,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+343,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+344,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+345,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+346,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+347,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+348,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+349,(vlSelf->nrdiv__DOT__rq_part_ff
                              [4U]),17);
    bufp->fullSData(oldp+350,(vlSelf->nrdiv__DOT__b_ff
                              [4U]),9);
    bufp->fullBit(oldp+351,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 4U))));
    bufp->fullIData(oldp+352,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [4U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+353,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+354,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [4U] << 1U))),17);
    bufp->fullSData(oldp+355,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+356,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+357,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [4U] >> 7U))),9);
    bufp->fullBit(oldp+358,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [4U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 4U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [4U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+359,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 4U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+360,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [4U] >> 7U))),8);
    bufp->fullCData(oldp+361,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+362,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [4U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+363,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+364,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+365,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+366,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+367,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+368,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+369,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+370,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+371,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+372,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+373,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+374,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+375,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+376,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+377,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+378,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+379,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+380,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+381,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+382,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+383,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+384,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+385,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+386,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+387,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+388,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+389,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+390,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+391,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+392,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+393,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+394,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+395,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+396,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+397,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+398,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+399,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+400,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+401,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+402,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+403,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 4U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+404,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+405,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+406,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+407,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+408,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+409,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+410,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+411,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+412,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+413,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+414,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+415,(vlSelf->nrdiv__DOT__rq_part_ff
                              [5U]),17);
    bufp->fullSData(oldp+416,(vlSelf->nrdiv__DOT__b_ff
                              [5U]),9);
    bufp->fullBit(oldp+417,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 5U))));
    bufp->fullIData(oldp+418,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [5U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+419,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+420,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [5U] << 1U))),17);
    bufp->fullSData(oldp+421,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+422,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+423,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [5U] >> 7U))),9);
    bufp->fullBit(oldp+424,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [5U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 5U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [5U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+425,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 5U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+426,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [5U] >> 7U))),8);
    bufp->fullCData(oldp+427,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+428,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [5U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+429,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+430,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+431,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+432,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+433,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+434,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+435,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+436,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+437,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+438,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+439,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+440,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+441,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+442,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+443,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+444,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+445,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+446,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+447,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+448,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+449,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+450,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+451,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+452,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+453,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+454,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+455,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+456,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+457,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+458,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+459,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+460,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+461,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+462,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+463,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+464,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+465,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+466,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+467,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+468,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+469,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 5U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+470,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+471,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+472,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+473,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+474,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+475,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+476,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+477,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+478,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+479,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+480,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+481,(vlSelf->nrdiv__DOT__rq_part_ff
                              [6U]),17);
    bufp->fullSData(oldp+482,(vlSelf->nrdiv__DOT__b_ff
                              [6U]),9);
    bufp->fullBit(oldp+483,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 6U))));
    bufp->fullIData(oldp+484,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [6U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+485,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+486,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [6U] << 1U))),17);
    bufp->fullSData(oldp+487,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+488,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+489,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [6U] >> 7U))),9);
    bufp->fullBit(oldp+490,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [6U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [6U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+491,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 6U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+492,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [6U] >> 7U))),8);
    bufp->fullCData(oldp+493,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+494,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [6U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+495,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+496,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+497,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+498,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+499,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+500,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+501,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+502,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+503,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+504,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+505,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+506,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+507,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+508,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+509,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+510,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+511,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+512,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+513,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+514,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+515,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+516,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+517,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+518,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+519,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+520,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+521,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+522,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+523,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+524,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+525,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+526,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+527,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+528,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+529,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+530,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+531,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+532,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+533,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+534,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+535,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 6U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+536,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+537,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+538,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+539,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+540,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+541,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+542,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+543,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+544,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+545,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+546,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+547,(vlSelf->nrdiv__DOT__rq_part_ff
                              [7U]),17);
    bufp->fullSData(oldp+548,(vlSelf->nrdiv__DOT__b_ff
                              [7U]),9);
    bufp->fullBit(oldp+549,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   >> 7U))));
    bufp->fullIData(oldp+550,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->nrdiv__DOT__rq_part_ff
                                            [7U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+551,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+552,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [7U] << 1U))),17);
    bufp->fullSData(oldp+553,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+554,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+555,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                         [7U] >> 7U))),9);
    bufp->fullBit(oldp+556,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [7U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                           >> 7U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [7U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+557,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                       >> 7U) & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+558,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                        [7U] >> 7U))),8);
    bufp->fullCData(oldp+559,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+560,((0xffU & (((0x1ffU & 
                                          (vlSelf->nrdiv__DOT__rq_part_ff
                                           [7U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+561,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+562,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+563,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+564,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+565,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+566,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+567,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+568,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+569,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+570,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+571,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+572,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+573,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+574,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+575,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+576,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+577,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+578,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+579,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+580,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+581,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+582,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+583,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+584,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+585,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+586,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+587,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+588,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+589,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+590,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+591,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+592,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+593,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+594,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+595,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+596,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+597,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+598,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+599,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+600,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+601,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                 >> 7U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+602,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+603,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+604,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+605,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+606,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+607,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+608,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+609,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+610,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+611,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+612,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullBit(oldp+613,(vlSelf->clk));
    bufp->fullBit(oldp+614,(vlSelf->nrst));
    bufp->fullIData(oldp+615,(vlSelf->a),17);
    bufp->fullSData(oldp+616,(vlSelf->b),9);
    bufp->fullCData(oldp+617,(vlSelf->q),8);
    bufp->fullCData(oldp+618,(vlSelf->r),8);
    bufp->fullIData(oldp+619,(8U),32);
    bufp->fullIData(oldp+620,(9U),32);
    bufp->fullBit(oldp+621,(0U));
    bufp->fullIData(oldp+622,(0x10U),32);
}
