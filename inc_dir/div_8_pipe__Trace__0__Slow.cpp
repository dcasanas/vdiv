// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "div_8_pipe__Syms.h"


VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBit(c+617,"clk", false,-1);
    tracep->declBit(c+618,"nrst", false,-1);
    tracep->declBus(c+619,"dnd", false,-1, 7,0);
    tracep->declBus(c+620,"div", false,-1, 7,0);
    tracep->declBit(c+621,"sgn", false,-1);
    tracep->declBus(c+622,"q", false,-1, 7,0);
    tracep->declBus(c+623,"r", false,-1, 7,0);
    tracep->pushNamePrefix("div_8_pipe ");
    tracep->declBit(c+617,"clk", false,-1);
    tracep->declBit(c+618,"nrst", false,-1);
    tracep->declBus(c+619,"dnd", false,-1, 7,0);
    tracep->declBus(c+620,"div", false,-1, 7,0);
    tracep->declBit(c+621,"sgn", false,-1);
    tracep->declBus(c+622,"q", false,-1, 7,0);
    tracep->declBus(c+623,"r", false,-1, 7,0);
    tracep->declBus(c+1,"q_w", false,-1, 7,0);
    tracep->declBus(c+2,"r_w", false,-1, 7,0);
    tracep->declBus(c+624,"dnd_2c", false,-1, 7,0);
    tracep->declBus(c+625,"div_2c", false,-1, 7,0);
    tracep->declBus(c+3,"q_2c", false,-1, 7,0);
    tracep->declBus(c+4,"r_2c", false,-1, 7,0);
    tracep->declBus(c+626,"dnd_eff", false,-1, 16,0);
    tracep->declBus(c+627,"div_eff", false,-1, 8,0);
    tracep->pushNamePrefix("nrdiv_inst ");
    tracep->declBus(c+628,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+628,"N_INST", false,-1, 31,0);
    tracep->declBit(c+617,"clk", false,-1);
    tracep->declBit(c+618,"nrst", false,-1);
    tracep->declBus(c+626,"a", false,-1, 16,0);
    tracep->declBus(c+627,"b", false,-1, 8,0);
    tracep->declBus(c+1,"q", false,-1, 7,0);
    tracep->declBus(c+2,"r", false,-1, 7,0);
    for (int i = 0; i < 8; ++i) {
        tracep->declBus(c+5+i*1,"rq_part_ff", true,(i+0), 16,0);
    }
    for (int i = 0; i < 8; ++i) {
        tracep->declBus(c+13+i*1,"b_ff", true,(i+0), 8,0);
    }
    tracep->declBus(c+21,"nxt_add_sub_ff", false,-1, 7,0);
    for (int i = 0; i < 8; ++i) {
        tracep->declBus(c+22+i*1,"rq_part_w", true,(i+0), 16,0);
    }
    tracep->declBus(c+30,"nxt_add_sub_w", false,-1, 7,0);
    tracep->declBus(c+31,"r_corr_w", false,-1, 8,0);
    tracep->declBus(c+32,"r_w", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst_0 ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+33,"op_a", false,-1, 8,0);
    tracep->declBus(c+31,"op_b", false,-1, 8,0);
    tracep->declBit(c+630,"in_c", false,-1);
    tracep->declBus(c+32,"sum", false,-1, 8,0);
    tracep->declBit(c+34,"out_c", false,-1);
    tracep->declBit(c+35,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+36,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+37,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+630,"input_carry", false,-1);
    tracep->declBus(c+38,"target_element", false,-1, 7,0);
    tracep->declBit(c+35,"output_carry", false,-1);
    tracep->declBus(c+39,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+36,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+37,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+630,"input_carry", false,-1);
    tracep->declBus(c+39,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+35,"output_carry", false,-1);
    tracep->declBus(c+40,"g", false,-1, 7,0);
    tracep->declBus(c+41,"p", false,-1, 7,0);
    tracep->declBit(c+630,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+630,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+42,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+42,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+43,"g_20_21", false,-1);
    tracep->declBit(c+44,"p_20_21", false,-1);
    tracep->declBit(c+43,"g_20_41", false,-1);
    tracep->declBit(c+44,"p_20_41", false,-1);
    tracep->declBit(c+45,"g_30_31", false,-1);
    tracep->declBit(c+46,"p_30_31", false,-1);
    tracep->declBit(c+45,"g_30_51", false,-1);
    tracep->declBit(c+46,"p_30_51", false,-1);
    tracep->declBit(c+47,"g_40_41", false,-1);
    tracep->declBit(c+48,"p_40_41", false,-1);
    tracep->declBit(c+47,"g_40_61", false,-1);
    tracep->declBit(c+48,"p_40_61", false,-1);
    tracep->declBit(c+49,"g_50_51", false,-1);
    tracep->declBit(c+50,"p_50_51", false,-1);
    tracep->declBit(c+49,"g_50_71", false,-1);
    tracep->declBit(c+50,"p_50_71", false,-1);
    tracep->declBit(c+51,"g_60_61", false,-1);
    tracep->declBit(c+52,"p_60_61", false,-1);
    tracep->declBit(c+51,"g_60_81", false,-1);
    tracep->declBit(c+52,"p_60_81", false,-1);
    tracep->declBit(c+53,"g_70_71", false,-1);
    tracep->declBit(c+54,"p_70_71", false,-1);
    tracep->declBit(c+55,"g_80_81", false,-1);
    tracep->declBit(c+56,"p_80_81", false,-1);
    tracep->declBit(c+630,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+630,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+57,"g_41_42", false,-1);
    tracep->declBit(c+58,"p_41_42", false,-1);
    tracep->declBit(c+42,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+59,"g_51_52", false,-1);
    tracep->declBit(c+60,"p_51_52", false,-1);
    tracep->declBit(c+43,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+61,"g_61_62", false,-1);
    tracep->declBit(c+62,"p_61_62", false,-1);
    tracep->declBit(c+63,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+64,"g_71_72", false,-1);
    tracep->declBit(c+65,"p_71_72", false,-1);
    tracep->declBit(c+57,"g_41_82", false,-1);
    tracep->declBit(c+58,"p_41_82", false,-1);
    tracep->declBit(c+66,"g_81_82", false,-1);
    tracep->declBit(c+67,"p_81_82", false,-1);
    tracep->declBit(c+630,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+35,"g_82_83", false,-1);
    tracep->declBit(c+68,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+630,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+42,"g_current", false,-1);
    tracep->declBit(c+69,"p_current", false,-1);
    tracep->declBit(c+42,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+42,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+42,"g_previous", false,-1);
    tracep->declBit(c+69,"p_previous", false,-1);
    tracep->declBit(c+70,"g_current", false,-1);
    tracep->declBit(c+71,"p_current", false,-1);
    tracep->declBit(c+43,"g_combined_current", false,-1);
    tracep->declBit(c+44,"p_combined_current", false,-1);
    tracep->declBit(c+43,"g_combined_next", false,-1);
    tracep->declBit(c+44,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+630,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+43,"g_current", false,-1);
    tracep->declBit(c+44,"p_current", false,-1);
    tracep->declBit(c+43,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+43,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+70,"g_previous", false,-1);
    tracep->declBit(c+71,"p_previous", false,-1);
    tracep->declBit(c+72,"g_current", false,-1);
    tracep->declBit(c+73,"p_current", false,-1);
    tracep->declBit(c+45,"g_combined_current", false,-1);
    tracep->declBit(c+46,"p_combined_current", false,-1);
    tracep->declBit(c+45,"g_combined_next", false,-1);
    tracep->declBit(c+46,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+42,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+45,"g_current", false,-1);
    tracep->declBit(c+46,"p_current", false,-1);
    tracep->declBit(c+63,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+63,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+72,"g_previous", false,-1);
    tracep->declBit(c+73,"p_previous", false,-1);
    tracep->declBit(c+74,"g_current", false,-1);
    tracep->declBit(c+75,"p_current", false,-1);
    tracep->declBit(c+47,"g_combined_current", false,-1);
    tracep->declBit(c+48,"p_combined_current", false,-1);
    tracep->declBit(c+47,"g_combined_next", false,-1);
    tracep->declBit(c+48,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+43,"g_previous", false,-1);
    tracep->declBit(c+44,"p_previous", false,-1);
    tracep->declBit(c+47,"g_current", false,-1);
    tracep->declBit(c+48,"p_current", false,-1);
    tracep->declBit(c+57,"g_combined_current", false,-1);
    tracep->declBit(c+58,"p_combined_current", false,-1);
    tracep->declBit(c+57,"g_combined_next", false,-1);
    tracep->declBit(c+58,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+630,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+57,"g_current", false,-1);
    tracep->declBit(c+58,"p_current", false,-1);
    tracep->declBit(c+57,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+57,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+74,"g_previous", false,-1);
    tracep->declBit(c+75,"p_previous", false,-1);
    tracep->declBit(c+76,"g_current", false,-1);
    tracep->declBit(c+77,"p_current", false,-1);
    tracep->declBit(c+49,"g_combined_current", false,-1);
    tracep->declBit(c+50,"p_combined_current", false,-1);
    tracep->declBit(c+49,"g_combined_next", false,-1);
    tracep->declBit(c+50,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+45,"g_previous", false,-1);
    tracep->declBit(c+46,"p_previous", false,-1);
    tracep->declBit(c+49,"g_current", false,-1);
    tracep->declBit(c+50,"p_current", false,-1);
    tracep->declBit(c+59,"g_combined_current", false,-1);
    tracep->declBit(c+60,"p_combined_current", false,-1);
    tracep->declBit(c+59,"g_combined_next", false,-1);
    tracep->declBit(c+60,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+42,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+59,"g_current", false,-1);
    tracep->declBit(c+60,"p_current", false,-1);
    tracep->declBit(c+78,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+78,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+76,"g_previous", false,-1);
    tracep->declBit(c+77,"p_previous", false,-1);
    tracep->declBit(c+79,"g_current", false,-1);
    tracep->declBit(c+80,"p_current", false,-1);
    tracep->declBit(c+51,"g_combined_current", false,-1);
    tracep->declBit(c+52,"p_combined_current", false,-1);
    tracep->declBit(c+51,"g_combined_next", false,-1);
    tracep->declBit(c+52,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+47,"g_previous", false,-1);
    tracep->declBit(c+48,"p_previous", false,-1);
    tracep->declBit(c+51,"g_current", false,-1);
    tracep->declBit(c+52,"p_current", false,-1);
    tracep->declBit(c+61,"g_combined_current", false,-1);
    tracep->declBit(c+62,"p_combined_current", false,-1);
    tracep->declBit(c+61,"g_combined_next", false,-1);
    tracep->declBit(c+62,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+43,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+61,"g_current", false,-1);
    tracep->declBit(c+62,"p_current", false,-1);
    tracep->declBit(c+81,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+81,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+79,"g_previous", false,-1);
    tracep->declBit(c+80,"p_previous", false,-1);
    tracep->declBit(c+82,"g_current", false,-1);
    tracep->declBit(c+83,"p_current", false,-1);
    tracep->declBit(c+53,"g_combined_current", false,-1);
    tracep->declBit(c+54,"p_combined_current", false,-1);
    tracep->declBit(c+53,"g_combined_next", false,-1);
    tracep->declBit(c+54,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+49,"g_previous", false,-1);
    tracep->declBit(c+50,"p_previous", false,-1);
    tracep->declBit(c+53,"g_current", false,-1);
    tracep->declBit(c+54,"p_current", false,-1);
    tracep->declBit(c+64,"g_combined_current", false,-1);
    tracep->declBit(c+65,"p_combined_current", false,-1);
    tracep->declBit(c+64,"g_combined_next", false,-1);
    tracep->declBit(c+65,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+63,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+64,"g_current", false,-1);
    tracep->declBit(c+65,"p_current", false,-1);
    tracep->declBit(c+84,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+84,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+82,"g_previous", false,-1);
    tracep->declBit(c+83,"p_previous", false,-1);
    tracep->declBit(c+85,"g_current", false,-1);
    tracep->declBit(c+86,"p_current", false,-1);
    tracep->declBit(c+55,"g_combined_current", false,-1);
    tracep->declBit(c+56,"p_combined_current", false,-1);
    tracep->declBit(c+55,"g_combined_next", false,-1);
    tracep->declBit(c+56,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+51,"g_previous", false,-1);
    tracep->declBit(c+52,"p_previous", false,-1);
    tracep->declBit(c+55,"g_current", false,-1);
    tracep->declBit(c+56,"p_current", false,-1);
    tracep->declBit(c+66,"g_combined_current", false,-1);
    tracep->declBit(c+67,"p_combined_current", false,-1);
    tracep->declBit(c+66,"g_combined_next", false,-1);
    tracep->declBit(c+67,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+57,"g_previous", false,-1);
    tracep->declBit(c+58,"p_previous", false,-1);
    tracep->declBit(c+66,"g_current", false,-1);
    tracep->declBit(c+67,"p_current", false,-1);
    tracep->declBit(c+35,"g_combined_current", false,-1);
    tracep->declBit(c+68,"p_combined_current", false,-1);
    tracep->declBit(c+35,"g_combined_next", false,-1);
    tracep->declBit(c+68,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+630,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+35,"g_current", false,-1);
    tracep->declBit(c+68,"p_current", false,-1);
    tracep->declBit(c+35,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+35,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+630,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+630,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+630,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+630,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+630,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+630,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+630,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+630,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+630,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+42,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+42,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+42,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
    tracep->pushNamePrefix("unnamedblk1 ");
    tracep->declBus(c+87,"i", false,-1, 31,0);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("unnamedblk2 ");
    tracep->declBus(c+88,"i", false,-1, 31,0);
    tracep->popNamePrefix(3);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+89,"a_part", false,-1, 16,0);
    tracep->declBus(c+90,"b_part", false,-1, 8,0);
    tracep->declBit(c+91,"add_sub", false,-1);
    tracep->declBus(c+92,"rq_part", false,-1, 16,0);
    tracep->declBit(c+93,"nxt_add_sub", false,-1);
    tracep->declBus(c+94,"ax2", false,-1, 16,0);
    tracep->declBus(c+95,"b_final", false,-1, 8,0);
    tracep->declBus(c+96,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+97,"op_a", false,-1, 8,0);
    tracep->declBus(c+95,"op_b", false,-1, 8,0);
    tracep->declBit(c+91,"in_c", false,-1);
    tracep->declBus(c+96,"sum", false,-1, 8,0);
    tracep->declBit(c+98,"out_c", false,-1);
    tracep->declBit(c+99,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+100,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+101,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+91,"input_carry", false,-1);
    tracep->declBus(c+102,"target_element", false,-1, 7,0);
    tracep->declBit(c+99,"output_carry", false,-1);
    tracep->declBus(c+103,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+100,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+101,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+91,"input_carry", false,-1);
    tracep->declBus(c+103,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+99,"output_carry", false,-1);
    tracep->declBus(c+104,"g", false,-1, 7,0);
    tracep->declBus(c+105,"p", false,-1, 7,0);
    tracep->declBit(c+91,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+91,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+106,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+106,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+107,"g_20_21", false,-1);
    tracep->declBit(c+108,"p_20_21", false,-1);
    tracep->declBit(c+107,"g_20_41", false,-1);
    tracep->declBit(c+108,"p_20_41", false,-1);
    tracep->declBit(c+109,"g_30_31", false,-1);
    tracep->declBit(c+110,"p_30_31", false,-1);
    tracep->declBit(c+109,"g_30_51", false,-1);
    tracep->declBit(c+110,"p_30_51", false,-1);
    tracep->declBit(c+111,"g_40_41", false,-1);
    tracep->declBit(c+112,"p_40_41", false,-1);
    tracep->declBit(c+111,"g_40_61", false,-1);
    tracep->declBit(c+112,"p_40_61", false,-1);
    tracep->declBit(c+113,"g_50_51", false,-1);
    tracep->declBit(c+114,"p_50_51", false,-1);
    tracep->declBit(c+113,"g_50_71", false,-1);
    tracep->declBit(c+114,"p_50_71", false,-1);
    tracep->declBit(c+115,"g_60_61", false,-1);
    tracep->declBit(c+116,"p_60_61", false,-1);
    tracep->declBit(c+115,"g_60_81", false,-1);
    tracep->declBit(c+116,"p_60_81", false,-1);
    tracep->declBit(c+117,"g_70_71", false,-1);
    tracep->declBit(c+118,"p_70_71", false,-1);
    tracep->declBit(c+119,"g_80_81", false,-1);
    tracep->declBit(c+120,"p_80_81", false,-1);
    tracep->declBit(c+91,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+91,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+121,"g_41_42", false,-1);
    tracep->declBit(c+122,"p_41_42", false,-1);
    tracep->declBit(c+106,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+123,"g_51_52", false,-1);
    tracep->declBit(c+124,"p_51_52", false,-1);
    tracep->declBit(c+125,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+126,"g_61_62", false,-1);
    tracep->declBit(c+127,"p_61_62", false,-1);
    tracep->declBit(c+128,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+129,"g_71_72", false,-1);
    tracep->declBit(c+130,"p_71_72", false,-1);
    tracep->declBit(c+121,"g_41_82", false,-1);
    tracep->declBit(c+122,"p_41_82", false,-1);
    tracep->declBit(c+131,"g_81_82", false,-1);
    tracep->declBit(c+132,"p_81_82", false,-1);
    tracep->declBit(c+91,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+133,"g_82_83", false,-1);
    tracep->declBit(c+134,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+91,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+135,"g_current", false,-1);
    tracep->declBit(c+136,"p_current", false,-1);
    tracep->declBit(c+106,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+106,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+135,"g_previous", false,-1);
    tracep->declBit(c+136,"p_previous", false,-1);
    tracep->declBit(c+137,"g_current", false,-1);
    tracep->declBit(c+138,"p_current", false,-1);
    tracep->declBit(c+107,"g_combined_current", false,-1);
    tracep->declBit(c+108,"p_combined_current", false,-1);
    tracep->declBit(c+107,"g_combined_next", false,-1);
    tracep->declBit(c+108,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+91,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+107,"g_current", false,-1);
    tracep->declBit(c+108,"p_current", false,-1);
    tracep->declBit(c+125,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+125,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+137,"g_previous", false,-1);
    tracep->declBit(c+138,"p_previous", false,-1);
    tracep->declBit(c+139,"g_current", false,-1);
    tracep->declBit(c+140,"p_current", false,-1);
    tracep->declBit(c+109,"g_combined_current", false,-1);
    tracep->declBit(c+110,"p_combined_current", false,-1);
    tracep->declBit(c+109,"g_combined_next", false,-1);
    tracep->declBit(c+110,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+106,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+109,"g_current", false,-1);
    tracep->declBit(c+110,"p_current", false,-1);
    tracep->declBit(c+128,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+128,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+139,"g_previous", false,-1);
    tracep->declBit(c+140,"p_previous", false,-1);
    tracep->declBit(c+141,"g_current", false,-1);
    tracep->declBit(c+142,"p_current", false,-1);
    tracep->declBit(c+111,"g_combined_current", false,-1);
    tracep->declBit(c+112,"p_combined_current", false,-1);
    tracep->declBit(c+111,"g_combined_next", false,-1);
    tracep->declBit(c+112,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+107,"g_previous", false,-1);
    tracep->declBit(c+108,"p_previous", false,-1);
    tracep->declBit(c+111,"g_current", false,-1);
    tracep->declBit(c+112,"p_current", false,-1);
    tracep->declBit(c+121,"g_combined_current", false,-1);
    tracep->declBit(c+122,"p_combined_current", false,-1);
    tracep->declBit(c+121,"g_combined_next", false,-1);
    tracep->declBit(c+122,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+91,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+121,"g_current", false,-1);
    tracep->declBit(c+122,"p_current", false,-1);
    tracep->declBit(c+143,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+143,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+141,"g_previous", false,-1);
    tracep->declBit(c+142,"p_previous", false,-1);
    tracep->declBit(c+144,"g_current", false,-1);
    tracep->declBit(c+145,"p_current", false,-1);
    tracep->declBit(c+113,"g_combined_current", false,-1);
    tracep->declBit(c+114,"p_combined_current", false,-1);
    tracep->declBit(c+113,"g_combined_next", false,-1);
    tracep->declBit(c+114,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+109,"g_previous", false,-1);
    tracep->declBit(c+110,"p_previous", false,-1);
    tracep->declBit(c+113,"g_current", false,-1);
    tracep->declBit(c+114,"p_current", false,-1);
    tracep->declBit(c+123,"g_combined_current", false,-1);
    tracep->declBit(c+124,"p_combined_current", false,-1);
    tracep->declBit(c+123,"g_combined_next", false,-1);
    tracep->declBit(c+124,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+106,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+123,"g_current", false,-1);
    tracep->declBit(c+124,"p_current", false,-1);
    tracep->declBit(c+146,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+146,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+144,"g_previous", false,-1);
    tracep->declBit(c+145,"p_previous", false,-1);
    tracep->declBit(c+147,"g_current", false,-1);
    tracep->declBit(c+148,"p_current", false,-1);
    tracep->declBit(c+115,"g_combined_current", false,-1);
    tracep->declBit(c+116,"p_combined_current", false,-1);
    tracep->declBit(c+115,"g_combined_next", false,-1);
    tracep->declBit(c+116,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+111,"g_previous", false,-1);
    tracep->declBit(c+112,"p_previous", false,-1);
    tracep->declBit(c+115,"g_current", false,-1);
    tracep->declBit(c+116,"p_current", false,-1);
    tracep->declBit(c+126,"g_combined_current", false,-1);
    tracep->declBit(c+127,"p_combined_current", false,-1);
    tracep->declBit(c+126,"g_combined_next", false,-1);
    tracep->declBit(c+127,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+125,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+126,"g_current", false,-1);
    tracep->declBit(c+127,"p_current", false,-1);
    tracep->declBit(c+149,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+149,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+147,"g_previous", false,-1);
    tracep->declBit(c+148,"p_previous", false,-1);
    tracep->declBit(c+150,"g_current", false,-1);
    tracep->declBit(c+151,"p_current", false,-1);
    tracep->declBit(c+117,"g_combined_current", false,-1);
    tracep->declBit(c+118,"p_combined_current", false,-1);
    tracep->declBit(c+117,"g_combined_next", false,-1);
    tracep->declBit(c+118,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+113,"g_previous", false,-1);
    tracep->declBit(c+114,"p_previous", false,-1);
    tracep->declBit(c+117,"g_current", false,-1);
    tracep->declBit(c+118,"p_current", false,-1);
    tracep->declBit(c+129,"g_combined_current", false,-1);
    tracep->declBit(c+130,"p_combined_current", false,-1);
    tracep->declBit(c+129,"g_combined_next", false,-1);
    tracep->declBit(c+130,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+128,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+129,"g_current", false,-1);
    tracep->declBit(c+130,"p_current", false,-1);
    tracep->declBit(c+152,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+152,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+150,"g_previous", false,-1);
    tracep->declBit(c+151,"p_previous", false,-1);
    tracep->declBit(c+153,"g_current", false,-1);
    tracep->declBit(c+154,"p_current", false,-1);
    tracep->declBit(c+119,"g_combined_current", false,-1);
    tracep->declBit(c+120,"p_combined_current", false,-1);
    tracep->declBit(c+119,"g_combined_next", false,-1);
    tracep->declBit(c+120,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+115,"g_previous", false,-1);
    tracep->declBit(c+116,"p_previous", false,-1);
    tracep->declBit(c+119,"g_current", false,-1);
    tracep->declBit(c+120,"p_current", false,-1);
    tracep->declBit(c+131,"g_combined_current", false,-1);
    tracep->declBit(c+132,"p_combined_current", false,-1);
    tracep->declBit(c+131,"g_combined_next", false,-1);
    tracep->declBit(c+132,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+121,"g_previous", false,-1);
    tracep->declBit(c+122,"p_previous", false,-1);
    tracep->declBit(c+131,"g_current", false,-1);
    tracep->declBit(c+132,"p_current", false,-1);
    tracep->declBit(c+133,"g_combined_current", false,-1);
    tracep->declBit(c+134,"p_combined_current", false,-1);
    tracep->declBit(c+133,"g_combined_next", false,-1);
    tracep->declBit(c+134,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+91,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+133,"g_current", false,-1);
    tracep->declBit(c+134,"p_current", false,-1);
    tracep->declBit(c+99,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+99,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+91,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+91,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+91,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+91,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+91,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+91,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+91,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+91,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+91,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+106,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+106,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+106,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+155,"a_part", false,-1, 16,0);
    tracep->declBus(c+156,"b_part", false,-1, 8,0);
    tracep->declBit(c+157,"add_sub", false,-1);
    tracep->declBus(c+158,"rq_part", false,-1, 16,0);
    tracep->declBit(c+159,"nxt_add_sub", false,-1);
    tracep->declBus(c+160,"ax2", false,-1, 16,0);
    tracep->declBus(c+161,"b_final", false,-1, 8,0);
    tracep->declBus(c+162,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+163,"op_a", false,-1, 8,0);
    tracep->declBus(c+161,"op_b", false,-1, 8,0);
    tracep->declBit(c+157,"in_c", false,-1);
    tracep->declBus(c+162,"sum", false,-1, 8,0);
    tracep->declBit(c+164,"out_c", false,-1);
    tracep->declBit(c+165,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+166,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+167,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+157,"input_carry", false,-1);
    tracep->declBus(c+168,"target_element", false,-1, 7,0);
    tracep->declBit(c+165,"output_carry", false,-1);
    tracep->declBus(c+169,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+166,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+167,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+157,"input_carry", false,-1);
    tracep->declBus(c+169,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+165,"output_carry", false,-1);
    tracep->declBus(c+170,"g", false,-1, 7,0);
    tracep->declBus(c+171,"p", false,-1, 7,0);
    tracep->declBit(c+157,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+157,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+172,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+172,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+173,"g_20_21", false,-1);
    tracep->declBit(c+174,"p_20_21", false,-1);
    tracep->declBit(c+173,"g_20_41", false,-1);
    tracep->declBit(c+174,"p_20_41", false,-1);
    tracep->declBit(c+175,"g_30_31", false,-1);
    tracep->declBit(c+176,"p_30_31", false,-1);
    tracep->declBit(c+175,"g_30_51", false,-1);
    tracep->declBit(c+176,"p_30_51", false,-1);
    tracep->declBit(c+177,"g_40_41", false,-1);
    tracep->declBit(c+178,"p_40_41", false,-1);
    tracep->declBit(c+177,"g_40_61", false,-1);
    tracep->declBit(c+178,"p_40_61", false,-1);
    tracep->declBit(c+179,"g_50_51", false,-1);
    tracep->declBit(c+180,"p_50_51", false,-1);
    tracep->declBit(c+179,"g_50_71", false,-1);
    tracep->declBit(c+180,"p_50_71", false,-1);
    tracep->declBit(c+181,"g_60_61", false,-1);
    tracep->declBit(c+182,"p_60_61", false,-1);
    tracep->declBit(c+181,"g_60_81", false,-1);
    tracep->declBit(c+182,"p_60_81", false,-1);
    tracep->declBit(c+183,"g_70_71", false,-1);
    tracep->declBit(c+184,"p_70_71", false,-1);
    tracep->declBit(c+185,"g_80_81", false,-1);
    tracep->declBit(c+186,"p_80_81", false,-1);
    tracep->declBit(c+157,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+157,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+187,"g_41_42", false,-1);
    tracep->declBit(c+188,"p_41_42", false,-1);
    tracep->declBit(c+172,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+189,"g_51_52", false,-1);
    tracep->declBit(c+190,"p_51_52", false,-1);
    tracep->declBit(c+191,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+192,"g_61_62", false,-1);
    tracep->declBit(c+193,"p_61_62", false,-1);
    tracep->declBit(c+194,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+195,"g_71_72", false,-1);
    tracep->declBit(c+196,"p_71_72", false,-1);
    tracep->declBit(c+187,"g_41_82", false,-1);
    tracep->declBit(c+188,"p_41_82", false,-1);
    tracep->declBit(c+197,"g_81_82", false,-1);
    tracep->declBit(c+198,"p_81_82", false,-1);
    tracep->declBit(c+157,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+199,"g_82_83", false,-1);
    tracep->declBit(c+200,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+157,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+201,"g_current", false,-1);
    tracep->declBit(c+202,"p_current", false,-1);
    tracep->declBit(c+172,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+172,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+201,"g_previous", false,-1);
    tracep->declBit(c+202,"p_previous", false,-1);
    tracep->declBit(c+203,"g_current", false,-1);
    tracep->declBit(c+204,"p_current", false,-1);
    tracep->declBit(c+173,"g_combined_current", false,-1);
    tracep->declBit(c+174,"p_combined_current", false,-1);
    tracep->declBit(c+173,"g_combined_next", false,-1);
    tracep->declBit(c+174,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+157,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+173,"g_current", false,-1);
    tracep->declBit(c+174,"p_current", false,-1);
    tracep->declBit(c+191,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+191,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+203,"g_previous", false,-1);
    tracep->declBit(c+204,"p_previous", false,-1);
    tracep->declBit(c+205,"g_current", false,-1);
    tracep->declBit(c+206,"p_current", false,-1);
    tracep->declBit(c+175,"g_combined_current", false,-1);
    tracep->declBit(c+176,"p_combined_current", false,-1);
    tracep->declBit(c+175,"g_combined_next", false,-1);
    tracep->declBit(c+176,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+172,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+175,"g_current", false,-1);
    tracep->declBit(c+176,"p_current", false,-1);
    tracep->declBit(c+194,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+194,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+205,"g_previous", false,-1);
    tracep->declBit(c+206,"p_previous", false,-1);
    tracep->declBit(c+207,"g_current", false,-1);
    tracep->declBit(c+208,"p_current", false,-1);
    tracep->declBit(c+177,"g_combined_current", false,-1);
    tracep->declBit(c+178,"p_combined_current", false,-1);
    tracep->declBit(c+177,"g_combined_next", false,-1);
    tracep->declBit(c+178,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+173,"g_previous", false,-1);
    tracep->declBit(c+174,"p_previous", false,-1);
    tracep->declBit(c+177,"g_current", false,-1);
    tracep->declBit(c+178,"p_current", false,-1);
    tracep->declBit(c+187,"g_combined_current", false,-1);
    tracep->declBit(c+188,"p_combined_current", false,-1);
    tracep->declBit(c+187,"g_combined_next", false,-1);
    tracep->declBit(c+188,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+157,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+187,"g_current", false,-1);
    tracep->declBit(c+188,"p_current", false,-1);
    tracep->declBit(c+209,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+209,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+207,"g_previous", false,-1);
    tracep->declBit(c+208,"p_previous", false,-1);
    tracep->declBit(c+210,"g_current", false,-1);
    tracep->declBit(c+211,"p_current", false,-1);
    tracep->declBit(c+179,"g_combined_current", false,-1);
    tracep->declBit(c+180,"p_combined_current", false,-1);
    tracep->declBit(c+179,"g_combined_next", false,-1);
    tracep->declBit(c+180,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+175,"g_previous", false,-1);
    tracep->declBit(c+176,"p_previous", false,-1);
    tracep->declBit(c+179,"g_current", false,-1);
    tracep->declBit(c+180,"p_current", false,-1);
    tracep->declBit(c+189,"g_combined_current", false,-1);
    tracep->declBit(c+190,"p_combined_current", false,-1);
    tracep->declBit(c+189,"g_combined_next", false,-1);
    tracep->declBit(c+190,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+172,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+189,"g_current", false,-1);
    tracep->declBit(c+190,"p_current", false,-1);
    tracep->declBit(c+212,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+212,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+210,"g_previous", false,-1);
    tracep->declBit(c+211,"p_previous", false,-1);
    tracep->declBit(c+213,"g_current", false,-1);
    tracep->declBit(c+214,"p_current", false,-1);
    tracep->declBit(c+181,"g_combined_current", false,-1);
    tracep->declBit(c+182,"p_combined_current", false,-1);
    tracep->declBit(c+181,"g_combined_next", false,-1);
    tracep->declBit(c+182,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+177,"g_previous", false,-1);
    tracep->declBit(c+178,"p_previous", false,-1);
    tracep->declBit(c+181,"g_current", false,-1);
    tracep->declBit(c+182,"p_current", false,-1);
    tracep->declBit(c+192,"g_combined_current", false,-1);
    tracep->declBit(c+193,"p_combined_current", false,-1);
    tracep->declBit(c+192,"g_combined_next", false,-1);
    tracep->declBit(c+193,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+191,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+192,"g_current", false,-1);
    tracep->declBit(c+193,"p_current", false,-1);
    tracep->declBit(c+215,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+215,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+213,"g_previous", false,-1);
    tracep->declBit(c+214,"p_previous", false,-1);
    tracep->declBit(c+216,"g_current", false,-1);
    tracep->declBit(c+217,"p_current", false,-1);
    tracep->declBit(c+183,"g_combined_current", false,-1);
    tracep->declBit(c+184,"p_combined_current", false,-1);
    tracep->declBit(c+183,"g_combined_next", false,-1);
    tracep->declBit(c+184,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+179,"g_previous", false,-1);
    tracep->declBit(c+180,"p_previous", false,-1);
    tracep->declBit(c+183,"g_current", false,-1);
    tracep->declBit(c+184,"p_current", false,-1);
    tracep->declBit(c+195,"g_combined_current", false,-1);
    tracep->declBit(c+196,"p_combined_current", false,-1);
    tracep->declBit(c+195,"g_combined_next", false,-1);
    tracep->declBit(c+196,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+194,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+195,"g_current", false,-1);
    tracep->declBit(c+196,"p_current", false,-1);
    tracep->declBit(c+218,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+218,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+216,"g_previous", false,-1);
    tracep->declBit(c+217,"p_previous", false,-1);
    tracep->declBit(c+219,"g_current", false,-1);
    tracep->declBit(c+220,"p_current", false,-1);
    tracep->declBit(c+185,"g_combined_current", false,-1);
    tracep->declBit(c+186,"p_combined_current", false,-1);
    tracep->declBit(c+185,"g_combined_next", false,-1);
    tracep->declBit(c+186,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+181,"g_previous", false,-1);
    tracep->declBit(c+182,"p_previous", false,-1);
    tracep->declBit(c+185,"g_current", false,-1);
    tracep->declBit(c+186,"p_current", false,-1);
    tracep->declBit(c+197,"g_combined_current", false,-1);
    tracep->declBit(c+198,"p_combined_current", false,-1);
    tracep->declBit(c+197,"g_combined_next", false,-1);
    tracep->declBit(c+198,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+187,"g_previous", false,-1);
    tracep->declBit(c+188,"p_previous", false,-1);
    tracep->declBit(c+197,"g_current", false,-1);
    tracep->declBit(c+198,"p_current", false,-1);
    tracep->declBit(c+199,"g_combined_current", false,-1);
    tracep->declBit(c+200,"p_combined_current", false,-1);
    tracep->declBit(c+199,"g_combined_next", false,-1);
    tracep->declBit(c+200,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+157,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+199,"g_current", false,-1);
    tracep->declBit(c+200,"p_current", false,-1);
    tracep->declBit(c+165,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+165,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+157,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+157,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+157,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+157,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+157,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+157,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+157,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+157,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+157,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+172,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+172,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+172,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+221,"a_part", false,-1, 16,0);
    tracep->declBus(c+222,"b_part", false,-1, 8,0);
    tracep->declBit(c+223,"add_sub", false,-1);
    tracep->declBus(c+224,"rq_part", false,-1, 16,0);
    tracep->declBit(c+225,"nxt_add_sub", false,-1);
    tracep->declBus(c+226,"ax2", false,-1, 16,0);
    tracep->declBus(c+227,"b_final", false,-1, 8,0);
    tracep->declBus(c+228,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+229,"op_a", false,-1, 8,0);
    tracep->declBus(c+227,"op_b", false,-1, 8,0);
    tracep->declBit(c+223,"in_c", false,-1);
    tracep->declBus(c+228,"sum", false,-1, 8,0);
    tracep->declBit(c+230,"out_c", false,-1);
    tracep->declBit(c+231,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+232,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+233,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+223,"input_carry", false,-1);
    tracep->declBus(c+234,"target_element", false,-1, 7,0);
    tracep->declBit(c+231,"output_carry", false,-1);
    tracep->declBus(c+235,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+232,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+233,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+223,"input_carry", false,-1);
    tracep->declBus(c+235,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+231,"output_carry", false,-1);
    tracep->declBus(c+236,"g", false,-1, 7,0);
    tracep->declBus(c+237,"p", false,-1, 7,0);
    tracep->declBit(c+223,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+223,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+238,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+238,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+239,"g_20_21", false,-1);
    tracep->declBit(c+240,"p_20_21", false,-1);
    tracep->declBit(c+239,"g_20_41", false,-1);
    tracep->declBit(c+240,"p_20_41", false,-1);
    tracep->declBit(c+241,"g_30_31", false,-1);
    tracep->declBit(c+242,"p_30_31", false,-1);
    tracep->declBit(c+241,"g_30_51", false,-1);
    tracep->declBit(c+242,"p_30_51", false,-1);
    tracep->declBit(c+243,"g_40_41", false,-1);
    tracep->declBit(c+244,"p_40_41", false,-1);
    tracep->declBit(c+243,"g_40_61", false,-1);
    tracep->declBit(c+244,"p_40_61", false,-1);
    tracep->declBit(c+245,"g_50_51", false,-1);
    tracep->declBit(c+246,"p_50_51", false,-1);
    tracep->declBit(c+245,"g_50_71", false,-1);
    tracep->declBit(c+246,"p_50_71", false,-1);
    tracep->declBit(c+247,"g_60_61", false,-1);
    tracep->declBit(c+248,"p_60_61", false,-1);
    tracep->declBit(c+247,"g_60_81", false,-1);
    tracep->declBit(c+248,"p_60_81", false,-1);
    tracep->declBit(c+249,"g_70_71", false,-1);
    tracep->declBit(c+250,"p_70_71", false,-1);
    tracep->declBit(c+251,"g_80_81", false,-1);
    tracep->declBit(c+252,"p_80_81", false,-1);
    tracep->declBit(c+223,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+223,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+253,"g_41_42", false,-1);
    tracep->declBit(c+254,"p_41_42", false,-1);
    tracep->declBit(c+238,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+255,"g_51_52", false,-1);
    tracep->declBit(c+256,"p_51_52", false,-1);
    tracep->declBit(c+257,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+258,"g_61_62", false,-1);
    tracep->declBit(c+259,"p_61_62", false,-1);
    tracep->declBit(c+260,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+261,"g_71_72", false,-1);
    tracep->declBit(c+262,"p_71_72", false,-1);
    tracep->declBit(c+253,"g_41_82", false,-1);
    tracep->declBit(c+254,"p_41_82", false,-1);
    tracep->declBit(c+263,"g_81_82", false,-1);
    tracep->declBit(c+264,"p_81_82", false,-1);
    tracep->declBit(c+223,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+265,"g_82_83", false,-1);
    tracep->declBit(c+266,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+223,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+267,"g_current", false,-1);
    tracep->declBit(c+268,"p_current", false,-1);
    tracep->declBit(c+238,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+238,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+267,"g_previous", false,-1);
    tracep->declBit(c+268,"p_previous", false,-1);
    tracep->declBit(c+269,"g_current", false,-1);
    tracep->declBit(c+270,"p_current", false,-1);
    tracep->declBit(c+239,"g_combined_current", false,-1);
    tracep->declBit(c+240,"p_combined_current", false,-1);
    tracep->declBit(c+239,"g_combined_next", false,-1);
    tracep->declBit(c+240,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+223,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+239,"g_current", false,-1);
    tracep->declBit(c+240,"p_current", false,-1);
    tracep->declBit(c+257,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+257,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+269,"g_previous", false,-1);
    tracep->declBit(c+270,"p_previous", false,-1);
    tracep->declBit(c+271,"g_current", false,-1);
    tracep->declBit(c+272,"p_current", false,-1);
    tracep->declBit(c+241,"g_combined_current", false,-1);
    tracep->declBit(c+242,"p_combined_current", false,-1);
    tracep->declBit(c+241,"g_combined_next", false,-1);
    tracep->declBit(c+242,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+238,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+241,"g_current", false,-1);
    tracep->declBit(c+242,"p_current", false,-1);
    tracep->declBit(c+260,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+260,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+271,"g_previous", false,-1);
    tracep->declBit(c+272,"p_previous", false,-1);
    tracep->declBit(c+273,"g_current", false,-1);
    tracep->declBit(c+274,"p_current", false,-1);
    tracep->declBit(c+243,"g_combined_current", false,-1);
    tracep->declBit(c+244,"p_combined_current", false,-1);
    tracep->declBit(c+243,"g_combined_next", false,-1);
    tracep->declBit(c+244,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+239,"g_previous", false,-1);
    tracep->declBit(c+240,"p_previous", false,-1);
    tracep->declBit(c+243,"g_current", false,-1);
    tracep->declBit(c+244,"p_current", false,-1);
    tracep->declBit(c+253,"g_combined_current", false,-1);
    tracep->declBit(c+254,"p_combined_current", false,-1);
    tracep->declBit(c+253,"g_combined_next", false,-1);
    tracep->declBit(c+254,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+223,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+253,"g_current", false,-1);
    tracep->declBit(c+254,"p_current", false,-1);
    tracep->declBit(c+275,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+275,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+273,"g_previous", false,-1);
    tracep->declBit(c+274,"p_previous", false,-1);
    tracep->declBit(c+276,"g_current", false,-1);
    tracep->declBit(c+277,"p_current", false,-1);
    tracep->declBit(c+245,"g_combined_current", false,-1);
    tracep->declBit(c+246,"p_combined_current", false,-1);
    tracep->declBit(c+245,"g_combined_next", false,-1);
    tracep->declBit(c+246,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+241,"g_previous", false,-1);
    tracep->declBit(c+242,"p_previous", false,-1);
    tracep->declBit(c+245,"g_current", false,-1);
    tracep->declBit(c+246,"p_current", false,-1);
    tracep->declBit(c+255,"g_combined_current", false,-1);
    tracep->declBit(c+256,"p_combined_current", false,-1);
    tracep->declBit(c+255,"g_combined_next", false,-1);
    tracep->declBit(c+256,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+238,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+255,"g_current", false,-1);
    tracep->declBit(c+256,"p_current", false,-1);
    tracep->declBit(c+278,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+278,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+276,"g_previous", false,-1);
    tracep->declBit(c+277,"p_previous", false,-1);
    tracep->declBit(c+279,"g_current", false,-1);
    tracep->declBit(c+280,"p_current", false,-1);
    tracep->declBit(c+247,"g_combined_current", false,-1);
    tracep->declBit(c+248,"p_combined_current", false,-1);
    tracep->declBit(c+247,"g_combined_next", false,-1);
    tracep->declBit(c+248,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+243,"g_previous", false,-1);
    tracep->declBit(c+244,"p_previous", false,-1);
    tracep->declBit(c+247,"g_current", false,-1);
    tracep->declBit(c+248,"p_current", false,-1);
    tracep->declBit(c+258,"g_combined_current", false,-1);
    tracep->declBit(c+259,"p_combined_current", false,-1);
    tracep->declBit(c+258,"g_combined_next", false,-1);
    tracep->declBit(c+259,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+257,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+258,"g_current", false,-1);
    tracep->declBit(c+259,"p_current", false,-1);
    tracep->declBit(c+281,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+281,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+279,"g_previous", false,-1);
    tracep->declBit(c+280,"p_previous", false,-1);
    tracep->declBit(c+282,"g_current", false,-1);
    tracep->declBit(c+283,"p_current", false,-1);
    tracep->declBit(c+249,"g_combined_current", false,-1);
    tracep->declBit(c+250,"p_combined_current", false,-1);
    tracep->declBit(c+249,"g_combined_next", false,-1);
    tracep->declBit(c+250,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+245,"g_previous", false,-1);
    tracep->declBit(c+246,"p_previous", false,-1);
    tracep->declBit(c+249,"g_current", false,-1);
    tracep->declBit(c+250,"p_current", false,-1);
    tracep->declBit(c+261,"g_combined_current", false,-1);
    tracep->declBit(c+262,"p_combined_current", false,-1);
    tracep->declBit(c+261,"g_combined_next", false,-1);
    tracep->declBit(c+262,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+260,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+261,"g_current", false,-1);
    tracep->declBit(c+262,"p_current", false,-1);
    tracep->declBit(c+284,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+284,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+282,"g_previous", false,-1);
    tracep->declBit(c+283,"p_previous", false,-1);
    tracep->declBit(c+285,"g_current", false,-1);
    tracep->declBit(c+286,"p_current", false,-1);
    tracep->declBit(c+251,"g_combined_current", false,-1);
    tracep->declBit(c+252,"p_combined_current", false,-1);
    tracep->declBit(c+251,"g_combined_next", false,-1);
    tracep->declBit(c+252,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+247,"g_previous", false,-1);
    tracep->declBit(c+248,"p_previous", false,-1);
    tracep->declBit(c+251,"g_current", false,-1);
    tracep->declBit(c+252,"p_current", false,-1);
    tracep->declBit(c+263,"g_combined_current", false,-1);
    tracep->declBit(c+264,"p_combined_current", false,-1);
    tracep->declBit(c+263,"g_combined_next", false,-1);
    tracep->declBit(c+264,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+253,"g_previous", false,-1);
    tracep->declBit(c+254,"p_previous", false,-1);
    tracep->declBit(c+263,"g_current", false,-1);
    tracep->declBit(c+264,"p_current", false,-1);
    tracep->declBit(c+265,"g_combined_current", false,-1);
    tracep->declBit(c+266,"p_combined_current", false,-1);
    tracep->declBit(c+265,"g_combined_next", false,-1);
    tracep->declBit(c+266,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+223,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+265,"g_current", false,-1);
    tracep->declBit(c+266,"p_current", false,-1);
    tracep->declBit(c+231,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+231,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+223,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+223,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+223,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+223,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+223,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+223,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+223,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+223,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+223,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+238,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+238,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+238,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+287,"a_part", false,-1, 16,0);
    tracep->declBus(c+288,"b_part", false,-1, 8,0);
    tracep->declBit(c+289,"add_sub", false,-1);
    tracep->declBus(c+290,"rq_part", false,-1, 16,0);
    tracep->declBit(c+291,"nxt_add_sub", false,-1);
    tracep->declBus(c+292,"ax2", false,-1, 16,0);
    tracep->declBus(c+293,"b_final", false,-1, 8,0);
    tracep->declBus(c+294,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+295,"op_a", false,-1, 8,0);
    tracep->declBus(c+293,"op_b", false,-1, 8,0);
    tracep->declBit(c+289,"in_c", false,-1);
    tracep->declBus(c+294,"sum", false,-1, 8,0);
    tracep->declBit(c+296,"out_c", false,-1);
    tracep->declBit(c+297,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+298,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+299,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+289,"input_carry", false,-1);
    tracep->declBus(c+300,"target_element", false,-1, 7,0);
    tracep->declBit(c+297,"output_carry", false,-1);
    tracep->declBus(c+301,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+298,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+299,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+289,"input_carry", false,-1);
    tracep->declBus(c+301,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+297,"output_carry", false,-1);
    tracep->declBus(c+302,"g", false,-1, 7,0);
    tracep->declBus(c+303,"p", false,-1, 7,0);
    tracep->declBit(c+289,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+289,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+304,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+304,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+305,"g_20_21", false,-1);
    tracep->declBit(c+306,"p_20_21", false,-1);
    tracep->declBit(c+305,"g_20_41", false,-1);
    tracep->declBit(c+306,"p_20_41", false,-1);
    tracep->declBit(c+307,"g_30_31", false,-1);
    tracep->declBit(c+308,"p_30_31", false,-1);
    tracep->declBit(c+307,"g_30_51", false,-1);
    tracep->declBit(c+308,"p_30_51", false,-1);
    tracep->declBit(c+309,"g_40_41", false,-1);
    tracep->declBit(c+310,"p_40_41", false,-1);
    tracep->declBit(c+309,"g_40_61", false,-1);
    tracep->declBit(c+310,"p_40_61", false,-1);
    tracep->declBit(c+311,"g_50_51", false,-1);
    tracep->declBit(c+312,"p_50_51", false,-1);
    tracep->declBit(c+311,"g_50_71", false,-1);
    tracep->declBit(c+312,"p_50_71", false,-1);
    tracep->declBit(c+313,"g_60_61", false,-1);
    tracep->declBit(c+314,"p_60_61", false,-1);
    tracep->declBit(c+313,"g_60_81", false,-1);
    tracep->declBit(c+314,"p_60_81", false,-1);
    tracep->declBit(c+315,"g_70_71", false,-1);
    tracep->declBit(c+316,"p_70_71", false,-1);
    tracep->declBit(c+317,"g_80_81", false,-1);
    tracep->declBit(c+318,"p_80_81", false,-1);
    tracep->declBit(c+289,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+289,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+319,"g_41_42", false,-1);
    tracep->declBit(c+320,"p_41_42", false,-1);
    tracep->declBit(c+304,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+321,"g_51_52", false,-1);
    tracep->declBit(c+322,"p_51_52", false,-1);
    tracep->declBit(c+323,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+324,"g_61_62", false,-1);
    tracep->declBit(c+325,"p_61_62", false,-1);
    tracep->declBit(c+326,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+327,"g_71_72", false,-1);
    tracep->declBit(c+328,"p_71_72", false,-1);
    tracep->declBit(c+319,"g_41_82", false,-1);
    tracep->declBit(c+320,"p_41_82", false,-1);
    tracep->declBit(c+329,"g_81_82", false,-1);
    tracep->declBit(c+330,"p_81_82", false,-1);
    tracep->declBit(c+289,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+331,"g_82_83", false,-1);
    tracep->declBit(c+332,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+289,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+333,"g_current", false,-1);
    tracep->declBit(c+334,"p_current", false,-1);
    tracep->declBit(c+304,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+304,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+333,"g_previous", false,-1);
    tracep->declBit(c+334,"p_previous", false,-1);
    tracep->declBit(c+335,"g_current", false,-1);
    tracep->declBit(c+336,"p_current", false,-1);
    tracep->declBit(c+305,"g_combined_current", false,-1);
    tracep->declBit(c+306,"p_combined_current", false,-1);
    tracep->declBit(c+305,"g_combined_next", false,-1);
    tracep->declBit(c+306,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+289,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+305,"g_current", false,-1);
    tracep->declBit(c+306,"p_current", false,-1);
    tracep->declBit(c+323,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+323,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+335,"g_previous", false,-1);
    tracep->declBit(c+336,"p_previous", false,-1);
    tracep->declBit(c+337,"g_current", false,-1);
    tracep->declBit(c+338,"p_current", false,-1);
    tracep->declBit(c+307,"g_combined_current", false,-1);
    tracep->declBit(c+308,"p_combined_current", false,-1);
    tracep->declBit(c+307,"g_combined_next", false,-1);
    tracep->declBit(c+308,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+304,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+307,"g_current", false,-1);
    tracep->declBit(c+308,"p_current", false,-1);
    tracep->declBit(c+326,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+326,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+337,"g_previous", false,-1);
    tracep->declBit(c+338,"p_previous", false,-1);
    tracep->declBit(c+339,"g_current", false,-1);
    tracep->declBit(c+340,"p_current", false,-1);
    tracep->declBit(c+309,"g_combined_current", false,-1);
    tracep->declBit(c+310,"p_combined_current", false,-1);
    tracep->declBit(c+309,"g_combined_next", false,-1);
    tracep->declBit(c+310,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+305,"g_previous", false,-1);
    tracep->declBit(c+306,"p_previous", false,-1);
    tracep->declBit(c+309,"g_current", false,-1);
    tracep->declBit(c+310,"p_current", false,-1);
    tracep->declBit(c+319,"g_combined_current", false,-1);
    tracep->declBit(c+320,"p_combined_current", false,-1);
    tracep->declBit(c+319,"g_combined_next", false,-1);
    tracep->declBit(c+320,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+289,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+319,"g_current", false,-1);
    tracep->declBit(c+320,"p_current", false,-1);
    tracep->declBit(c+341,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+341,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+339,"g_previous", false,-1);
    tracep->declBit(c+340,"p_previous", false,-1);
    tracep->declBit(c+342,"g_current", false,-1);
    tracep->declBit(c+343,"p_current", false,-1);
    tracep->declBit(c+311,"g_combined_current", false,-1);
    tracep->declBit(c+312,"p_combined_current", false,-1);
    tracep->declBit(c+311,"g_combined_next", false,-1);
    tracep->declBit(c+312,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+307,"g_previous", false,-1);
    tracep->declBit(c+308,"p_previous", false,-1);
    tracep->declBit(c+311,"g_current", false,-1);
    tracep->declBit(c+312,"p_current", false,-1);
    tracep->declBit(c+321,"g_combined_current", false,-1);
    tracep->declBit(c+322,"p_combined_current", false,-1);
    tracep->declBit(c+321,"g_combined_next", false,-1);
    tracep->declBit(c+322,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+304,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+321,"g_current", false,-1);
    tracep->declBit(c+322,"p_current", false,-1);
    tracep->declBit(c+344,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+344,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+342,"g_previous", false,-1);
    tracep->declBit(c+343,"p_previous", false,-1);
    tracep->declBit(c+345,"g_current", false,-1);
    tracep->declBit(c+346,"p_current", false,-1);
    tracep->declBit(c+313,"g_combined_current", false,-1);
    tracep->declBit(c+314,"p_combined_current", false,-1);
    tracep->declBit(c+313,"g_combined_next", false,-1);
    tracep->declBit(c+314,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+309,"g_previous", false,-1);
    tracep->declBit(c+310,"p_previous", false,-1);
    tracep->declBit(c+313,"g_current", false,-1);
    tracep->declBit(c+314,"p_current", false,-1);
    tracep->declBit(c+324,"g_combined_current", false,-1);
    tracep->declBit(c+325,"p_combined_current", false,-1);
    tracep->declBit(c+324,"g_combined_next", false,-1);
    tracep->declBit(c+325,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+323,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+324,"g_current", false,-1);
    tracep->declBit(c+325,"p_current", false,-1);
    tracep->declBit(c+347,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+347,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+345,"g_previous", false,-1);
    tracep->declBit(c+346,"p_previous", false,-1);
    tracep->declBit(c+348,"g_current", false,-1);
    tracep->declBit(c+349,"p_current", false,-1);
    tracep->declBit(c+315,"g_combined_current", false,-1);
    tracep->declBit(c+316,"p_combined_current", false,-1);
    tracep->declBit(c+315,"g_combined_next", false,-1);
    tracep->declBit(c+316,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+311,"g_previous", false,-1);
    tracep->declBit(c+312,"p_previous", false,-1);
    tracep->declBit(c+315,"g_current", false,-1);
    tracep->declBit(c+316,"p_current", false,-1);
    tracep->declBit(c+327,"g_combined_current", false,-1);
    tracep->declBit(c+328,"p_combined_current", false,-1);
    tracep->declBit(c+327,"g_combined_next", false,-1);
    tracep->declBit(c+328,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+326,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+327,"g_current", false,-1);
    tracep->declBit(c+328,"p_current", false,-1);
    tracep->declBit(c+350,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+350,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+348,"g_previous", false,-1);
    tracep->declBit(c+349,"p_previous", false,-1);
    tracep->declBit(c+351,"g_current", false,-1);
    tracep->declBit(c+352,"p_current", false,-1);
    tracep->declBit(c+317,"g_combined_current", false,-1);
    tracep->declBit(c+318,"p_combined_current", false,-1);
    tracep->declBit(c+317,"g_combined_next", false,-1);
    tracep->declBit(c+318,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+313,"g_previous", false,-1);
    tracep->declBit(c+314,"p_previous", false,-1);
    tracep->declBit(c+317,"g_current", false,-1);
    tracep->declBit(c+318,"p_current", false,-1);
    tracep->declBit(c+329,"g_combined_current", false,-1);
    tracep->declBit(c+330,"p_combined_current", false,-1);
    tracep->declBit(c+329,"g_combined_next", false,-1);
    tracep->declBit(c+330,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+319,"g_previous", false,-1);
    tracep->declBit(c+320,"p_previous", false,-1);
    tracep->declBit(c+329,"g_current", false,-1);
    tracep->declBit(c+330,"p_current", false,-1);
    tracep->declBit(c+331,"g_combined_current", false,-1);
    tracep->declBit(c+332,"p_combined_current", false,-1);
    tracep->declBit(c+331,"g_combined_next", false,-1);
    tracep->declBit(c+332,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+289,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+331,"g_current", false,-1);
    tracep->declBit(c+332,"p_current", false,-1);
    tracep->declBit(c+297,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+297,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+289,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+289,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+289,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+289,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+289,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+289,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+289,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+289,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+289,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+304,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+304,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+304,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+353,"a_part", false,-1, 16,0);
    tracep->declBus(c+354,"b_part", false,-1, 8,0);
    tracep->declBit(c+355,"add_sub", false,-1);
    tracep->declBus(c+356,"rq_part", false,-1, 16,0);
    tracep->declBit(c+357,"nxt_add_sub", false,-1);
    tracep->declBus(c+358,"ax2", false,-1, 16,0);
    tracep->declBus(c+359,"b_final", false,-1, 8,0);
    tracep->declBus(c+360,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+361,"op_a", false,-1, 8,0);
    tracep->declBus(c+359,"op_b", false,-1, 8,0);
    tracep->declBit(c+355,"in_c", false,-1);
    tracep->declBus(c+360,"sum", false,-1, 8,0);
    tracep->declBit(c+362,"out_c", false,-1);
    tracep->declBit(c+363,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+364,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+365,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+355,"input_carry", false,-1);
    tracep->declBus(c+366,"target_element", false,-1, 7,0);
    tracep->declBit(c+363,"output_carry", false,-1);
    tracep->declBus(c+367,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+364,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+365,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+355,"input_carry", false,-1);
    tracep->declBus(c+367,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+363,"output_carry", false,-1);
    tracep->declBus(c+368,"g", false,-1, 7,0);
    tracep->declBus(c+369,"p", false,-1, 7,0);
    tracep->declBit(c+355,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+355,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+370,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+370,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+371,"g_20_21", false,-1);
    tracep->declBit(c+372,"p_20_21", false,-1);
    tracep->declBit(c+371,"g_20_41", false,-1);
    tracep->declBit(c+372,"p_20_41", false,-1);
    tracep->declBit(c+373,"g_30_31", false,-1);
    tracep->declBit(c+374,"p_30_31", false,-1);
    tracep->declBit(c+373,"g_30_51", false,-1);
    tracep->declBit(c+374,"p_30_51", false,-1);
    tracep->declBit(c+375,"g_40_41", false,-1);
    tracep->declBit(c+376,"p_40_41", false,-1);
    tracep->declBit(c+375,"g_40_61", false,-1);
    tracep->declBit(c+376,"p_40_61", false,-1);
    tracep->declBit(c+377,"g_50_51", false,-1);
    tracep->declBit(c+378,"p_50_51", false,-1);
    tracep->declBit(c+377,"g_50_71", false,-1);
    tracep->declBit(c+378,"p_50_71", false,-1);
    tracep->declBit(c+379,"g_60_61", false,-1);
    tracep->declBit(c+380,"p_60_61", false,-1);
    tracep->declBit(c+379,"g_60_81", false,-1);
    tracep->declBit(c+380,"p_60_81", false,-1);
    tracep->declBit(c+381,"g_70_71", false,-1);
    tracep->declBit(c+382,"p_70_71", false,-1);
    tracep->declBit(c+383,"g_80_81", false,-1);
    tracep->declBit(c+384,"p_80_81", false,-1);
    tracep->declBit(c+355,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+355,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+385,"g_41_42", false,-1);
    tracep->declBit(c+386,"p_41_42", false,-1);
    tracep->declBit(c+370,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+387,"g_51_52", false,-1);
    tracep->declBit(c+388,"p_51_52", false,-1);
    tracep->declBit(c+389,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+390,"g_61_62", false,-1);
    tracep->declBit(c+391,"p_61_62", false,-1);
    tracep->declBit(c+392,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+393,"g_71_72", false,-1);
    tracep->declBit(c+394,"p_71_72", false,-1);
    tracep->declBit(c+385,"g_41_82", false,-1);
    tracep->declBit(c+386,"p_41_82", false,-1);
    tracep->declBit(c+395,"g_81_82", false,-1);
    tracep->declBit(c+396,"p_81_82", false,-1);
    tracep->declBit(c+355,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+397,"g_82_83", false,-1);
    tracep->declBit(c+398,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+355,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+399,"g_current", false,-1);
    tracep->declBit(c+400,"p_current", false,-1);
    tracep->declBit(c+370,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+370,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+399,"g_previous", false,-1);
    tracep->declBit(c+400,"p_previous", false,-1);
    tracep->declBit(c+401,"g_current", false,-1);
    tracep->declBit(c+402,"p_current", false,-1);
    tracep->declBit(c+371,"g_combined_current", false,-1);
    tracep->declBit(c+372,"p_combined_current", false,-1);
    tracep->declBit(c+371,"g_combined_next", false,-1);
    tracep->declBit(c+372,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+355,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+371,"g_current", false,-1);
    tracep->declBit(c+372,"p_current", false,-1);
    tracep->declBit(c+389,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+389,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+401,"g_previous", false,-1);
    tracep->declBit(c+402,"p_previous", false,-1);
    tracep->declBit(c+403,"g_current", false,-1);
    tracep->declBit(c+404,"p_current", false,-1);
    tracep->declBit(c+373,"g_combined_current", false,-1);
    tracep->declBit(c+374,"p_combined_current", false,-1);
    tracep->declBit(c+373,"g_combined_next", false,-1);
    tracep->declBit(c+374,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+370,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+373,"g_current", false,-1);
    tracep->declBit(c+374,"p_current", false,-1);
    tracep->declBit(c+392,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+392,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+403,"g_previous", false,-1);
    tracep->declBit(c+404,"p_previous", false,-1);
    tracep->declBit(c+405,"g_current", false,-1);
    tracep->declBit(c+406,"p_current", false,-1);
    tracep->declBit(c+375,"g_combined_current", false,-1);
    tracep->declBit(c+376,"p_combined_current", false,-1);
    tracep->declBit(c+375,"g_combined_next", false,-1);
    tracep->declBit(c+376,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+371,"g_previous", false,-1);
    tracep->declBit(c+372,"p_previous", false,-1);
    tracep->declBit(c+375,"g_current", false,-1);
    tracep->declBit(c+376,"p_current", false,-1);
    tracep->declBit(c+385,"g_combined_current", false,-1);
    tracep->declBit(c+386,"p_combined_current", false,-1);
    tracep->declBit(c+385,"g_combined_next", false,-1);
    tracep->declBit(c+386,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+355,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+385,"g_current", false,-1);
    tracep->declBit(c+386,"p_current", false,-1);
    tracep->declBit(c+407,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+407,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+405,"g_previous", false,-1);
    tracep->declBit(c+406,"p_previous", false,-1);
    tracep->declBit(c+408,"g_current", false,-1);
    tracep->declBit(c+409,"p_current", false,-1);
    tracep->declBit(c+377,"g_combined_current", false,-1);
    tracep->declBit(c+378,"p_combined_current", false,-1);
    tracep->declBit(c+377,"g_combined_next", false,-1);
    tracep->declBit(c+378,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+373,"g_previous", false,-1);
    tracep->declBit(c+374,"p_previous", false,-1);
    tracep->declBit(c+377,"g_current", false,-1);
    tracep->declBit(c+378,"p_current", false,-1);
    tracep->declBit(c+387,"g_combined_current", false,-1);
    tracep->declBit(c+388,"p_combined_current", false,-1);
    tracep->declBit(c+387,"g_combined_next", false,-1);
    tracep->declBit(c+388,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+370,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+387,"g_current", false,-1);
    tracep->declBit(c+388,"p_current", false,-1);
    tracep->declBit(c+410,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+410,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+408,"g_previous", false,-1);
    tracep->declBit(c+409,"p_previous", false,-1);
    tracep->declBit(c+411,"g_current", false,-1);
    tracep->declBit(c+412,"p_current", false,-1);
    tracep->declBit(c+379,"g_combined_current", false,-1);
    tracep->declBit(c+380,"p_combined_current", false,-1);
    tracep->declBit(c+379,"g_combined_next", false,-1);
    tracep->declBit(c+380,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+375,"g_previous", false,-1);
    tracep->declBit(c+376,"p_previous", false,-1);
    tracep->declBit(c+379,"g_current", false,-1);
    tracep->declBit(c+380,"p_current", false,-1);
    tracep->declBit(c+390,"g_combined_current", false,-1);
    tracep->declBit(c+391,"p_combined_current", false,-1);
    tracep->declBit(c+390,"g_combined_next", false,-1);
    tracep->declBit(c+391,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+389,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+390,"g_current", false,-1);
    tracep->declBit(c+391,"p_current", false,-1);
    tracep->declBit(c+413,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+413,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+411,"g_previous", false,-1);
    tracep->declBit(c+412,"p_previous", false,-1);
    tracep->declBit(c+414,"g_current", false,-1);
    tracep->declBit(c+415,"p_current", false,-1);
    tracep->declBit(c+381,"g_combined_current", false,-1);
    tracep->declBit(c+382,"p_combined_current", false,-1);
    tracep->declBit(c+381,"g_combined_next", false,-1);
    tracep->declBit(c+382,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+377,"g_previous", false,-1);
    tracep->declBit(c+378,"p_previous", false,-1);
    tracep->declBit(c+381,"g_current", false,-1);
    tracep->declBit(c+382,"p_current", false,-1);
    tracep->declBit(c+393,"g_combined_current", false,-1);
    tracep->declBit(c+394,"p_combined_current", false,-1);
    tracep->declBit(c+393,"g_combined_next", false,-1);
    tracep->declBit(c+394,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+392,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+393,"g_current", false,-1);
    tracep->declBit(c+394,"p_current", false,-1);
    tracep->declBit(c+416,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+416,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+414,"g_previous", false,-1);
    tracep->declBit(c+415,"p_previous", false,-1);
    tracep->declBit(c+417,"g_current", false,-1);
    tracep->declBit(c+418,"p_current", false,-1);
    tracep->declBit(c+383,"g_combined_current", false,-1);
    tracep->declBit(c+384,"p_combined_current", false,-1);
    tracep->declBit(c+383,"g_combined_next", false,-1);
    tracep->declBit(c+384,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+379,"g_previous", false,-1);
    tracep->declBit(c+380,"p_previous", false,-1);
    tracep->declBit(c+383,"g_current", false,-1);
    tracep->declBit(c+384,"p_current", false,-1);
    tracep->declBit(c+395,"g_combined_current", false,-1);
    tracep->declBit(c+396,"p_combined_current", false,-1);
    tracep->declBit(c+395,"g_combined_next", false,-1);
    tracep->declBit(c+396,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+385,"g_previous", false,-1);
    tracep->declBit(c+386,"p_previous", false,-1);
    tracep->declBit(c+395,"g_current", false,-1);
    tracep->declBit(c+396,"p_current", false,-1);
    tracep->declBit(c+397,"g_combined_current", false,-1);
    tracep->declBit(c+398,"p_combined_current", false,-1);
    tracep->declBit(c+397,"g_combined_next", false,-1);
    tracep->declBit(c+398,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+355,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+397,"g_current", false,-1);
    tracep->declBit(c+398,"p_current", false,-1);
    tracep->declBit(c+363,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+363,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+355,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+355,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+355,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+355,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+355,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+355,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+355,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+355,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+355,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+370,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+370,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+370,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+419,"a_part", false,-1, 16,0);
    tracep->declBus(c+420,"b_part", false,-1, 8,0);
    tracep->declBit(c+421,"add_sub", false,-1);
    tracep->declBus(c+422,"rq_part", false,-1, 16,0);
    tracep->declBit(c+423,"nxt_add_sub", false,-1);
    tracep->declBus(c+424,"ax2", false,-1, 16,0);
    tracep->declBus(c+425,"b_final", false,-1, 8,0);
    tracep->declBus(c+426,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+427,"op_a", false,-1, 8,0);
    tracep->declBus(c+425,"op_b", false,-1, 8,0);
    tracep->declBit(c+421,"in_c", false,-1);
    tracep->declBus(c+426,"sum", false,-1, 8,0);
    tracep->declBit(c+428,"out_c", false,-1);
    tracep->declBit(c+429,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+430,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+431,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+421,"input_carry", false,-1);
    tracep->declBus(c+432,"target_element", false,-1, 7,0);
    tracep->declBit(c+429,"output_carry", false,-1);
    tracep->declBus(c+433,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+430,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+431,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+421,"input_carry", false,-1);
    tracep->declBus(c+433,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+429,"output_carry", false,-1);
    tracep->declBus(c+434,"g", false,-1, 7,0);
    tracep->declBus(c+435,"p", false,-1, 7,0);
    tracep->declBit(c+421,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+421,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+436,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+436,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+437,"g_20_21", false,-1);
    tracep->declBit(c+438,"p_20_21", false,-1);
    tracep->declBit(c+437,"g_20_41", false,-1);
    tracep->declBit(c+438,"p_20_41", false,-1);
    tracep->declBit(c+439,"g_30_31", false,-1);
    tracep->declBit(c+440,"p_30_31", false,-1);
    tracep->declBit(c+439,"g_30_51", false,-1);
    tracep->declBit(c+440,"p_30_51", false,-1);
    tracep->declBit(c+441,"g_40_41", false,-1);
    tracep->declBit(c+442,"p_40_41", false,-1);
    tracep->declBit(c+441,"g_40_61", false,-1);
    tracep->declBit(c+442,"p_40_61", false,-1);
    tracep->declBit(c+443,"g_50_51", false,-1);
    tracep->declBit(c+444,"p_50_51", false,-1);
    tracep->declBit(c+443,"g_50_71", false,-1);
    tracep->declBit(c+444,"p_50_71", false,-1);
    tracep->declBit(c+445,"g_60_61", false,-1);
    tracep->declBit(c+446,"p_60_61", false,-1);
    tracep->declBit(c+445,"g_60_81", false,-1);
    tracep->declBit(c+446,"p_60_81", false,-1);
    tracep->declBit(c+447,"g_70_71", false,-1);
    tracep->declBit(c+448,"p_70_71", false,-1);
    tracep->declBit(c+449,"g_80_81", false,-1);
    tracep->declBit(c+450,"p_80_81", false,-1);
    tracep->declBit(c+421,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+421,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+451,"g_41_42", false,-1);
    tracep->declBit(c+452,"p_41_42", false,-1);
    tracep->declBit(c+436,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+453,"g_51_52", false,-1);
    tracep->declBit(c+454,"p_51_52", false,-1);
    tracep->declBit(c+455,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+456,"g_61_62", false,-1);
    tracep->declBit(c+457,"p_61_62", false,-1);
    tracep->declBit(c+458,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+459,"g_71_72", false,-1);
    tracep->declBit(c+460,"p_71_72", false,-1);
    tracep->declBit(c+451,"g_41_82", false,-1);
    tracep->declBit(c+452,"p_41_82", false,-1);
    tracep->declBit(c+461,"g_81_82", false,-1);
    tracep->declBit(c+462,"p_81_82", false,-1);
    tracep->declBit(c+421,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+463,"g_82_83", false,-1);
    tracep->declBit(c+464,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+421,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+465,"g_current", false,-1);
    tracep->declBit(c+466,"p_current", false,-1);
    tracep->declBit(c+436,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+436,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+465,"g_previous", false,-1);
    tracep->declBit(c+466,"p_previous", false,-1);
    tracep->declBit(c+467,"g_current", false,-1);
    tracep->declBit(c+468,"p_current", false,-1);
    tracep->declBit(c+437,"g_combined_current", false,-1);
    tracep->declBit(c+438,"p_combined_current", false,-1);
    tracep->declBit(c+437,"g_combined_next", false,-1);
    tracep->declBit(c+438,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+421,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+437,"g_current", false,-1);
    tracep->declBit(c+438,"p_current", false,-1);
    tracep->declBit(c+455,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+455,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+467,"g_previous", false,-1);
    tracep->declBit(c+468,"p_previous", false,-1);
    tracep->declBit(c+469,"g_current", false,-1);
    tracep->declBit(c+470,"p_current", false,-1);
    tracep->declBit(c+439,"g_combined_current", false,-1);
    tracep->declBit(c+440,"p_combined_current", false,-1);
    tracep->declBit(c+439,"g_combined_next", false,-1);
    tracep->declBit(c+440,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+436,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+439,"g_current", false,-1);
    tracep->declBit(c+440,"p_current", false,-1);
    tracep->declBit(c+458,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+458,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+469,"g_previous", false,-1);
    tracep->declBit(c+470,"p_previous", false,-1);
    tracep->declBit(c+471,"g_current", false,-1);
    tracep->declBit(c+472,"p_current", false,-1);
    tracep->declBit(c+441,"g_combined_current", false,-1);
    tracep->declBit(c+442,"p_combined_current", false,-1);
    tracep->declBit(c+441,"g_combined_next", false,-1);
    tracep->declBit(c+442,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+437,"g_previous", false,-1);
    tracep->declBit(c+438,"p_previous", false,-1);
    tracep->declBit(c+441,"g_current", false,-1);
    tracep->declBit(c+442,"p_current", false,-1);
    tracep->declBit(c+451,"g_combined_current", false,-1);
    tracep->declBit(c+452,"p_combined_current", false,-1);
    tracep->declBit(c+451,"g_combined_next", false,-1);
    tracep->declBit(c+452,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+421,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+451,"g_current", false,-1);
    tracep->declBit(c+452,"p_current", false,-1);
    tracep->declBit(c+473,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+473,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+471,"g_previous", false,-1);
    tracep->declBit(c+472,"p_previous", false,-1);
    tracep->declBit(c+474,"g_current", false,-1);
    tracep->declBit(c+475,"p_current", false,-1);
    tracep->declBit(c+443,"g_combined_current", false,-1);
    tracep->declBit(c+444,"p_combined_current", false,-1);
    tracep->declBit(c+443,"g_combined_next", false,-1);
    tracep->declBit(c+444,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+439,"g_previous", false,-1);
    tracep->declBit(c+440,"p_previous", false,-1);
    tracep->declBit(c+443,"g_current", false,-1);
    tracep->declBit(c+444,"p_current", false,-1);
    tracep->declBit(c+453,"g_combined_current", false,-1);
    tracep->declBit(c+454,"p_combined_current", false,-1);
    tracep->declBit(c+453,"g_combined_next", false,-1);
    tracep->declBit(c+454,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+436,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+453,"g_current", false,-1);
    tracep->declBit(c+454,"p_current", false,-1);
    tracep->declBit(c+476,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+476,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+474,"g_previous", false,-1);
    tracep->declBit(c+475,"p_previous", false,-1);
    tracep->declBit(c+477,"g_current", false,-1);
    tracep->declBit(c+478,"p_current", false,-1);
    tracep->declBit(c+445,"g_combined_current", false,-1);
    tracep->declBit(c+446,"p_combined_current", false,-1);
    tracep->declBit(c+445,"g_combined_next", false,-1);
    tracep->declBit(c+446,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+441,"g_previous", false,-1);
    tracep->declBit(c+442,"p_previous", false,-1);
    tracep->declBit(c+445,"g_current", false,-1);
    tracep->declBit(c+446,"p_current", false,-1);
    tracep->declBit(c+456,"g_combined_current", false,-1);
    tracep->declBit(c+457,"p_combined_current", false,-1);
    tracep->declBit(c+456,"g_combined_next", false,-1);
    tracep->declBit(c+457,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+455,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+456,"g_current", false,-1);
    tracep->declBit(c+457,"p_current", false,-1);
    tracep->declBit(c+479,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+479,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+477,"g_previous", false,-1);
    tracep->declBit(c+478,"p_previous", false,-1);
    tracep->declBit(c+480,"g_current", false,-1);
    tracep->declBit(c+481,"p_current", false,-1);
    tracep->declBit(c+447,"g_combined_current", false,-1);
    tracep->declBit(c+448,"p_combined_current", false,-1);
    tracep->declBit(c+447,"g_combined_next", false,-1);
    tracep->declBit(c+448,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+443,"g_previous", false,-1);
    tracep->declBit(c+444,"p_previous", false,-1);
    tracep->declBit(c+447,"g_current", false,-1);
    tracep->declBit(c+448,"p_current", false,-1);
    tracep->declBit(c+459,"g_combined_current", false,-1);
    tracep->declBit(c+460,"p_combined_current", false,-1);
    tracep->declBit(c+459,"g_combined_next", false,-1);
    tracep->declBit(c+460,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+458,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+459,"g_current", false,-1);
    tracep->declBit(c+460,"p_current", false,-1);
    tracep->declBit(c+482,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+482,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+480,"g_previous", false,-1);
    tracep->declBit(c+481,"p_previous", false,-1);
    tracep->declBit(c+483,"g_current", false,-1);
    tracep->declBit(c+484,"p_current", false,-1);
    tracep->declBit(c+449,"g_combined_current", false,-1);
    tracep->declBit(c+450,"p_combined_current", false,-1);
    tracep->declBit(c+449,"g_combined_next", false,-1);
    tracep->declBit(c+450,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+445,"g_previous", false,-1);
    tracep->declBit(c+446,"p_previous", false,-1);
    tracep->declBit(c+449,"g_current", false,-1);
    tracep->declBit(c+450,"p_current", false,-1);
    tracep->declBit(c+461,"g_combined_current", false,-1);
    tracep->declBit(c+462,"p_combined_current", false,-1);
    tracep->declBit(c+461,"g_combined_next", false,-1);
    tracep->declBit(c+462,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+451,"g_previous", false,-1);
    tracep->declBit(c+452,"p_previous", false,-1);
    tracep->declBit(c+461,"g_current", false,-1);
    tracep->declBit(c+462,"p_current", false,-1);
    tracep->declBit(c+463,"g_combined_current", false,-1);
    tracep->declBit(c+464,"p_combined_current", false,-1);
    tracep->declBit(c+463,"g_combined_next", false,-1);
    tracep->declBit(c+464,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+421,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+463,"g_current", false,-1);
    tracep->declBit(c+464,"p_current", false,-1);
    tracep->declBit(c+429,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+429,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+421,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+421,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+421,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+421,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+421,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+421,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+421,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+421,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+421,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+436,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+436,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+436,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+485,"a_part", false,-1, 16,0);
    tracep->declBus(c+486,"b_part", false,-1, 8,0);
    tracep->declBit(c+487,"add_sub", false,-1);
    tracep->declBus(c+488,"rq_part", false,-1, 16,0);
    tracep->declBit(c+489,"nxt_add_sub", false,-1);
    tracep->declBus(c+490,"ax2", false,-1, 16,0);
    tracep->declBus(c+491,"b_final", false,-1, 8,0);
    tracep->declBus(c+492,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+493,"op_a", false,-1, 8,0);
    tracep->declBus(c+491,"op_b", false,-1, 8,0);
    tracep->declBit(c+487,"in_c", false,-1);
    tracep->declBus(c+492,"sum", false,-1, 8,0);
    tracep->declBit(c+494,"out_c", false,-1);
    tracep->declBit(c+495,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+496,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+497,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+487,"input_carry", false,-1);
    tracep->declBus(c+498,"target_element", false,-1, 7,0);
    tracep->declBit(c+495,"output_carry", false,-1);
    tracep->declBus(c+499,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+496,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+497,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+487,"input_carry", false,-1);
    tracep->declBus(c+499,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+495,"output_carry", false,-1);
    tracep->declBus(c+500,"g", false,-1, 7,0);
    tracep->declBus(c+501,"p", false,-1, 7,0);
    tracep->declBit(c+487,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+487,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+502,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+502,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+503,"g_20_21", false,-1);
    tracep->declBit(c+504,"p_20_21", false,-1);
    tracep->declBit(c+503,"g_20_41", false,-1);
    tracep->declBit(c+504,"p_20_41", false,-1);
    tracep->declBit(c+505,"g_30_31", false,-1);
    tracep->declBit(c+506,"p_30_31", false,-1);
    tracep->declBit(c+505,"g_30_51", false,-1);
    tracep->declBit(c+506,"p_30_51", false,-1);
    tracep->declBit(c+507,"g_40_41", false,-1);
    tracep->declBit(c+508,"p_40_41", false,-1);
    tracep->declBit(c+507,"g_40_61", false,-1);
    tracep->declBit(c+508,"p_40_61", false,-1);
    tracep->declBit(c+509,"g_50_51", false,-1);
    tracep->declBit(c+510,"p_50_51", false,-1);
    tracep->declBit(c+509,"g_50_71", false,-1);
    tracep->declBit(c+510,"p_50_71", false,-1);
    tracep->declBit(c+511,"g_60_61", false,-1);
    tracep->declBit(c+512,"p_60_61", false,-1);
    tracep->declBit(c+511,"g_60_81", false,-1);
    tracep->declBit(c+512,"p_60_81", false,-1);
    tracep->declBit(c+513,"g_70_71", false,-1);
    tracep->declBit(c+514,"p_70_71", false,-1);
    tracep->declBit(c+515,"g_80_81", false,-1);
    tracep->declBit(c+516,"p_80_81", false,-1);
    tracep->declBit(c+487,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+487,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+517,"g_41_42", false,-1);
    tracep->declBit(c+518,"p_41_42", false,-1);
    tracep->declBit(c+502,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+519,"g_51_52", false,-1);
    tracep->declBit(c+520,"p_51_52", false,-1);
    tracep->declBit(c+521,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+522,"g_61_62", false,-1);
    tracep->declBit(c+523,"p_61_62", false,-1);
    tracep->declBit(c+524,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+525,"g_71_72", false,-1);
    tracep->declBit(c+526,"p_71_72", false,-1);
    tracep->declBit(c+517,"g_41_82", false,-1);
    tracep->declBit(c+518,"p_41_82", false,-1);
    tracep->declBit(c+527,"g_81_82", false,-1);
    tracep->declBit(c+528,"p_81_82", false,-1);
    tracep->declBit(c+487,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+529,"g_82_83", false,-1);
    tracep->declBit(c+530,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+487,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+531,"g_current", false,-1);
    tracep->declBit(c+532,"p_current", false,-1);
    tracep->declBit(c+502,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+502,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+531,"g_previous", false,-1);
    tracep->declBit(c+532,"p_previous", false,-1);
    tracep->declBit(c+533,"g_current", false,-1);
    tracep->declBit(c+534,"p_current", false,-1);
    tracep->declBit(c+503,"g_combined_current", false,-1);
    tracep->declBit(c+504,"p_combined_current", false,-1);
    tracep->declBit(c+503,"g_combined_next", false,-1);
    tracep->declBit(c+504,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+487,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+503,"g_current", false,-1);
    tracep->declBit(c+504,"p_current", false,-1);
    tracep->declBit(c+521,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+521,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+533,"g_previous", false,-1);
    tracep->declBit(c+534,"p_previous", false,-1);
    tracep->declBit(c+535,"g_current", false,-1);
    tracep->declBit(c+536,"p_current", false,-1);
    tracep->declBit(c+505,"g_combined_current", false,-1);
    tracep->declBit(c+506,"p_combined_current", false,-1);
    tracep->declBit(c+505,"g_combined_next", false,-1);
    tracep->declBit(c+506,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+502,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+505,"g_current", false,-1);
    tracep->declBit(c+506,"p_current", false,-1);
    tracep->declBit(c+524,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+524,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+535,"g_previous", false,-1);
    tracep->declBit(c+536,"p_previous", false,-1);
    tracep->declBit(c+537,"g_current", false,-1);
    tracep->declBit(c+538,"p_current", false,-1);
    tracep->declBit(c+507,"g_combined_current", false,-1);
    tracep->declBit(c+508,"p_combined_current", false,-1);
    tracep->declBit(c+507,"g_combined_next", false,-1);
    tracep->declBit(c+508,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+503,"g_previous", false,-1);
    tracep->declBit(c+504,"p_previous", false,-1);
    tracep->declBit(c+507,"g_current", false,-1);
    tracep->declBit(c+508,"p_current", false,-1);
    tracep->declBit(c+517,"g_combined_current", false,-1);
    tracep->declBit(c+518,"p_combined_current", false,-1);
    tracep->declBit(c+517,"g_combined_next", false,-1);
    tracep->declBit(c+518,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+487,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+517,"g_current", false,-1);
    tracep->declBit(c+518,"p_current", false,-1);
    tracep->declBit(c+539,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+539,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+537,"g_previous", false,-1);
    tracep->declBit(c+538,"p_previous", false,-1);
    tracep->declBit(c+540,"g_current", false,-1);
    tracep->declBit(c+541,"p_current", false,-1);
    tracep->declBit(c+509,"g_combined_current", false,-1);
    tracep->declBit(c+510,"p_combined_current", false,-1);
    tracep->declBit(c+509,"g_combined_next", false,-1);
    tracep->declBit(c+510,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+505,"g_previous", false,-1);
    tracep->declBit(c+506,"p_previous", false,-1);
    tracep->declBit(c+509,"g_current", false,-1);
    tracep->declBit(c+510,"p_current", false,-1);
    tracep->declBit(c+519,"g_combined_current", false,-1);
    tracep->declBit(c+520,"p_combined_current", false,-1);
    tracep->declBit(c+519,"g_combined_next", false,-1);
    tracep->declBit(c+520,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+502,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+519,"g_current", false,-1);
    tracep->declBit(c+520,"p_current", false,-1);
    tracep->declBit(c+542,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+542,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+540,"g_previous", false,-1);
    tracep->declBit(c+541,"p_previous", false,-1);
    tracep->declBit(c+543,"g_current", false,-1);
    tracep->declBit(c+544,"p_current", false,-1);
    tracep->declBit(c+511,"g_combined_current", false,-1);
    tracep->declBit(c+512,"p_combined_current", false,-1);
    tracep->declBit(c+511,"g_combined_next", false,-1);
    tracep->declBit(c+512,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+507,"g_previous", false,-1);
    tracep->declBit(c+508,"p_previous", false,-1);
    tracep->declBit(c+511,"g_current", false,-1);
    tracep->declBit(c+512,"p_current", false,-1);
    tracep->declBit(c+522,"g_combined_current", false,-1);
    tracep->declBit(c+523,"p_combined_current", false,-1);
    tracep->declBit(c+522,"g_combined_next", false,-1);
    tracep->declBit(c+523,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+521,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+522,"g_current", false,-1);
    tracep->declBit(c+523,"p_current", false,-1);
    tracep->declBit(c+545,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+545,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+543,"g_previous", false,-1);
    tracep->declBit(c+544,"p_previous", false,-1);
    tracep->declBit(c+546,"g_current", false,-1);
    tracep->declBit(c+547,"p_current", false,-1);
    tracep->declBit(c+513,"g_combined_current", false,-1);
    tracep->declBit(c+514,"p_combined_current", false,-1);
    tracep->declBit(c+513,"g_combined_next", false,-1);
    tracep->declBit(c+514,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+509,"g_previous", false,-1);
    tracep->declBit(c+510,"p_previous", false,-1);
    tracep->declBit(c+513,"g_current", false,-1);
    tracep->declBit(c+514,"p_current", false,-1);
    tracep->declBit(c+525,"g_combined_current", false,-1);
    tracep->declBit(c+526,"p_combined_current", false,-1);
    tracep->declBit(c+525,"g_combined_next", false,-1);
    tracep->declBit(c+526,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+524,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+525,"g_current", false,-1);
    tracep->declBit(c+526,"p_current", false,-1);
    tracep->declBit(c+548,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+548,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+546,"g_previous", false,-1);
    tracep->declBit(c+547,"p_previous", false,-1);
    tracep->declBit(c+549,"g_current", false,-1);
    tracep->declBit(c+550,"p_current", false,-1);
    tracep->declBit(c+515,"g_combined_current", false,-1);
    tracep->declBit(c+516,"p_combined_current", false,-1);
    tracep->declBit(c+515,"g_combined_next", false,-1);
    tracep->declBit(c+516,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+511,"g_previous", false,-1);
    tracep->declBit(c+512,"p_previous", false,-1);
    tracep->declBit(c+515,"g_current", false,-1);
    tracep->declBit(c+516,"p_current", false,-1);
    tracep->declBit(c+527,"g_combined_current", false,-1);
    tracep->declBit(c+528,"p_combined_current", false,-1);
    tracep->declBit(c+527,"g_combined_next", false,-1);
    tracep->declBit(c+528,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+517,"g_previous", false,-1);
    tracep->declBit(c+518,"p_previous", false,-1);
    tracep->declBit(c+527,"g_current", false,-1);
    tracep->declBit(c+528,"p_current", false,-1);
    tracep->declBit(c+529,"g_combined_current", false,-1);
    tracep->declBit(c+530,"p_combined_current", false,-1);
    tracep->declBit(c+529,"g_combined_next", false,-1);
    tracep->declBit(c+530,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+487,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+529,"g_current", false,-1);
    tracep->declBit(c+530,"p_current", false,-1);
    tracep->declBit(c+495,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+495,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+487,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+487,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+487,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+487,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+487,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+487,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+487,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+487,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+487,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+502,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+502,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+502,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0\n"); );
    // Init
    const int c = vlSymsp->__Vm_baseCode;
    // Body
    tracep->declBus(c+628,"IP_LEN", false,-1, 31,0);
    tracep->declBus(c+631,"OP_LEN", false,-1, 31,0);
    tracep->declBus(c+551,"a_part", false,-1, 16,0);
    tracep->declBus(c+552,"b_part", false,-1, 8,0);
    tracep->declBit(c+553,"add_sub", false,-1);
    tracep->declBus(c+554,"rq_part", false,-1, 16,0);
    tracep->declBit(c+555,"nxt_add_sub", false,-1);
    tracep->declBus(c+556,"ax2", false,-1, 16,0);
    tracep->declBus(c+557,"b_final", false,-1, 8,0);
    tracep->declBus(c+558,"r_part", false,-1, 8,0);
    tracep->pushNamePrefix("add_9_inst ");
    tracep->declBus(c+629,"P_LEN", false,-1, 31,0);
    tracep->declBus(c+559,"op_a", false,-1, 8,0);
    tracep->declBus(c+557,"op_b", false,-1, 8,0);
    tracep->declBit(c+553,"in_c", false,-1);
    tracep->declBus(c+558,"sum", false,-1, 8,0);
    tracep->declBit(c+560,"out_c", false,-1);
    tracep->declBit(c+561,"out_c_w", false,-1);
    tracep->pushNamePrefix("add_8_inst ");
    tracep->declBus(c+562,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+563,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+553,"input_carry", false,-1);
    tracep->declBus(c+564,"target_element", false,-1, 7,0);
    tracep->declBit(c+561,"output_carry", false,-1);
    tracep->declBus(c+565,"bitwise_carry", false,-1, 7,0);
    tracep->pushNamePrefix("carry_8_0 ");
    tracep->declBus(c+562,"source_element_0", false,-1, 7,0);
    tracep->declBus(c+563,"source_element_1", false,-1, 7,0);
    tracep->declBit(c+553,"input_carry", false,-1);
    tracep->declBus(c+565,"bitwise_carry", false,-1, 7,0);
    tracep->declBit(c+561,"output_carry", false,-1);
    tracep->declBus(c+566,"g", false,-1, 7,0);
    tracep->declBus(c+567,"p", false,-1, 7,0);
    tracep->declBit(c+553,"g_00_01", false,-1);
    tracep->declBit(c+630,"p_00_01", false,-1);
    tracep->declBit(c+553,"g_00_21", false,-1);
    tracep->declBit(c+630,"p_00_21", false,-1);
    tracep->declBit(c+568,"g_10_11", false,-1);
    tracep->declBit(c+630,"p_10_11", false,-1);
    tracep->declBit(c+568,"g_10_31", false,-1);
    tracep->declBit(c+630,"p_10_31", false,-1);
    tracep->declBit(c+569,"g_20_21", false,-1);
    tracep->declBit(c+570,"p_20_21", false,-1);
    tracep->declBit(c+569,"g_20_41", false,-1);
    tracep->declBit(c+570,"p_20_41", false,-1);
    tracep->declBit(c+571,"g_30_31", false,-1);
    tracep->declBit(c+572,"p_30_31", false,-1);
    tracep->declBit(c+571,"g_30_51", false,-1);
    tracep->declBit(c+572,"p_30_51", false,-1);
    tracep->declBit(c+573,"g_40_41", false,-1);
    tracep->declBit(c+574,"p_40_41", false,-1);
    tracep->declBit(c+573,"g_40_61", false,-1);
    tracep->declBit(c+574,"p_40_61", false,-1);
    tracep->declBit(c+575,"g_50_51", false,-1);
    tracep->declBit(c+576,"p_50_51", false,-1);
    tracep->declBit(c+575,"g_50_71", false,-1);
    tracep->declBit(c+576,"p_50_71", false,-1);
    tracep->declBit(c+577,"g_60_61", false,-1);
    tracep->declBit(c+578,"p_60_61", false,-1);
    tracep->declBit(c+577,"g_60_81", false,-1);
    tracep->declBit(c+578,"p_60_81", false,-1);
    tracep->declBit(c+579,"g_70_71", false,-1);
    tracep->declBit(c+580,"p_70_71", false,-1);
    tracep->declBit(c+581,"g_80_81", false,-1);
    tracep->declBit(c+582,"p_80_81", false,-1);
    tracep->declBit(c+553,"g_01_02", false,-1);
    tracep->declBit(c+630,"p_01_02", false,-1);
    tracep->declBit(c+553,"g_01_42", false,-1);
    tracep->declBit(c+630,"p_01_42", false,-1);
    tracep->declBit(c+583,"g_41_42", false,-1);
    tracep->declBit(c+584,"p_41_42", false,-1);
    tracep->declBit(c+568,"g_11_52", false,-1);
    tracep->declBit(c+630,"p_11_52", false,-1);
    tracep->declBit(c+585,"g_51_52", false,-1);
    tracep->declBit(c+586,"p_51_52", false,-1);
    tracep->declBit(c+587,"g_21_62", false,-1);
    tracep->declBit(c+630,"p_21_62", false,-1);
    tracep->declBit(c+588,"g_61_62", false,-1);
    tracep->declBit(c+589,"p_61_62", false,-1);
    tracep->declBit(c+590,"g_31_72", false,-1);
    tracep->declBit(c+630,"p_31_72", false,-1);
    tracep->declBit(c+591,"g_71_72", false,-1);
    tracep->declBit(c+592,"p_71_72", false,-1);
    tracep->declBit(c+583,"g_41_82", false,-1);
    tracep->declBit(c+584,"p_41_82", false,-1);
    tracep->declBit(c+593,"g_81_82", false,-1);
    tracep->declBit(c+594,"p_81_82", false,-1);
    tracep->declBit(c+553,"g_02_83", false,-1);
    tracep->declBit(c+630,"p_02_83", false,-1);
    tracep->declBit(c+595,"g_82_83", false,-1);
    tracep->declBit(c+596,"p_82_83", false,-1);
    tracep->pushNamePrefix("black_cell_10 ");
    tracep->declBit(c+553,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+597,"g_current", false,-1);
    tracep->declBit(c+598,"p_current", false,-1);
    tracep->declBit(c+568,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+568,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_20 ");
    tracep->declBit(c+597,"g_previous", false,-1);
    tracep->declBit(c+598,"p_previous", false,-1);
    tracep->declBit(c+599,"g_current", false,-1);
    tracep->declBit(c+600,"p_current", false,-1);
    tracep->declBit(c+569,"g_combined_current", false,-1);
    tracep->declBit(c+570,"p_combined_current", false,-1);
    tracep->declBit(c+569,"g_combined_next", false,-1);
    tracep->declBit(c+570,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_21 ");
    tracep->declBit(c+553,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+569,"g_current", false,-1);
    tracep->declBit(c+570,"p_current", false,-1);
    tracep->declBit(c+587,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+587,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_30 ");
    tracep->declBit(c+599,"g_previous", false,-1);
    tracep->declBit(c+600,"p_previous", false,-1);
    tracep->declBit(c+601,"g_current", false,-1);
    tracep->declBit(c+602,"p_current", false,-1);
    tracep->declBit(c+571,"g_combined_current", false,-1);
    tracep->declBit(c+572,"p_combined_current", false,-1);
    tracep->declBit(c+571,"g_combined_next", false,-1);
    tracep->declBit(c+572,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_31 ");
    tracep->declBit(c+568,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+571,"g_current", false,-1);
    tracep->declBit(c+572,"p_current", false,-1);
    tracep->declBit(c+590,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+590,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_40 ");
    tracep->declBit(c+601,"g_previous", false,-1);
    tracep->declBit(c+602,"p_previous", false,-1);
    tracep->declBit(c+603,"g_current", false,-1);
    tracep->declBit(c+604,"p_current", false,-1);
    tracep->declBit(c+573,"g_combined_current", false,-1);
    tracep->declBit(c+574,"p_combined_current", false,-1);
    tracep->declBit(c+573,"g_combined_next", false,-1);
    tracep->declBit(c+574,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_41 ");
    tracep->declBit(c+569,"g_previous", false,-1);
    tracep->declBit(c+570,"p_previous", false,-1);
    tracep->declBit(c+573,"g_current", false,-1);
    tracep->declBit(c+574,"p_current", false,-1);
    tracep->declBit(c+583,"g_combined_current", false,-1);
    tracep->declBit(c+584,"p_combined_current", false,-1);
    tracep->declBit(c+583,"g_combined_next", false,-1);
    tracep->declBit(c+584,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_42 ");
    tracep->declBit(c+553,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+583,"g_current", false,-1);
    tracep->declBit(c+584,"p_current", false,-1);
    tracep->declBit(c+605,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+605,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_50 ");
    tracep->declBit(c+603,"g_previous", false,-1);
    tracep->declBit(c+604,"p_previous", false,-1);
    tracep->declBit(c+606,"g_current", false,-1);
    tracep->declBit(c+607,"p_current", false,-1);
    tracep->declBit(c+575,"g_combined_current", false,-1);
    tracep->declBit(c+576,"p_combined_current", false,-1);
    tracep->declBit(c+575,"g_combined_next", false,-1);
    tracep->declBit(c+576,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_51 ");
    tracep->declBit(c+571,"g_previous", false,-1);
    tracep->declBit(c+572,"p_previous", false,-1);
    tracep->declBit(c+575,"g_current", false,-1);
    tracep->declBit(c+576,"p_current", false,-1);
    tracep->declBit(c+585,"g_combined_current", false,-1);
    tracep->declBit(c+586,"p_combined_current", false,-1);
    tracep->declBit(c+585,"g_combined_next", false,-1);
    tracep->declBit(c+586,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_52 ");
    tracep->declBit(c+568,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+585,"g_current", false,-1);
    tracep->declBit(c+586,"p_current", false,-1);
    tracep->declBit(c+608,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+608,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_60 ");
    tracep->declBit(c+606,"g_previous", false,-1);
    tracep->declBit(c+607,"p_previous", false,-1);
    tracep->declBit(c+609,"g_current", false,-1);
    tracep->declBit(c+610,"p_current", false,-1);
    tracep->declBit(c+577,"g_combined_current", false,-1);
    tracep->declBit(c+578,"p_combined_current", false,-1);
    tracep->declBit(c+577,"g_combined_next", false,-1);
    tracep->declBit(c+578,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_61 ");
    tracep->declBit(c+573,"g_previous", false,-1);
    tracep->declBit(c+574,"p_previous", false,-1);
    tracep->declBit(c+577,"g_current", false,-1);
    tracep->declBit(c+578,"p_current", false,-1);
    tracep->declBit(c+588,"g_combined_current", false,-1);
    tracep->declBit(c+589,"p_combined_current", false,-1);
    tracep->declBit(c+588,"g_combined_next", false,-1);
    tracep->declBit(c+589,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_62 ");
    tracep->declBit(c+587,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+588,"g_current", false,-1);
    tracep->declBit(c+589,"p_current", false,-1);
    tracep->declBit(c+611,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+611,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_70 ");
    tracep->declBit(c+609,"g_previous", false,-1);
    tracep->declBit(c+610,"p_previous", false,-1);
    tracep->declBit(c+612,"g_current", false,-1);
    tracep->declBit(c+613,"p_current", false,-1);
    tracep->declBit(c+579,"g_combined_current", false,-1);
    tracep->declBit(c+580,"p_combined_current", false,-1);
    tracep->declBit(c+579,"g_combined_next", false,-1);
    tracep->declBit(c+580,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_71 ");
    tracep->declBit(c+575,"g_previous", false,-1);
    tracep->declBit(c+576,"p_previous", false,-1);
    tracep->declBit(c+579,"g_current", false,-1);
    tracep->declBit(c+580,"p_current", false,-1);
    tracep->declBit(c+591,"g_combined_current", false,-1);
    tracep->declBit(c+592,"p_combined_current", false,-1);
    tracep->declBit(c+591,"g_combined_next", false,-1);
    tracep->declBit(c+592,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_72 ");
    tracep->declBit(c+590,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+591,"g_current", false,-1);
    tracep->declBit(c+592,"p_current", false,-1);
    tracep->declBit(c+614,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+614,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_80 ");
    tracep->declBit(c+612,"g_previous", false,-1);
    tracep->declBit(c+613,"p_previous", false,-1);
    tracep->declBit(c+615,"g_current", false,-1);
    tracep->declBit(c+616,"p_current", false,-1);
    tracep->declBit(c+581,"g_combined_current", false,-1);
    tracep->declBit(c+582,"p_combined_current", false,-1);
    tracep->declBit(c+581,"g_combined_next", false,-1);
    tracep->declBit(c+582,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_81 ");
    tracep->declBit(c+577,"g_previous", false,-1);
    tracep->declBit(c+578,"p_previous", false,-1);
    tracep->declBit(c+581,"g_current", false,-1);
    tracep->declBit(c+582,"p_current", false,-1);
    tracep->declBit(c+593,"g_combined_current", false,-1);
    tracep->declBit(c+594,"p_combined_current", false,-1);
    tracep->declBit(c+593,"g_combined_next", false,-1);
    tracep->declBit(c+594,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_82 ");
    tracep->declBit(c+583,"g_previous", false,-1);
    tracep->declBit(c+584,"p_previous", false,-1);
    tracep->declBit(c+593,"g_current", false,-1);
    tracep->declBit(c+594,"p_current", false,-1);
    tracep->declBit(c+595,"g_combined_current", false,-1);
    tracep->declBit(c+596,"p_combined_current", false,-1);
    tracep->declBit(c+595,"g_combined_next", false,-1);
    tracep->declBit(c+596,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("black_cell_83 ");
    tracep->declBit(c+553,"g_previous", false,-1);
    tracep->declBit(c+630,"p_previous", false,-1);
    tracep->declBit(c+595,"g_current", false,-1);
    tracep->declBit(c+596,"p_current", false,-1);
    tracep->declBit(c+561,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+561,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_00 ");
    tracep->declBit(c+553,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+553,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+553,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_01 ");
    tracep->declBit(c+553,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+553,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+553,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_02 ");
    tracep->declBit(c+553,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+553,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+553,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(1);
    tracep->pushNamePrefix("white_cell_11 ");
    tracep->declBit(c+568,"g_current", false,-1);
    tracep->declBit(c+630,"p_current", false,-1);
    tracep->declBit(c+568,"g_combined_current", false,-1);
    tracep->declBit(c+630,"p_combined_current", false,-1);
    tracep->declBit(c+568,"g_combined_next", false,-1);
    tracep->declBit(c+630,"p_combined_next", false,-1);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_init_top(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_init_top\n"); );
    // Body
    div_8_pipe___024root__trace_init_sub__TOP__0(vlSelf, tracep);
    tracep->pushNamePrefix("div_8_pipe ");
    tracep->pushNamePrefix("nrdiv_inst ");
    tracep->pushNamePrefix("gen_loop[0] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[1] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[2] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[3] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[4] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[5] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[6] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(2);
    tracep->pushNamePrefix("gen_loop[7] ");
    tracep->pushNamePrefix("nrdiv_step_gen ");
    div_8_pipe___024root__trace_init_sub__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(vlSelf, tracep);
    tracep->popNamePrefix(4);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_full_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp);
void div_8_pipe___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp);
void div_8_pipe___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/);

VL_ATTR_COLD void div_8_pipe___024root__trace_register(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_register\n"); );
    // Body
    tracep->addFullCb(&div_8_pipe___024root__trace_full_top_0, vlSelf);
    tracep->addChgCb(&div_8_pipe___024root__trace_chg_top_0, vlSelf);
    tracep->addCleanupCb(&div_8_pipe___024root__trace_cleanup, vlSelf);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_full_sub_0(div_8_pipe___024root* vlSelf, VerilatedVcd::Buffer* bufp);

VL_ATTR_COLD void div_8_pipe___024root__trace_full_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_full_top_0\n"); );
    // Init
    div_8_pipe___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<div_8_pipe___024root*>(voidSelf);
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    div_8_pipe___024root__trace_full_sub_0((&vlSymsp->TOP), bufp);
}

VL_ATTR_COLD void div_8_pipe___024root__trace_full_sub_0(div_8_pipe___024root* vlSelf, VerilatedVcd::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_full_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode);
    // Body
    bufp->fullCData(oldp+1,((0xffU & vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                             [7U])),8);
    bufp->fullCData(oldp+2,((0xffU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w))),8);
    bufp->fullCData(oldp+3,((0xffU & ((IData)(1U) + 
                                      (~ vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                       [7U])))),8);
    bufp->fullCData(oldp+4,((0xffU & ((IData)(1U) + 
                                      (~ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w))))),8);
    bufp->fullIData(oldp+5,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[0]),17);
    bufp->fullIData(oldp+6,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[1]),17);
    bufp->fullIData(oldp+7,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[2]),17);
    bufp->fullIData(oldp+8,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[3]),17);
    bufp->fullIData(oldp+9,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[4]),17);
    bufp->fullIData(oldp+10,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[5]),17);
    bufp->fullIData(oldp+11,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[6]),17);
    bufp->fullIData(oldp+12,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[7]),17);
    bufp->fullSData(oldp+13,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[0]),9);
    bufp->fullSData(oldp+14,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[1]),9);
    bufp->fullSData(oldp+15,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[2]),9);
    bufp->fullSData(oldp+16,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[3]),9);
    bufp->fullSData(oldp+17,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[4]),9);
    bufp->fullSData(oldp+18,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[5]),9);
    bufp->fullSData(oldp+19,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[6]),9);
    bufp->fullSData(oldp+20,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[7]),9);
    bufp->fullCData(oldp+21,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff),8);
    bufp->fullIData(oldp+22,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[0]),17);
    bufp->fullIData(oldp+23,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[1]),17);
    bufp->fullIData(oldp+24,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[2]),17);
    bufp->fullIData(oldp+25,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[3]),17);
    bufp->fullIData(oldp+26,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[4]),17);
    bufp->fullIData(oldp+27,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[5]),17);
    bufp->fullIData(oldp+28,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[6]),17);
    bufp->fullIData(oldp+29,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[7]),17);
    bufp->fullCData(oldp+30,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w),8);
    bufp->fullSData(oldp+31,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w),9);
    bufp->fullSData(oldp+32,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w),9);
    bufp->fullSData(oldp+33,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                        [7U] >> 8U))),9);
    bufp->fullBit(oldp+34,((1U & (((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                    [7U] >> 0x10U) 
                                   & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w) 
                                      >> 8U)) | (((
                                                   (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                     >> 7U) 
                                                    | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                        >> 6U) 
                                                       & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                          >> 7U))) 
                                                   | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                                  | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                     & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                                 & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                                     [7U] 
                                                     >> 0x10U) 
                                                    ^ 
                                                    ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w) 
                                                     >> 8U)))))));
    bufp->fullBit(oldp+35,((1U & (((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                  | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                     & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullCData(oldp+36,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                       [7U] >> 8U))),8);
    bufp->fullCData(oldp+37,((0xffU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))),8);
    bufp->fullCData(oldp+38,((0xffU & (((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                         [7U] >> 8U) 
                                        ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)) 
                                       ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+39,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+40,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+41,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+42,((1U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+43,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+44,((IData)((3U == (3U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))))));
    bufp->fullBit(oldp+45,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+46,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+47,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+48,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+49,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+50,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+51,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+52,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+53,((1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 5U) 
                                             & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                >> 6U))))));
    bufp->fullBit(oldp+54,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+55,((1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 6U) 
                                             & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                >> 7U))))));
    bufp->fullBit(oldp+56,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+57,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+58,((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                             & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                >> 1U)) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41))));
    bufp->fullBit(oldp+59,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                            | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                               & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+60,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                            & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+61,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                            | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                               & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+62,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                            & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+63,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+64,((1U & ((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))) 
                                  | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                     & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+65,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                            & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+66,((1U & ((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))) 
                                  | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                     & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+67,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+68,(((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                              & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                 >> 1U)) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)) 
                            & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+69,((1U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+70,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 1U))));
    bufp->fullBit(oldp+71,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 1U))));
    bufp->fullBit(oldp+72,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 2U))));
    bufp->fullBit(oldp+73,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 2U))));
    bufp->fullBit(oldp+74,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 3U))));
    bufp->fullBit(oldp+75,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 3U))));
    bufp->fullBit(oldp+76,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 4U))));
    bufp->fullBit(oldp+77,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 4U))));
    bufp->fullBit(oldp+78,((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                            | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                               & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+79,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 5U))));
    bufp->fullBit(oldp+80,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 5U))));
    bufp->fullBit(oldp+81,((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                            | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
                               & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+82,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 6U))));
    bufp->fullBit(oldp+83,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 6U))));
    bufp->fullBit(oldp+84,((1U & (((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                  | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                     & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                        & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+85,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  >> 7U))));
    bufp->fullBit(oldp+86,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                  >> 7U))));
    bufp->fullIData(oldp+87,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk1__DOT__i),32);
    bufp->fullIData(oldp+88,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk2__DOT__i),32);
    bufp->fullIData(oldp+89,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                             [0U]),17);
    bufp->fullSData(oldp+90,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                             [0U]),9);
    bufp->fullBit(oldp+91,((1U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff))));
    bufp->fullIData(oldp+92,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                               << 8U) | ((0xfeU & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [0U] 
                                                   << 1U)) 
                                         | (1U & (~ 
                                                  ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                   >> 8U)))))),17);
    bufp->fullBit(oldp+93,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                     >> 8U)))));
    bufp->fullIData(oldp+94,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                          [0U] << 1U))),17);
    bufp->fullSData(oldp+95,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+96,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+97,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [0U] >> 7U))),9);
    bufp->fullBit(oldp+98,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U) 
                                           & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [0U] 
                                              >> 0xfU))) 
                                  | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                     & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [0U] 
                                               >> 0xfU)) 
                                        ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                           >> 8U)))))));
    bufp->fullBit(oldp+99,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                  | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                     & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+100,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [0U] >> 7U))),8);
    bufp->fullCData(oldp+101,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+102,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [0U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+103,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+104,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+105,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+106,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+107,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+108,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+109,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+110,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+111,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+112,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+113,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+114,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+115,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+116,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+117,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+118,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+119,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+120,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+121,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+122,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+123,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+124,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+125,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+126,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+127,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+128,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+129,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+130,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+131,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+132,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+133,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+134,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+135,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+136,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+137,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+138,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+139,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+140,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+141,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+142,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+143,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+144,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+145,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+146,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+147,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+148,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+149,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+150,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+151,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+152,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+153,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+154,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+155,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [1U]),17);
    bufp->fullSData(oldp+156,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [1U]),9);
    bufp->fullBit(oldp+157,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 1U))));
    bufp->fullIData(oldp+158,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [1U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+159,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+160,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [1U] << 1U))),17);
    bufp->fullSData(oldp+161,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+162,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+163,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [1U] >> 7U))),9);
    bufp->fullBit(oldp+164,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [1U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 1U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [1U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+165,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 1U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+166,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [1U] >> 7U))),8);
    bufp->fullCData(oldp+167,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+168,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [1U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+169,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+170,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+171,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+172,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+173,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+174,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+175,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+176,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+177,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+178,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+179,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+180,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+181,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+182,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+183,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+184,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+185,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+186,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+187,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+188,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+189,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+190,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+191,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+192,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+193,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+194,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+195,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+196,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+197,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+198,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+199,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+200,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+201,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+202,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+203,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+204,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+205,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+206,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+207,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+208,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+209,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 1U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+210,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+211,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+212,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+213,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+214,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+215,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+216,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+217,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+218,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+219,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+220,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+221,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [2U]),17);
    bufp->fullSData(oldp+222,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [2U]),9);
    bufp->fullBit(oldp+223,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 2U))));
    bufp->fullIData(oldp+224,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [2U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+225,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+226,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [2U] << 1U))),17);
    bufp->fullSData(oldp+227,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+228,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+229,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [2U] >> 7U))),9);
    bufp->fullBit(oldp+230,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [2U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 2U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [2U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+231,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 2U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+232,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [2U] >> 7U))),8);
    bufp->fullCData(oldp+233,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+234,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [2U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+235,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+236,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+237,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+238,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+239,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+240,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+241,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+242,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+243,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+244,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+245,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+246,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+247,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+248,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+249,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+250,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+251,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+252,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+253,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+254,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+255,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+256,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+257,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+258,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+259,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+260,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+261,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+262,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+263,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+264,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+265,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+266,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+267,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+268,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+269,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+270,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+271,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+272,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+273,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+274,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+275,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 2U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+276,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+277,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+278,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+279,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+280,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+281,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+282,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+283,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+284,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+285,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+286,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+287,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [3U]),17);
    bufp->fullSData(oldp+288,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [3U]),9);
    bufp->fullBit(oldp+289,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 3U))));
    bufp->fullIData(oldp+290,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [3U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+291,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+292,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [3U] << 1U))),17);
    bufp->fullSData(oldp+293,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+294,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+295,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [3U] >> 7U))),9);
    bufp->fullBit(oldp+296,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [3U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 3U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [3U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+297,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 3U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+298,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [3U] >> 7U))),8);
    bufp->fullCData(oldp+299,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+300,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [3U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+301,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+302,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+303,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+304,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+305,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+306,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+307,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+308,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+309,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+310,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+311,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+312,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+313,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+314,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+315,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+316,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+317,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+318,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+319,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+320,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+321,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+322,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+323,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+324,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+325,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+326,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+327,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+328,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+329,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+330,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+331,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+332,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+333,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+334,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+335,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+336,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+337,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+338,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+339,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+340,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+341,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 3U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+342,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+343,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+344,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+345,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+346,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+347,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+348,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+349,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+350,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+351,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+352,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+353,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [4U]),17);
    bufp->fullSData(oldp+354,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [4U]),9);
    bufp->fullBit(oldp+355,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 4U))));
    bufp->fullIData(oldp+356,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [4U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+357,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+358,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [4U] << 1U))),17);
    bufp->fullSData(oldp+359,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+360,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+361,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [4U] >> 7U))),9);
    bufp->fullBit(oldp+362,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [4U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 4U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [4U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+363,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 4U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+364,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [4U] >> 7U))),8);
    bufp->fullCData(oldp+365,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+366,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [4U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+367,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+368,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+369,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+370,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+371,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+372,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+373,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+374,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+375,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+376,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+377,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+378,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+379,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+380,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+381,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+382,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+383,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+384,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+385,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+386,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+387,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+388,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+389,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+390,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+391,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+392,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+393,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+394,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+395,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+396,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+397,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+398,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+399,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+400,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+401,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+402,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+403,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+404,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+405,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+406,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+407,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 4U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+408,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+409,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+410,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+411,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+412,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+413,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+414,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+415,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+416,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+417,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+418,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+419,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [5U]),17);
    bufp->fullSData(oldp+420,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [5U]),9);
    bufp->fullBit(oldp+421,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 5U))));
    bufp->fullIData(oldp+422,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [5U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+423,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+424,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [5U] << 1U))),17);
    bufp->fullSData(oldp+425,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+426,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+427,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [5U] >> 7U))),9);
    bufp->fullBit(oldp+428,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [5U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 5U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [5U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+429,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 5U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+430,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [5U] >> 7U))),8);
    bufp->fullCData(oldp+431,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+432,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [5U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+433,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+434,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+435,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+436,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+437,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+438,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+439,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+440,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+441,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+442,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+443,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+444,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+445,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+446,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+447,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+448,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+449,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+450,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+451,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+452,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+453,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+454,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+455,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+456,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+457,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+458,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+459,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+460,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+461,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+462,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+463,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+464,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+465,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+466,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+467,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+468,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+469,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+470,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+471,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+472,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+473,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 5U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+474,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+475,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+476,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+477,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+478,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+479,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+480,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+481,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+482,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+483,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+484,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+485,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [6U]),17);
    bufp->fullSData(oldp+486,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [6U]),9);
    bufp->fullBit(oldp+487,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 6U))));
    bufp->fullIData(oldp+488,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [6U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+489,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+490,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [6U] << 1U))),17);
    bufp->fullSData(oldp+491,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+492,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+493,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [6U] >> 7U))),9);
    bufp->fullBit(oldp+494,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [6U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [6U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+495,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 6U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+496,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [6U] >> 7U))),8);
    bufp->fullCData(oldp+497,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+498,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [6U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+499,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+500,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+501,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+502,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+503,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+504,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+505,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+506,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+507,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+508,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+509,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+510,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+511,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+512,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+513,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+514,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+515,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+516,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+517,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+518,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+519,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+520,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+521,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+522,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+523,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+524,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+525,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+526,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+527,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+528,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+529,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+530,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+531,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+532,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+533,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+534,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+535,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+536,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+537,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+538,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+539,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 6U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+540,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+541,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+542,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+543,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+544,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+545,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+546,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+547,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+548,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+549,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+550,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullIData(oldp+551,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                              [7U]),17);
    bufp->fullSData(oldp+552,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                              [7U]),9);
    bufp->fullBit(oldp+553,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   >> 7U))));
    bufp->fullIData(oldp+554,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                << 8U) | ((0xfeU & 
                                           (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [7U] << 1U)) 
                                          | (1U & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
    bufp->fullBit(oldp+555,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                      >> 8U)))));
    bufp->fullIData(oldp+556,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [7U] << 1U))),17);
    bufp->fullSData(oldp+557,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
    bufp->fullSData(oldp+558,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
    bufp->fullSData(oldp+559,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                         [7U] >> 7U))),9);
    bufp->fullBit(oldp+560,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                             >> 8U) 
                                            & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                               [7U] 
                                               >> 0xfU))) 
                                   | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 7U) | 
                                          (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                            >> 6U) 
                                           & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                              >> 7U))) 
                                         | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                       | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                           >> 7U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                      & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [7U] 
                                                >> 0xfU)) 
                                         ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                            >> 8U)))))));
    bufp->fullBit(oldp+561,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                   | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                       >> 7U) & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
    bufp->fullCData(oldp+562,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                        [7U] >> 7U))),8);
    bufp->fullCData(oldp+563,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
    bufp->fullCData(oldp+564,((0xffU & (((0x1ffU & 
                                          (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [7U] >> 7U)) 
                                         ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                        ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
    bufp->fullCData(oldp+565,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
    bufp->fullCData(oldp+566,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
    bufp->fullCData(oldp+567,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
    bufp->fullBit(oldp+568,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
    bufp->fullBit(oldp+569,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
    bufp->fullBit(oldp+570,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
    bufp->fullBit(oldp+571,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
    bufp->fullBit(oldp+572,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
    bufp->fullBit(oldp+573,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
    bufp->fullBit(oldp+574,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
    bufp->fullBit(oldp+575,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
    bufp->fullBit(oldp+576,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
    bufp->fullBit(oldp+577,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
    bufp->fullBit(oldp+578,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
    bufp->fullBit(oldp+579,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 5U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 6U))))));
    bufp->fullBit(oldp+580,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
    bufp->fullBit(oldp+581,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                    >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                               >> 6U) 
                                              & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                 >> 7U))))));
    bufp->fullBit(oldp+582,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    bufp->fullBit(oldp+583,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
    bufp->fullBit(oldp+584,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
    bufp->fullBit(oldp+585,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
    bufp->fullBit(oldp+586,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
    bufp->fullBit(oldp+587,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
    bufp->fullBit(oldp+588,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
    bufp->fullBit(oldp+589,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
    bufp->fullBit(oldp+590,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
    bufp->fullBit(oldp+591,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 5U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 6U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
    bufp->fullBit(oldp+592,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
    bufp->fullBit(oldp+593,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
    bufp->fullBit(oldp+594,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
    bufp->fullBit(oldp+595,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
    bufp->fullBit(oldp+596,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
    bufp->fullBit(oldp+597,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
    bufp->fullBit(oldp+598,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    bufp->fullBit(oldp+599,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 1U))));
    bufp->fullBit(oldp+600,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U))));
    bufp->fullBit(oldp+601,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 2U))));
    bufp->fullBit(oldp+602,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 2U))));
    bufp->fullBit(oldp+603,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 3U))));
    bufp->fullBit(oldp+604,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 3U))));
    bufp->fullBit(oldp+605,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                             | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                 >> 7U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
    bufp->fullBit(oldp+606,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 4U))));
    bufp->fullBit(oldp+607,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 4U))));
    bufp->fullBit(oldp+608,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
    bufp->fullBit(oldp+609,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 5U))));
    bufp->fullBit(oldp+610,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 5U))));
    bufp->fullBit(oldp+611,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                              | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                 & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                             | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
    bufp->fullBit(oldp+612,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 6U))));
    bufp->fullBit(oldp+613,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 6U))));
    bufp->fullBit(oldp+614,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))) 
                                    | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                       & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                   | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                      & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
    bufp->fullBit(oldp+615,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                   >> 7U))));
    bufp->fullBit(oldp+616,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 7U))));
    bufp->fullBit(oldp+617,(vlSelf->clk));
    bufp->fullBit(oldp+618,(vlSelf->nrst));
    bufp->fullCData(oldp+619,(vlSelf->dnd),8);
    bufp->fullCData(oldp+620,(vlSelf->div),8);
    bufp->fullBit(oldp+621,(vlSelf->sgn));
    bufp->fullCData(oldp+622,(vlSelf->q),8);
    bufp->fullCData(oldp+623,(vlSelf->r),8);
    bufp->fullCData(oldp+624,((0xffU & ((IData)(1U) 
                                        + (~ (IData)(vlSelf->dnd))))),8);
    bufp->fullCData(oldp+625,((0xffU & ((IData)(1U) 
                                        + (~ (IData)(vlSelf->div))))),8);
    bufp->fullIData(oldp+626,(vlSelf->div_8_pipe__DOT__dnd_eff),17);
    bufp->fullSData(oldp+627,(vlSelf->div_8_pipe__DOT__div_eff),9);
    bufp->fullIData(oldp+628,(8U),32);
    bufp->fullIData(oldp+629,(9U),32);
    bufp->fullBit(oldp+630,(0U));
    bufp->fullIData(oldp+631,(0x10U),32);
}
