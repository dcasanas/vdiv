// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See div_8_pipe.h for the primary calling header

#include "verilated.h"

#include "div_8_pipe__Syms.h"
#include "div_8_pipe_nrdiv_step.h"

void div_8_pipe_nrdiv_step___ctor_var_reset(div_8_pipe_nrdiv_step* vlSelf);

div_8_pipe_nrdiv_step::div_8_pipe_nrdiv_step(div_8_pipe__Syms* symsp, const char* name)
    : VerilatedModule{name}
    , vlSymsp{symsp}
 {
    // Reset structure values
    div_8_pipe_nrdiv_step___ctor_var_reset(this);
}

void div_8_pipe_nrdiv_step::__Vconfigure(bool first) {
    if (false && first) {}  // Prevent unused
}

div_8_pipe_nrdiv_step::~div_8_pipe_nrdiv_step() {
}
