// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See div_8_pipe.h for the primary calling header

#include "verilated.h"

#include "div_8_pipe___024root.h"

VL_INLINE_OPT void div_8_pipe___024root___sequent__TOP__0(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___sequent__TOP__0\n"); );
    // Init
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0;
    CData/*0:0*/ __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v1;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v2;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v3;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v4;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v5;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v6;
    IData/*16:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v7;
    CData/*0:0*/ __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v8;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0;
    CData/*0:0*/ __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v1;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v2;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v3;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v4;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v5;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v6;
    SData/*8:0*/ __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v7;
    CData/*0:0*/ __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v8;
    // Body
    __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0 = 0U;
    __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v8 = 0U;
    if ((1U & (~ (IData)(vlSelf->nrst)))) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk1__DOT__i = 8U;
    }
    __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0 = 0U;
    __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v8 = 0U;
    if (vlSelf->nrst) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk2__DOT__i = 8U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff 
            = ((0xf8U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff)) 
               | (1U | ((4U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w) 
                               << 1U)) | (2U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w) 
                                                << 1U)))));
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff 
            = ((7U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff)) 
               | (0xf8U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w) 
                           << 1U)));
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0 
            = vlSelf->div_8_pipe__DOT__dnd_eff;
        __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0 = 1U;
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v1 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [0U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v2 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [1U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v3 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [2U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v4 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [3U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v5 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [4U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v6 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [5U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v7 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [6U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0 
            = vlSelf->div_8_pipe__DOT__div_eff;
        __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0 = 1U;
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v1 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [0U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v2 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [1U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v3 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [2U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v4 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [3U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v5 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [4U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v6 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [5U];
        __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v7 
            = vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
            [6U];
    } else {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff = 0U;
        __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v8 = 1U;
        __Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v8 = 1U;
    }
    if (__Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[0U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v0;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[1U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v1;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[2U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v2;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[3U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v3;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[4U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v4;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[5U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v5;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[6U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v6;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[7U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v7;
    }
    if (__Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff__v8) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[0U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[1U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[2U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[3U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[4U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[5U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[6U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[7U] = 0U;
    }
    if (__Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[0U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v0;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[1U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v1;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[2U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v2;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[3U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v3;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[4U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v4;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[5U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v5;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[6U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v6;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[7U] 
            = __Vdlyvval__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v7;
    }
    if (__Vdlyvset__div_8_pipe__DOT__nrdiv_inst__DOT__b_ff__v8) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[0U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[1U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[2U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[3U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[4U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[5U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[6U] = 0U;
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[7U] = 0U;
    }
}

VL_INLINE_OPT void div_8_pipe___024root___combo__TOP__0(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___combo__TOP__0\n"); );
    // Body
    if (((IData)(vlSelf->sgn) & ((IData)(vlSelf->dnd) 
                                 >> 7U))) {
        vlSelf->div_8_pipe__DOT__dnd_eff = (0xffU & 
                                            ((IData)(1U) 
                                             + (~ (IData)(vlSelf->dnd))));
        vlSelf->r = (0xffU & ((IData)(1U) + (~ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w))));
    } else {
        vlSelf->div_8_pipe__DOT__dnd_eff = (0xffU & (IData)(vlSelf->dnd));
        vlSelf->r = (0xffU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w));
    }
    vlSelf->div_8_pipe__DOT__div_eff = (0xffU & (((IData)(vlSelf->sgn) 
                                                  & ((IData)(vlSelf->div) 
                                                     >> 7U))
                                                  ? 
                                                 ((IData)(1U) 
                                                  + 
                                                  (~ (IData)(vlSelf->div)))
                                                  : (IData)(vlSelf->div)));
    vlSelf->q = (0xffU & (((IData)(vlSelf->sgn) & (
                                                   ((IData)(vlSelf->dnd) 
                                                    ^ (IData)(vlSelf->div)) 
                                                   >> 7U))
                           ? ((IData)(1U) + (~ vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                             [7U]))
                           : vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                          [7U]));
}

#ifdef VL_DEBUG
void div_8_pipe___024root___eval_debug_assertions(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((vlSelf->clk & 0xfeU))) {
        Verilated::overWidthError("clk");}
    if (VL_UNLIKELY((vlSelf->nrst & 0xfeU))) {
        Verilated::overWidthError("nrst");}
    if (VL_UNLIKELY((vlSelf->sgn & 0xfeU))) {
        Verilated::overWidthError("sgn");}
}
#endif  // VL_DEBUG
