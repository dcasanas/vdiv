// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See nrdiv.h for the primary calling header

#include "verilated.h"

#include "nrdiv__Syms.h"
#include "nrdiv___024root.h"

void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(nrdiv_nrdiv_step* vlSelf);
void nrdiv___024root___sequent__TOP__1(nrdiv___024root* vlSelf);

VL_ATTR_COLD void nrdiv___024root___eval_settle(nrdiv___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root___eval_settle\n"); );
    // Body
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen));
    vlSelf->__Vm_traceActivity[1U] = 1U;
    vlSelf->__Vm_traceActivity[0U] = 1U;
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen));
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen));
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen));
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen));
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen));
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen));
    nrdiv_nrdiv_step___sequent__TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen));
    nrdiv___024root___sequent__TOP__1(vlSelf);
}
