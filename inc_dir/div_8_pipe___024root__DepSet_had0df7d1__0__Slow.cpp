// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See div_8_pipe.h for the primary calling header

#include "verilated.h"

#include "div_8_pipe___024root.h"

VL_ATTR_COLD void div_8_pipe___024root___settle__TOP__0(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___settle__TOP__0\n"); );
    // Body
    vlSelf->div_8_pipe__DOT__dnd_eff = (0xffU & (((IData)(vlSelf->sgn) 
                                                  & ((IData)(vlSelf->dnd) 
                                                     >> 7U))
                                                  ? 
                                                 ((IData)(1U) 
                                                  + 
                                                  (~ (IData)(vlSelf->dnd)))
                                                  : (IData)(vlSelf->dnd)));
    vlSelf->div_8_pipe__DOT__div_eff = (0xffU & (((IData)(vlSelf->sgn) 
                                                  & ((IData)(vlSelf->div) 
                                                     >> 7U))
                                                  ? 
                                                 ((IData)(1U) 
                                                  + 
                                                  (~ (IData)(vlSelf->div)))
                                                  : (IData)(vlSelf->div)));
}

VL_ATTR_COLD void div_8_pipe___024root___eval_initial(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___eval_initial\n"); );
    // Body
    vlSelf->__Vclklast__TOP__clk = vlSelf->clk;
    vlSelf->__Vclklast__TOP__nrst = vlSelf->nrst;
}

VL_ATTR_COLD void div_8_pipe___024root___final(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___final\n"); );
}

VL_ATTR_COLD void div_8_pipe___024root___ctor_var_reset(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___ctor_var_reset\n"); );
    // Body
    vlSelf->clk = VL_RAND_RESET_I(1);
    vlSelf->nrst = VL_RAND_RESET_I(1);
    vlSelf->dnd = VL_RAND_RESET_I(8);
    vlSelf->div = VL_RAND_RESET_I(8);
    vlSelf->sgn = VL_RAND_RESET_I(1);
    vlSelf->q = VL_RAND_RESET_I(8);
    vlSelf->r = VL_RAND_RESET_I(8);
    vlSelf->div_8_pipe__DOT__dnd_eff = VL_RAND_RESET_I(17);
    vlSelf->div_8_pipe__DOT__div_eff = VL_RAND_RESET_I(9);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[__Vi0] = VL_RAND_RESET_I(17);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[__Vi0] = VL_RAND_RESET_I(9);
    }
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[__Vi0] = VL_RAND_RESET_I(17);
    }
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w = VL_RAND_RESET_I(8);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w = VL_RAND_RESET_I(9);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w = VL_RAND_RESET_I(9);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk2__DOT__i = 0;
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk1__DOT__i = 0;
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry = VL_RAND_RESET_I(8);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g = VL_RAND_RESET_I(8);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p = VL_RAND_RESET_I(8);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 = VL_RAND_RESET_I(1);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        vlSelf->__Vm_traceActivity[__Vi0] = VL_RAND_RESET_I(1);
    }
}
