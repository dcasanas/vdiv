// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header,
// unless using verilator public meta comments.

#ifndef VERILATED_DIV_8_PIPE__SYMS_H_
#define VERILATED_DIV_8_PIPE__SYMS_H_  // guard

#include "verilated.h"

// INCLUDE MODEL CLASS

#include "div_8_pipe.h"

// INCLUDE MODULE CLASSES
#include "div_8_pipe___024root.h"
#include "div_8_pipe_nrdiv_step.h"

// SYMS CLASS (contains all model state)
class div_8_pipe__Syms final : public VerilatedSyms {
  public:
    // INTERNAL STATE
    div_8_pipe* const __Vm_modelp;
    bool __Vm_activity = false;  ///< Used by trace routines to determine change occurred
    uint32_t __Vm_baseCode = 0;  ///< Used by trace routines when tracing multiple models
    bool __Vm_didInit = false;

    // MODULE INSTANCE STATE
    div_8_pipe___024root           TOP;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen;
    div_8_pipe_nrdiv_step          TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen;

    // CONSTRUCTORS
    div_8_pipe__Syms(VerilatedContext* contextp, const char* namep, div_8_pipe* modelp);
    ~div_8_pipe__Syms();

    // METHODS
    const char* name() { return TOP.name(); }
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

#endif  // guard
