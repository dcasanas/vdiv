// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "nrdiv__Syms.h"


void nrdiv___024root__trace_chg_sub_0(nrdiv___024root* vlSelf, VerilatedVcd::Buffer* bufp);

void nrdiv___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_chg_top_0\n"); );
    // Init
    nrdiv___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<nrdiv___024root*>(voidSelf);
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    nrdiv___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void nrdiv___024root__trace_chg_sub_0(nrdiv___024root* vlSelf, VerilatedVcd::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[1U])) {
        bufp->chgIData(oldp+0,(vlSelf->nrdiv__DOT__rq_part_ff[0]),17);
        bufp->chgIData(oldp+1,(vlSelf->nrdiv__DOT__rq_part_ff[1]),17);
        bufp->chgIData(oldp+2,(vlSelf->nrdiv__DOT__rq_part_ff[2]),17);
        bufp->chgIData(oldp+3,(vlSelf->nrdiv__DOT__rq_part_ff[3]),17);
        bufp->chgIData(oldp+4,(vlSelf->nrdiv__DOT__rq_part_ff[4]),17);
        bufp->chgIData(oldp+5,(vlSelf->nrdiv__DOT__rq_part_ff[5]),17);
        bufp->chgIData(oldp+6,(vlSelf->nrdiv__DOT__rq_part_ff[6]),17);
        bufp->chgIData(oldp+7,(vlSelf->nrdiv__DOT__rq_part_ff[7]),17);
        bufp->chgSData(oldp+8,(vlSelf->nrdiv__DOT__b_ff[0]),9);
        bufp->chgSData(oldp+9,(vlSelf->nrdiv__DOT__b_ff[1]),9);
        bufp->chgSData(oldp+10,(vlSelf->nrdiv__DOT__b_ff[2]),9);
        bufp->chgSData(oldp+11,(vlSelf->nrdiv__DOT__b_ff[3]),9);
        bufp->chgSData(oldp+12,(vlSelf->nrdiv__DOT__b_ff[4]),9);
        bufp->chgSData(oldp+13,(vlSelf->nrdiv__DOT__b_ff[5]),9);
        bufp->chgSData(oldp+14,(vlSelf->nrdiv__DOT__b_ff[6]),9);
        bufp->chgSData(oldp+15,(vlSelf->nrdiv__DOT__b_ff[7]),9);
        bufp->chgCData(oldp+16,(vlSelf->nrdiv__DOT__nxt_add_sub_ff),8);
        bufp->chgIData(oldp+17,(vlSelf->nrdiv__DOT__rq_part_w[0]),17);
        bufp->chgIData(oldp+18,(vlSelf->nrdiv__DOT__rq_part_w[1]),17);
        bufp->chgIData(oldp+19,(vlSelf->nrdiv__DOT__rq_part_w[2]),17);
        bufp->chgIData(oldp+20,(vlSelf->nrdiv__DOT__rq_part_w[3]),17);
        bufp->chgIData(oldp+21,(vlSelf->nrdiv__DOT__rq_part_w[4]),17);
        bufp->chgIData(oldp+22,(vlSelf->nrdiv__DOT__rq_part_w[5]),17);
        bufp->chgIData(oldp+23,(vlSelf->nrdiv__DOT__rq_part_w[6]),17);
        bufp->chgIData(oldp+24,(vlSelf->nrdiv__DOT__rq_part_w[7]),17);
        bufp->chgCData(oldp+25,(vlSelf->nrdiv__DOT__nxt_add_sub_w),8);
        bufp->chgSData(oldp+26,(vlSelf->nrdiv__DOT__r_corr_w),9);
        bufp->chgSData(oldp+27,(vlSelf->nrdiv__DOT__r_w),9);
        bufp->chgSData(oldp+28,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_w
                                           [7U] >> 8U))),9);
        bufp->chgBit(oldp+29,((1U & (((vlSelf->nrdiv__DOT__rq_part_w
                                       [7U] >> 0x10U) 
                                      & ((IData)(vlSelf->nrdiv__DOT__r_corr_w) 
                                         >> 8U)) | 
                                     ((((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      & ((vlSelf->nrdiv__DOT__rq_part_w
                                          [7U] >> 0x10U) 
                                         ^ ((IData)(vlSelf->nrdiv__DOT__r_corr_w) 
                                            >> 8U)))))));
        bufp->chgBit(oldp+30,((1U & (((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                     | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                        & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgCData(oldp+31,((0xffU & (vlSelf->nrdiv__DOT__rq_part_w
                                          [7U] >> 8U))),8);
        bufp->chgCData(oldp+32,((0xffU & (IData)(vlSelf->nrdiv__DOT__r_corr_w))),8);
        bufp->chgCData(oldp+33,((0xffU & (((vlSelf->nrdiv__DOT__rq_part_w
                                            [7U] >> 8U) 
                                           ^ (IData)(vlSelf->nrdiv__DOT__r_corr_w)) 
                                          ^ (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+34,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+35,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+36,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+37,((1U & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+38,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+39,((IData)((3U == (3U & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))))));
        bufp->chgBit(oldp+40,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+41,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+42,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+43,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+44,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+45,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+46,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+47,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+48,((1U & (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))))));
        bufp->chgBit(oldp+49,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+50,((1U & (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))))));
        bufp->chgBit(oldp+51,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+52,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+53,((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U)) & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41))));
        bufp->chgBit(oldp+54,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                               | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                  & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+55,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                               & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+56,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                               | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                  & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+57,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                               & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+58,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+59,((1U & ((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))) 
                                     | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                        & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+60,(((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                               & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+61,((1U & ((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+62,(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+63,(((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                 & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                    >> 1U)) & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)) 
                               & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+64,((1U & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+65,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 1U))));
        bufp->chgBit(oldp+66,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 1U))));
        bufp->chgBit(oldp+67,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 2U))));
        bufp->chgBit(oldp+68,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 2U))));
        bufp->chgBit(oldp+69,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 3U))));
        bufp->chgBit(oldp+70,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 3U))));
        bufp->chgBit(oldp+71,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 4U))));
        bufp->chgBit(oldp+72,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 4U))));
        bufp->chgBit(oldp+73,((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                               | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                     & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+74,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 5U))));
        bufp->chgBit(oldp+75,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 5U))));
        bufp->chgBit(oldp+76,((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                               | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
                                  & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                     & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+77,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U))));
        bufp->chgBit(oldp+78,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 6U))));
        bufp->chgBit(oldp+79,((1U & (((((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                     | ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+80,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U))));
        bufp->chgBit(oldp+81,((1U & ((IData)(vlSelf->nrdiv__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 7U))));
        bufp->chgIData(oldp+82,(vlSelf->nrdiv__DOT__unnamedblk1__DOT__i),32);
        bufp->chgIData(oldp+83,(vlSelf->nrdiv__DOT__unnamedblk2__DOT__i),32);
        bufp->chgIData(oldp+84,(vlSelf->nrdiv__DOT__rq_part_ff
                                [0U]),17);
        bufp->chgSData(oldp+85,(vlSelf->nrdiv__DOT__b_ff
                                [0U]),9);
        bufp->chgBit(oldp+86,((1U & (IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff))));
        bufp->chgIData(oldp+87,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                  << 8U) | ((0xfeU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [0U] 
                                                << 1U)) 
                                            | (1U & 
                                               (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                   >> 8U)))))),17);
        bufp->chgBit(oldp+88,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                        >> 8U)))));
        bufp->chgIData(oldp+89,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                             [0U] << 1U))),17);
        bufp->chgSData(oldp+90,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+91,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+92,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [0U] >> 7U))),9);
        bufp->chgBit(oldp+93,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U) 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [0U] 
                                                 >> 0xfU))) 
                                     | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                             >> 7U) 
                                            | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                          | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                             & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                         | ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                            & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                        & ((1U & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [0U] 
                                                  >> 0xfU)) 
                                           ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                              >> 8U)))))));
        bufp->chgBit(oldp+94,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                     | ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                        & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+95,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                          [0U] >> 7U))),8);
        bufp->chgCData(oldp+96,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+97,((0xffU & (((0x1ffU 
                                            & (vlSelf->nrdiv__DOT__rq_part_ff
                                               [0U] 
                                               >> 7U)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                          ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+98,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+99,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+100,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+101,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+102,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+103,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+104,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+105,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+106,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+107,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+108,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+109,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+110,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+111,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+112,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+113,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+114,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+115,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+116,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+117,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+118,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+119,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+120,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+121,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+122,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+123,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+124,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+125,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+126,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+127,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+128,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+129,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+130,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+131,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+132,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+133,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+134,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+135,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+136,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+137,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+138,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+139,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+140,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+141,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+142,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+143,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+144,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+145,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+146,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+147,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+148,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+149,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+150,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [1U]),17);
        bufp->chgSData(oldp+151,(vlSelf->nrdiv__DOT__b_ff
                                 [1U]),9);
        bufp->chgBit(oldp+152,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 1U))));
        bufp->chgIData(oldp+153,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [1U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+154,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+155,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [1U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+156,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+157,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+158,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [1U] >> 7U))),9);
        bufp->chgBit(oldp+159,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [1U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 1U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [1U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+160,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 1U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+161,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [1U] >> 7U))),8);
        bufp->chgCData(oldp+162,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+163,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [1U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+164,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+165,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+166,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+167,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+168,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+169,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+170,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+171,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+172,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+173,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+174,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+175,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+176,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+177,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+178,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+179,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+180,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+181,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+182,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+183,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+184,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+185,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+186,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+187,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+188,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+189,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+190,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+191,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+192,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+193,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+194,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+195,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+196,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+197,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+198,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+199,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+200,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+201,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+202,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+203,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+204,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 1U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+205,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+206,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+207,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+208,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+209,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+210,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+211,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+212,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+213,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+214,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+215,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+216,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [2U]),17);
        bufp->chgSData(oldp+217,(vlSelf->nrdiv__DOT__b_ff
                                 [2U]),9);
        bufp->chgBit(oldp+218,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 2U))));
        bufp->chgIData(oldp+219,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [2U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+220,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+221,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [2U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+222,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+223,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+224,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [2U] >> 7U))),9);
        bufp->chgBit(oldp+225,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [2U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 2U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [2U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+226,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 2U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+227,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [2U] >> 7U))),8);
        bufp->chgCData(oldp+228,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+229,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [2U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+230,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+231,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+232,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+233,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+234,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+235,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+236,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+237,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+238,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+239,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+240,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+241,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+242,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+243,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+244,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+245,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+246,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+247,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+248,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+249,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+250,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+251,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+252,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+253,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+254,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+255,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+256,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+257,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+258,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+259,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+260,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+261,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+262,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+263,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+264,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+265,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+266,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+267,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+268,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+269,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+270,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 2U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+271,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+272,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+273,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+274,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+275,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+276,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+277,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+278,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+279,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+280,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+281,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+282,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [3U]),17);
        bufp->chgSData(oldp+283,(vlSelf->nrdiv__DOT__b_ff
                                 [3U]),9);
        bufp->chgBit(oldp+284,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 3U))));
        bufp->chgIData(oldp+285,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [3U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+286,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+287,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [3U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+288,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+289,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+290,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [3U] >> 7U))),9);
        bufp->chgBit(oldp+291,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [3U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 3U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [3U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+292,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 3U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+293,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [3U] >> 7U))),8);
        bufp->chgCData(oldp+294,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+295,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [3U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+296,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+297,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+298,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+299,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+300,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+301,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+302,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+303,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+304,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+305,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+306,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+307,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+308,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+309,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+310,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+311,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+312,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+313,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+314,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+315,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+316,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+317,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+318,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+319,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+320,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+321,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+322,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+323,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+324,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+325,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+326,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+327,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+328,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+329,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+330,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+331,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+332,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+333,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+334,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+335,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+336,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 3U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+337,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+338,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+339,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+340,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+341,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+342,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+343,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+344,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+345,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+346,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+347,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+348,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [4U]),17);
        bufp->chgSData(oldp+349,(vlSelf->nrdiv__DOT__b_ff
                                 [4U]),9);
        bufp->chgBit(oldp+350,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 4U))));
        bufp->chgIData(oldp+351,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [4U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+352,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+353,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [4U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+354,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+355,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+356,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [4U] >> 7U))),9);
        bufp->chgBit(oldp+357,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [4U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 4U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [4U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+358,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 4U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+359,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [4U] >> 7U))),8);
        bufp->chgCData(oldp+360,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+361,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [4U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+362,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+363,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+364,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+365,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+366,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+367,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+368,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+369,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+370,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+371,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+372,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+373,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+374,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+375,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+376,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+377,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+378,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+379,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+380,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+381,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+382,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+383,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+384,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+385,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+386,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+387,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+388,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+389,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+390,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+391,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+392,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+393,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+394,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+395,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+396,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+397,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+398,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+399,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+400,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+401,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+402,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 4U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+403,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+404,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+405,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+406,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+407,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+408,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+409,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+410,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+411,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+412,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+413,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+414,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [5U]),17);
        bufp->chgSData(oldp+415,(vlSelf->nrdiv__DOT__b_ff
                                 [5U]),9);
        bufp->chgBit(oldp+416,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 5U))));
        bufp->chgIData(oldp+417,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [5U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+418,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+419,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [5U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+420,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+421,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+422,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [5U] >> 7U))),9);
        bufp->chgBit(oldp+423,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [5U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 5U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [5U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+424,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 5U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+425,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [5U] >> 7U))),8);
        bufp->chgCData(oldp+426,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+427,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [5U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+428,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+429,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+430,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+431,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+432,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+433,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+434,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+435,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+436,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+437,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+438,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+439,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+440,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+441,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+442,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+443,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+444,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+445,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+446,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+447,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+448,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+449,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+450,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+451,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+452,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+453,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+454,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+455,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+456,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+457,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+458,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+459,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+460,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+461,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+462,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+463,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+464,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+465,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+466,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+467,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+468,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 5U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+469,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+470,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+471,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+472,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+473,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+474,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+475,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+476,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+477,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+478,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+479,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+480,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [6U]),17);
        bufp->chgSData(oldp+481,(vlSelf->nrdiv__DOT__b_ff
                                 [6U]),9);
        bufp->chgBit(oldp+482,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 6U))));
        bufp->chgIData(oldp+483,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [6U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+484,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+485,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [6U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+486,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+487,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+488,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [6U] >> 7U))),9);
        bufp->chgBit(oldp+489,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [6U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 6U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [6U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+490,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 6U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+491,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [6U] >> 7U))),8);
        bufp->chgCData(oldp+492,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+493,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [6U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+494,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+495,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+496,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+497,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+498,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+499,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+500,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+501,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+502,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+503,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+504,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+505,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+506,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+507,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+508,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+509,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+510,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+511,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+512,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+513,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+514,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+515,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+516,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+517,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+518,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+519,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+520,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+521,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+522,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+523,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+524,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+525,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+526,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+527,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+528,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+529,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+530,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+531,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+532,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+533,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+534,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 6U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+535,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+536,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+537,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+538,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+539,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+540,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+541,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+542,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+543,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+544,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+545,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+546,(vlSelf->nrdiv__DOT__rq_part_ff
                                 [7U]),17);
        bufp->chgSData(oldp+547,(vlSelf->nrdiv__DOT__b_ff
                                 [7U]),9);
        bufp->chgBit(oldp+548,((1U & ((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                      >> 7U))));
        bufp->chgIData(oldp+549,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->nrdiv__DOT__rq_part_ff
                                                 [7U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+550,((1U & (~ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+551,((0x1ffffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                              [7U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+552,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+553,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+554,((0x1ffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                            [7U] >> 7U))),9);
        bufp->chgBit(oldp+555,((1U & ((IData)((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->nrdiv__DOT__rq_part_ff
                                                  [7U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                              >> 7U) 
                                             & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->nrdiv__DOT__rq_part_ff
                                                   [7U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+556,((1U & ((((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                          >> 7U) & 
                                         ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+557,((0xffU & (vlSelf->nrdiv__DOT__rq_part_ff
                                           [7U] >> 7U))),8);
        bufp->chgCData(oldp+558,((0xffU & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+559,((0xffU & (((0x1ffU 
                                             & (vlSelf->nrdiv__DOT__rq_part_ff
                                                [7U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+560,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+561,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+562,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+563,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+564,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+565,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+566,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+567,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+568,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+569,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+570,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+571,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+572,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+573,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+574,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+575,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+576,((1U & (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+577,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+578,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+579,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+580,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+581,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+582,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+583,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+584,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+585,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+586,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+587,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+588,((1U & ((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+589,(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+590,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+591,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+592,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+593,((1U & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+594,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+595,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+596,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+597,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+598,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+599,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+600,(((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->nrdiv__DOT__nxt_add_sub_ff) 
                                    >> 7U) & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+601,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+602,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+603,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+604,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+605,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+606,((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+607,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+608,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+609,((1U & (((((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+610,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+611,((1U & ((IData)(vlSymsp->TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
    }
    bufp->chgBit(oldp+612,(vlSelf->clk));
    bufp->chgBit(oldp+613,(vlSelf->nrst));
    bufp->chgIData(oldp+614,(vlSelf->a),17);
    bufp->chgSData(oldp+615,(vlSelf->b),9);
    bufp->chgCData(oldp+616,(vlSelf->q),8);
    bufp->chgCData(oldp+617,(vlSelf->r),8);
}

void nrdiv___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    nrdiv___024root__trace_cleanup\n"); );
    // Init
    nrdiv___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<nrdiv___024root*>(voidSelf);
    nrdiv__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
}
