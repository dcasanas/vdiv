// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header,
// unless using verilator public meta comments.

#ifndef VERILATED_NRDIV__SYMS_H_
#define VERILATED_NRDIV__SYMS_H_  // guard

#include "verilated.h"

// INCLUDE MODEL CLASS

#include "nrdiv.h"

// INCLUDE MODULE CLASSES
#include "nrdiv___024root.h"
#include "nrdiv_nrdiv_step.h"

// SYMS CLASS (contains all model state)
class nrdiv__Syms final : public VerilatedSyms {
  public:
    // INTERNAL STATE
    nrdiv* const __Vm_modelp;
    bool __Vm_activity = false;  ///< Used by trace routines to determine change occurred
    uint32_t __Vm_baseCode = 0;  ///< Used by trace routines when tracing multiple models
    bool __Vm_didInit = false;

    // MODULE INSTANCE STATE
    nrdiv___024root                TOP;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen;
    nrdiv_nrdiv_step               TOP__nrdiv__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen;

    // CONSTRUCTORS
    nrdiv__Syms(VerilatedContext* contextp, const char* namep, nrdiv* modelp);
    ~nrdiv__Syms();

    // METHODS
    const char* name() { return TOP.name(); }
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

#endif  // guard
