// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See div_8_pipe.h for the primary calling header

#include "verilated.h"

#include "div_8_pipe__Syms.h"
#include "div_8_pipe___024root.h"

VL_INLINE_OPT void div_8_pipe___024root___sequent__TOP__1(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___sequent__TOP__1\n"); );
    // Body
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xfeU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                       >> 8U))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[0U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [0U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xfdU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (2U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                        >> 8U)) << 1U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[1U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [1U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xfbU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (4U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                        >> 8U)) << 2U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[2U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [2U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xf7U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (8U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                        >> 8U)) << 3U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[3U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [3U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xefU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (0x10U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                           >> 8U)) << 4U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[4U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [4U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xdfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (0x20U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                           >> 8U)) << 5U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[5U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [5U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0xbfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (0x40U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                           >> 8U)) << 6U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[6U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [6U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w 
        = ((0x7fU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w)) 
           | (0x80U & ((~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                           >> 8U)) << 7U)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[7U] 
        = (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
            << 8U) | ((0xfeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [7U] << 1U)) | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w 
        = ((0x10000U & vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
            [7U]) ? vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
           [7U] : 0U);
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfeU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (1U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                     [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfdU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (2U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                     [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xfbU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (4U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                     [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xf7U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (8U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                     [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xefU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x10U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                        [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xdfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x20U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                        [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0xbfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x40U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                        [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g 
        = ((0x7fU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g)) 
           | (0x80U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                        [7U] >> 8U) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfeU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (1U & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                     [7U] >> 8U) ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfdU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (2U & ((0xfffffeU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                  [7U] >> 8U)) ^ (0xfffffffeU 
                                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xfbU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (4U & ((0xfffffcU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                  [7U] >> 8U)) ^ (0xfffffffcU 
                                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xf7U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (8U & ((0xfffff8U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                  [7U] >> 8U)) ^ (0xfffffff8U 
                                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xefU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x10U & ((0xfffff0U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                     [7U] >> 8U)) ^ 
                       (0xfffffff0U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xdfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x20U & ((0xffffe0U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                     [7U] >> 8U)) ^ 
                       (0xffffffe0U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0xbfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x40U & ((0xffffc0U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                     [7U] >> 8U)) ^ 
                       (0xffffffc0U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p 
        = ((0x7fU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p)) 
           | (0x80U & ((0xffff80U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                     [7U] >> 8U)) ^ 
                       (0xffffff80U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51 
        = (IData)((0x18U == (0x18U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71 
        = (IData)((0x60U == (0x60U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51 
        = (1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 4U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 3U) & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 4U))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81 
        = (IData)((0xc0U == (0xc0U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61 
        = (1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 5U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 4U) & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 5U))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61 
        = (IData)((0x30U == (0x30U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41 
        = (IData)((0xcU == (0xcU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41 
        = (1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 3U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 2U) & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 3U))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21 
        = (1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 1U) | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                            & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                               >> 1U))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31 
        = (IData)((6U == (6U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31 
        = (1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                  >> 2U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                             >> 1U) & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                       >> 2U))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82 
        = ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61) 
           & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42 
        = ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
           | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
              & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current 
        = ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
           | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
              & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31)));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xf0U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry)) 
           | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
               << 3U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
                          << 2U) | (2U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry 
        = ((0xfU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry)) 
           | ((0x80U & (((0xffffff80U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          << 1U) | 
                                         (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           << 2U) & 
                                          ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           << 1U)))) 
                         | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                             & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)) 
                            << 7U)) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                       << 7U))) | (
                                                   ((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                                      | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                                         & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                                     | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
                                                        & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                                           & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))) 
                                                    << 6U) 
                                                   | (((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                                         | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                                            & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                                        | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                           & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                                              & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))) 
                                                       << 5U) 
                                                      | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                                         << 4U)))));
    vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w = 
        ((0x100U & (((0xffff00U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                   [7U] >> 8U)) ^ (0xffffff00U 
                                                   & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))) 
                    ^ (((0xffffff00U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         << 1U) | (
                                                   ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    << 2U) 
                                                   & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      << 1U)))) 
                        | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                            & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81)) 
                           << 8U)) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                       & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)) 
                                      << 8U)))) | (0xffU 
                                                   & (((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                                        [7U] 
                                                        >> 8U) 
                                                       ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)) 
                                                      ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry))));
}

void div_8_pipe___024root___sequent__TOP__0(div_8_pipe___024root* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0(div_8_pipe_nrdiv_step* vlSelf);
void div_8_pipe___024root___combo__TOP__0(div_8_pipe___024root* vlSelf);

void div_8_pipe___024root___eval(div_8_pipe___024root* vlSelf) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root___eval\n"); );
    // Body
    if ((((IData)(vlSelf->clk) & (~ (IData)(vlSelf->__Vclklast__TOP__clk))) 
         | ((~ (IData)(vlSelf->nrst)) & (IData)(vlSelf->__Vclklast__TOP__nrst)))) {
        div_8_pipe___024root___sequent__TOP__0(vlSelf);
        vlSelf->__Vm_traceActivity[1U] = 1U;
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen));
        div_8_pipe_nrdiv_step___sequent__TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen__0((&vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen));
        div_8_pipe___024root___sequent__TOP__1(vlSelf);
    }
    div_8_pipe___024root___combo__TOP__0(vlSelf);
    // Final
    vlSelf->__Vclklast__TOP__clk = vlSelf->clk;
    vlSelf->__Vclklast__TOP__nrst = vlSelf->nrst;
}
