// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Model implementation (design independent parts)

#include "div_8_pipe.h"
#include "div_8_pipe__Syms.h"
#include "verilated_vcd_c.h"

//============================================================
// Constructors

div_8_pipe::div_8_pipe(VerilatedContext* _vcontextp__, const char* _vcname__)
    : VerilatedModel{*_vcontextp__}
    , vlSymsp{new div_8_pipe__Syms(contextp(), _vcname__, this)}
    , clk{vlSymsp->TOP.clk}
    , nrst{vlSymsp->TOP.nrst}
    , dnd{vlSymsp->TOP.dnd}
    , div{vlSymsp->TOP.div}
    , sgn{vlSymsp->TOP.sgn}
    , q{vlSymsp->TOP.q}
    , r{vlSymsp->TOP.r}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen}
    , __PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen{vlSymsp->TOP.__PVT__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen}
    , rootp{&(vlSymsp->TOP)}
{
    // Register model with the context
    contextp()->addModel(this);
}

div_8_pipe::div_8_pipe(const char* _vcname__)
    : div_8_pipe(Verilated::threadContextp(), _vcname__)
{
}

//============================================================
// Destructor

div_8_pipe::~div_8_pipe() {
    delete vlSymsp;
}

//============================================================
// Evaluation loop

void div_8_pipe___024root___eval_initial(div_8_pipe___024root* vlSelf);
void div_8_pipe___024root___eval_settle(div_8_pipe___024root* vlSelf);
void div_8_pipe___024root___eval(div_8_pipe___024root* vlSelf);
#ifdef VL_DEBUG
void div_8_pipe___024root___eval_debug_assertions(div_8_pipe___024root* vlSelf);
#endif  // VL_DEBUG
void div_8_pipe___024root___final(div_8_pipe___024root* vlSelf);

static void _eval_initial_loop(div_8_pipe__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    div_8_pipe___024root___eval_initial(&(vlSymsp->TOP));
    // Evaluate till stable
    vlSymsp->__Vm_activity = true;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Initial loop\n"););
        div_8_pipe___024root___eval_settle(&(vlSymsp->TOP));
        div_8_pipe___024root___eval(&(vlSymsp->TOP));
    } while (0);
}

void div_8_pipe::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate div_8_pipe::eval_step\n"); );
#ifdef VL_DEBUG
    // Debug assertions
    div_8_pipe___024root___eval_debug_assertions(&(vlSymsp->TOP));
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    vlSymsp->__Vm_activity = true;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        div_8_pipe___024root___eval(&(vlSymsp->TOP));
    } while (0);
    // Evaluate cleanup
}

//============================================================
// Utilities

const char* div_8_pipe::name() const {
    return vlSymsp->name();
}

//============================================================
// Invoke final blocks

VL_ATTR_COLD void div_8_pipe::final() {
    div_8_pipe___024root___final(&(vlSymsp->TOP));
}

//============================================================
// Implementations of abstract methods from VerilatedModel

const char* div_8_pipe::hierName() const { return vlSymsp->name(); }
const char* div_8_pipe::modelName() const { return "div_8_pipe"; }
unsigned div_8_pipe::threads() const { return 1; }
std::unique_ptr<VerilatedTraceConfig> div_8_pipe::traceConfig() const {
    return std::unique_ptr<VerilatedTraceConfig>{new VerilatedTraceConfig{false, false, false}};
};

//============================================================
// Trace configuration

void div_8_pipe___024root__trace_init_top(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD static void trace_init(void* voidSelf, VerilatedVcd* tracep, uint32_t code) {
    // Callback from tracep->open()
    div_8_pipe___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<div_8_pipe___024root*>(voidSelf);
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (!vlSymsp->_vm_contextp__->calcUnusedSigs()) {
        VL_FATAL_MT(__FILE__, __LINE__, __FILE__,
            "Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    }
    vlSymsp->__Vm_baseCode = code;
    tracep->scopeEscape(' ');
    tracep->pushNamePrefix(std::string{vlSymsp->name()} + ' ');
    div_8_pipe___024root__trace_init_top(vlSelf, tracep);
    tracep->popNamePrefix();
    tracep->scopeEscape('.');
}

VL_ATTR_COLD void div_8_pipe___024root__trace_register(div_8_pipe___024root* vlSelf, VerilatedVcd* tracep);

VL_ATTR_COLD void div_8_pipe::trace(VerilatedVcdC* tfp, int levels, int options) {
    if (false && levels && options) {}  // Prevent unused
    tfp->spTrace()->addModel(this);
    tfp->spTrace()->addInitCb(&trace_init, &(vlSymsp->TOP));
    div_8_pipe___024root__trace_register(&(vlSymsp->TOP), tfp->spTrace());
}
