// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "div_8_pipe__Syms.h"


void div_8_pipe___024root__trace_chg_sub_0(div_8_pipe___024root* vlSelf, VerilatedVcd::Buffer* bufp);

void div_8_pipe___024root__trace_chg_top_0(void* voidSelf, VerilatedVcd::Buffer* bufp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_chg_top_0\n"); );
    // Init
    div_8_pipe___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<div_8_pipe___024root*>(voidSelf);
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    if (VL_UNLIKELY(!vlSymsp->__Vm_activity)) return;
    // Body
    div_8_pipe___024root__trace_chg_sub_0((&vlSymsp->TOP), bufp);
}

void div_8_pipe___024root__trace_chg_sub_0(div_8_pipe___024root* vlSelf, VerilatedVcd::Buffer* bufp) {
    if (false && vlSelf) {}  // Prevent unused
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_chg_sub_0\n"); );
    // Init
    uint32_t* const oldp VL_ATTR_UNUSED = bufp->oldp(vlSymsp->__Vm_baseCode + 1);
    // Body
    if (VL_UNLIKELY(vlSelf->__Vm_traceActivity[1U])) {
        bufp->chgCData(oldp+0,((0xffU & vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                [7U])),8);
        bufp->chgCData(oldp+1,((0xffU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w))),8);
        bufp->chgCData(oldp+2,((0xffU & ((IData)(1U) 
                                         + (~ vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                            [7U])))),8);
        bufp->chgCData(oldp+3,((0xffU & ((IData)(1U) 
                                         + (~ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w))))),8);
        bufp->chgIData(oldp+4,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[0]),17);
        bufp->chgIData(oldp+5,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[1]),17);
        bufp->chgIData(oldp+6,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[2]),17);
        bufp->chgIData(oldp+7,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[3]),17);
        bufp->chgIData(oldp+8,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[4]),17);
        bufp->chgIData(oldp+9,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[5]),17);
        bufp->chgIData(oldp+10,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[6]),17);
        bufp->chgIData(oldp+11,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff[7]),17);
        bufp->chgSData(oldp+12,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[0]),9);
        bufp->chgSData(oldp+13,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[1]),9);
        bufp->chgSData(oldp+14,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[2]),9);
        bufp->chgSData(oldp+15,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[3]),9);
        bufp->chgSData(oldp+16,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[4]),9);
        bufp->chgSData(oldp+17,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[5]),9);
        bufp->chgSData(oldp+18,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[6]),9);
        bufp->chgSData(oldp+19,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff[7]),9);
        bufp->chgCData(oldp+20,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff),8);
        bufp->chgIData(oldp+21,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[0]),17);
        bufp->chgIData(oldp+22,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[1]),17);
        bufp->chgIData(oldp+23,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[2]),17);
        bufp->chgIData(oldp+24,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[3]),17);
        bufp->chgIData(oldp+25,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[4]),17);
        bufp->chgIData(oldp+26,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[5]),17);
        bufp->chgIData(oldp+27,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[6]),17);
        bufp->chgIData(oldp+28,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w[7]),17);
        bufp->chgCData(oldp+29,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_w),8);
        bufp->chgSData(oldp+30,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w),9);
        bufp->chgSData(oldp+31,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_w),9);
        bufp->chgSData(oldp+32,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                           [7U] >> 8U))),9);
        bufp->chgBit(oldp+33,((1U & (((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                       [7U] >> 0x10U) 
                                      & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w) 
                                         >> 8U)) | 
                                     ((((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      & ((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                          [7U] >> 0x10U) 
                                         ^ ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w) 
                                            >> 8U)))))));
        bufp->chgBit(oldp+34,((1U & (((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                     | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                        & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgCData(oldp+35,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                          [7U] >> 8U))),8);
        bufp->chgCData(oldp+36,((0xffU & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w))),8);
        bufp->chgCData(oldp+37,((0xffU & (((vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_w
                                            [7U] >> 8U) 
                                           ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__r_corr_w)) 
                                          ^ (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+38,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+39,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+40,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+41,((1U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+42,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+43,((IData)((3U == (3U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))))));
        bufp->chgBit(oldp+44,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+45,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+46,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+47,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+48,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+49,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+50,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+51,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+52,((1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 5U) 
                                                & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 6U))))));
        bufp->chgBit(oldp+53,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+54,((1U & (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))))));
        bufp->chgBit(oldp+55,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+56,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+57,((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                   >> 1U)) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41))));
        bufp->chgBit(oldp+58,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                               | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+59,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                               & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+60,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                               | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                  & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+61,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                               & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+62,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+63,((1U & ((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))) 
                                     | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                        & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+64,(((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                               & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+65,((1U & ((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))) 
                                     | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                        & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+66,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+67,(((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                 & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                    >> 1U)) & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41)) 
                               & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+68,((1U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+69,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 1U))));
        bufp->chgBit(oldp+70,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 1U))));
        bufp->chgBit(oldp+71,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 2U))));
        bufp->chgBit(oldp+72,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 2U))));
        bufp->chgBit(oldp+73,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 3U))));
        bufp->chgBit(oldp+74,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 3U))));
        bufp->chgBit(oldp+75,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 4U))));
        bufp->chgBit(oldp+76,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 4U))));
        bufp->chgBit(oldp+77,((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                               | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                  & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                     & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+78,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 5U))));
        bufp->chgBit(oldp+79,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 5U))));
        bufp->chgBit(oldp+80,((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                               | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21) 
                                  & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                     & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+81,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 6U))));
        bufp->chgBit(oldp+82,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 6U))));
        bufp->chgBit(oldp+83,((1U & (((((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                     | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                        & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                           & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+84,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                     >> 7U))));
        bufp->chgBit(oldp+85,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__add_9_inst_0__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                     >> 7U))));
        bufp->chgIData(oldp+86,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk1__DOT__i),32);
        bufp->chgIData(oldp+87,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__unnamedblk2__DOT__i),32);
        bufp->chgIData(oldp+88,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                [0U]),17);
        bufp->chgSData(oldp+89,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                [0U]),9);
        bufp->chgBit(oldp+90,((1U & (IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff))));
        bufp->chgIData(oldp+91,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                  << 8U) | ((0xfeU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [0U] 
                                                << 1U)) 
                                            | (1U & 
                                               (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                   >> 8U)))))),17);
        bufp->chgBit(oldp+92,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                        >> 8U)))));
        bufp->chgIData(oldp+93,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                             [0U] << 1U))),17);
        bufp->chgSData(oldp+94,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+95,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+96,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [0U] >> 7U))),9);
        bufp->chgBit(oldp+97,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U) 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [0U] 
                                                 >> 0xfU))) 
                                     | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                             >> 7U) 
                                            | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                >> 6U) 
                                               & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                  >> 7U))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                          | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                             & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                         | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                            & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                        & ((1U & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [0U] 
                                                  >> 0xfU)) 
                                           ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                              >> 8U)))))));
        bufp->chgBit(oldp+98,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                     | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                        & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+99,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                          [0U] >> 7U))),8);
        bufp->chgCData(oldp+100,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+101,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [0U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+102,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+103,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+104,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+105,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+106,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+107,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+108,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+109,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+110,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+111,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+112,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+113,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+114,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+115,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+116,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+117,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+118,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+119,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+120,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+121,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+122,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+123,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+124,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+125,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+126,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+127,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+128,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+129,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+130,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+131,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+132,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+133,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+134,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+135,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+136,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+137,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+138,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+139,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+140,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+141,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+142,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+143,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+144,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+145,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+146,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+147,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+148,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+149,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+150,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+151,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+152,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+153,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__0__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+154,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [1U]),17);
        bufp->chgSData(oldp+155,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [1U]),9);
        bufp->chgBit(oldp+156,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 1U))));
        bufp->chgIData(oldp+157,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [1U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+158,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+159,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [1U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+160,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+161,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+162,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [1U] >> 7U))),9);
        bufp->chgBit(oldp+163,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [1U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 1U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [1U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+164,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 1U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+165,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [1U] >> 7U))),8);
        bufp->chgCData(oldp+166,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+167,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [1U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+168,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+169,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+170,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+171,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+172,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+173,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+174,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+175,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+176,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+177,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+178,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+179,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+180,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+181,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+182,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+183,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+184,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+185,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+186,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+187,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+188,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+189,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+190,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+191,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+192,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+193,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+194,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+195,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+196,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+197,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+198,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+199,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+200,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+201,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+202,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+203,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+204,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+205,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+206,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+207,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+208,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 1U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+209,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+210,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+211,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+212,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+213,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+214,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+215,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+216,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+217,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+218,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+219,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__1__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+220,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [2U]),17);
        bufp->chgSData(oldp+221,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [2U]),9);
        bufp->chgBit(oldp+222,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 2U))));
        bufp->chgIData(oldp+223,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [2U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+224,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+225,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [2U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+226,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+227,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+228,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [2U] >> 7U))),9);
        bufp->chgBit(oldp+229,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [2U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 2U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [2U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+230,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 2U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+231,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [2U] >> 7U))),8);
        bufp->chgCData(oldp+232,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+233,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [2U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+234,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+235,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+236,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+237,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+238,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+239,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+240,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+241,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+242,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+243,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+244,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+245,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+246,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+247,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+248,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+249,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+250,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+251,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+252,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+253,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+254,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+255,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+256,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+257,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+258,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+259,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+260,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+261,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+262,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+263,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+264,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+265,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+266,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+267,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+268,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+269,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+270,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+271,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+272,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+273,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+274,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 2U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+275,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+276,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+277,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+278,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+279,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+280,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+281,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+282,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+283,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+284,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+285,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__2__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+286,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [3U]),17);
        bufp->chgSData(oldp+287,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [3U]),9);
        bufp->chgBit(oldp+288,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 3U))));
        bufp->chgIData(oldp+289,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [3U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+290,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+291,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [3U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+292,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+293,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+294,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [3U] >> 7U))),9);
        bufp->chgBit(oldp+295,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [3U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 3U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [3U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+296,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 3U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+297,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [3U] >> 7U))),8);
        bufp->chgCData(oldp+298,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+299,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [3U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+300,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+301,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+302,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+303,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+304,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+305,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+306,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+307,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+308,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+309,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+310,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+311,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+312,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+313,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+314,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+315,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+316,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+317,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+318,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+319,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+320,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+321,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+322,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+323,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+324,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+325,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+326,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+327,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+328,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+329,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+330,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+331,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+332,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+333,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+334,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+335,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+336,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+337,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+338,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+339,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+340,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 3U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+341,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+342,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+343,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+344,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+345,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+346,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+347,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+348,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+349,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+350,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+351,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__3__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+352,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [4U]),17);
        bufp->chgSData(oldp+353,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [4U]),9);
        bufp->chgBit(oldp+354,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 4U))));
        bufp->chgIData(oldp+355,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [4U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+356,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+357,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [4U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+358,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+359,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+360,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [4U] >> 7U))),9);
        bufp->chgBit(oldp+361,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [4U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 4U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [4U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+362,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 4U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+363,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [4U] >> 7U))),8);
        bufp->chgCData(oldp+364,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+365,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [4U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+366,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+367,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+368,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+369,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+370,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+371,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+372,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+373,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+374,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+375,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+376,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+377,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+378,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+379,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+380,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+381,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+382,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+383,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+384,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+385,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+386,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+387,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+388,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+389,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+390,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+391,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+392,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+393,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+394,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+395,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+396,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+397,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+398,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+399,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+400,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+401,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+402,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+403,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+404,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+405,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+406,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 4U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+407,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+408,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+409,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+410,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+411,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+412,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+413,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+414,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+415,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+416,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+417,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__4__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+418,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [5U]),17);
        bufp->chgSData(oldp+419,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [5U]),9);
        bufp->chgBit(oldp+420,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 5U))));
        bufp->chgIData(oldp+421,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [5U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+422,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+423,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [5U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+424,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+425,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+426,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [5U] >> 7U))),9);
        bufp->chgBit(oldp+427,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [5U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 5U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [5U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+428,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 5U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+429,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [5U] >> 7U))),8);
        bufp->chgCData(oldp+430,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+431,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [5U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+432,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+433,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+434,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+435,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+436,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+437,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+438,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+439,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+440,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+441,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+442,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+443,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+444,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+445,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+446,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+447,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+448,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+449,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+450,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+451,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+452,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+453,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+454,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+455,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+456,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+457,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+458,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+459,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+460,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+461,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+462,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+463,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+464,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+465,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+466,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+467,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+468,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+469,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+470,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+471,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+472,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 5U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+473,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+474,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+475,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+476,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+477,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+478,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+479,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+480,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+481,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+482,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+483,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__5__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+484,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [6U]),17);
        bufp->chgSData(oldp+485,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [6U]),9);
        bufp->chgBit(oldp+486,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 6U))));
        bufp->chgIData(oldp+487,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [6U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+488,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+489,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [6U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+490,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+491,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+492,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [6U] >> 7U))),9);
        bufp->chgBit(oldp+493,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [6U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 6U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [6U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+494,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 6U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+495,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [6U] >> 7U))),8);
        bufp->chgCData(oldp+496,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+497,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [6U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+498,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+499,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+500,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+501,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+502,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+503,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+504,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+505,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+506,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+507,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+508,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+509,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+510,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+511,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+512,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+513,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+514,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+515,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+516,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+517,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+518,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+519,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+520,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+521,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+522,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+523,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+524,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+525,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+526,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+527,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+528,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+529,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+530,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+531,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+532,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+533,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+534,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+535,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+536,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+537,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+538,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 6U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+539,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+540,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+541,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+542,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+543,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+544,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+545,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+546,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+547,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+548,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+549,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__6__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
        bufp->chgIData(oldp+550,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                 [7U]),17);
        bufp->chgSData(oldp+551,(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__b_ff
                                 [7U]),9);
        bufp->chgBit(oldp+552,((1U & ((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                      >> 7U))));
        bufp->chgIData(oldp+553,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                   << 8U) | ((0xfeU 
                                              & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                 [7U] 
                                                 << 1U)) 
                                             | (1U 
                                                & (~ 
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                                    >> 8U)))))),17);
        bufp->chgBit(oldp+554,((1U & (~ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part) 
                                         >> 8U)))));
        bufp->chgIData(oldp+555,((0x1ffffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                              [7U] 
                                              << 1U))),17);
        bufp->chgSData(oldp+556,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final),9);
        bufp->chgSData(oldp+557,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__r_part),9);
        bufp->chgSData(oldp+558,((0x1ffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                            [7U] >> 7U))),9);
        bufp->chgBit(oldp+559,((1U & ((IData)((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                                >> 8U) 
                                               & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                  [7U] 
                                                  >> 0xfU))) 
                                      | (((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                              >> 7U) 
                                             | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                 >> 6U) 
                                                & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                   >> 7U))) 
                                            | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                               & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                           | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                              & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                          | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                              >> 7U) 
                                             & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))) 
                                         & ((1U & (
                                                   vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                   [7U] 
                                                   >> 0xfU)) 
                                            ^ ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final) 
                                               >> 8U)))))));
        bufp->chgBit(oldp+560,((1U & ((((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                          >> 7U) | 
                                         (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                           >> 6U) & 
                                          ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                           >> 7U))) 
                                        | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                           & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))) 
                                      | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                          >> 7U) & 
                                         ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82)))))));
        bufp->chgCData(oldp+561,((0xffU & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                           [7U] >> 7U))),8);
        bufp->chgCData(oldp+562,((0xffU & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final))),8);
        bufp->chgCData(oldp+563,((0xffU & (((0x1ffU 
                                             & (vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__rq_part_ff
                                                [7U] 
                                                >> 7U)) 
                                            ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__b_final)) 
                                           ^ (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry)))),8);
        bufp->chgCData(oldp+564,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__bitwise_carry),8);
        bufp->chgCData(oldp+565,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g),8);
        bufp->chgCData(oldp+566,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p),8);
        bufp->chgBit(oldp+567,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11));
        bufp->chgBit(oldp+568,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_20_21));
        bufp->chgBit(oldp+569,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_20_21));
        bufp->chgBit(oldp+570,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31));
        bufp->chgBit(oldp+571,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31));
        bufp->chgBit(oldp+572,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41));
        bufp->chgBit(oldp+573,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41));
        bufp->chgBit(oldp+574,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51));
        bufp->chgBit(oldp+575,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51));
        bufp->chgBit(oldp+576,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61));
        bufp->chgBit(oldp+577,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61));
        bufp->chgBit(oldp+578,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 5U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 6U))))));
        bufp->chgBit(oldp+579,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71));
        bufp->chgBit(oldp+580,((1U & (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                       >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                  >> 6U) 
                                                 & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                    >> 7U))))));
        bufp->chgBit(oldp+581,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81));
        bufp->chgBit(oldp+582,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42));
        bufp->chgBit(oldp+583,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42));
        bufp->chgBit(oldp+584,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51)))));
        bufp->chgBit(oldp+585,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))));
        bufp->chgBit(oldp+586,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current));
        bufp->chgBit(oldp+587,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                   & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61)))));
        bufp->chgBit(oldp+588,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))));
        bufp->chgBit(oldp+589,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current));
        bufp->chgBit(oldp+590,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 6U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 5U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 6U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))))));
        bufp->chgBit(oldp+591,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))));
        bufp->chgBit(oldp+592,((1U & ((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                        >> 7U) | (((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                   >> 6U) 
                                                  & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                     >> 7U))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))))));
        bufp->chgBit(oldp+593,(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82));
        bufp->chgBit(oldp+594,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 7U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 6U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 7U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_80_81))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                         & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))))));
        bufp->chgBit(oldp+595,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42) 
                                & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_81_82))));
        bufp->chgBit(oldp+596,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g))));
        bufp->chgBit(oldp+597,((1U & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p))));
        bufp->chgBit(oldp+598,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 1U))));
        bufp->chgBit(oldp+599,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 1U))));
        bufp->chgBit(oldp+600,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 2U))));
        bufp->chgBit(oldp+601,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 2U))));
        bufp->chgBit(oldp+602,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 3U))));
        bufp->chgBit(oldp+603,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 3U))));
        bufp->chgBit(oldp+604,(((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_41_42) 
                                | (((IData)(vlSelf->div_8_pipe__DOT__nrdiv_inst__DOT__nxt_add_sub_ff) 
                                    >> 7U) & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_41_42)))));
        bufp->chgBit(oldp+605,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 4U))));
        bufp->chgBit(oldp+606,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 4U))));
        bufp->chgBit(oldp+607,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_30_31) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_10_11) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_30_31) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51))))));
        bufp->chgBit(oldp+608,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 5U))));
        bufp->chgBit(oldp+609,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 5U))));
        bufp->chgBit(oldp+610,((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_60_61) 
                                 | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_40_41) 
                                    & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))) 
                                | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_21__g_combined_current) 
                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_40_41) 
                                      & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_60_61))))));
        bufp->chgBit(oldp+611,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 6U))));
        bufp->chgBit(oldp+612,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 6U))));
        bufp->chgBit(oldp+613,((1U & (((((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                         >> 6U) | (
                                                   ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                                    >> 5U) 
                                                   & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                                      >> 6U))) 
                                       | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g_50_51) 
                                          & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71))) 
                                      | ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT____Vcellout__black_cell_31__g_combined_current) 
                                         & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_50_51) 
                                            & (IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p_70_71)))))));
        bufp->chgBit(oldp+614,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__g) 
                                      >> 7U))));
        bufp->chgBit(oldp+615,((1U & ((IData)(vlSymsp->TOP__div_8_pipe__DOT__nrdiv_inst__DOT__gen_loop__BRA__7__KET____DOT__nrdiv_step_gen.__PVT__add_9_inst__DOT__add_8_inst__DOT__carry_8_0__DOT__p) 
                                      >> 7U))));
    }
    bufp->chgBit(oldp+616,(vlSelf->clk));
    bufp->chgBit(oldp+617,(vlSelf->nrst));
    bufp->chgCData(oldp+618,(vlSelf->dnd),8);
    bufp->chgCData(oldp+619,(vlSelf->div),8);
    bufp->chgBit(oldp+620,(vlSelf->sgn));
    bufp->chgCData(oldp+621,(vlSelf->q),8);
    bufp->chgCData(oldp+622,(vlSelf->r),8);
    bufp->chgCData(oldp+623,((0xffU & ((IData)(1U) 
                                       + (~ (IData)(vlSelf->dnd))))),8);
    bufp->chgCData(oldp+624,((0xffU & ((IData)(1U) 
                                       + (~ (IData)(vlSelf->div))))),8);
    bufp->chgIData(oldp+625,(vlSelf->div_8_pipe__DOT__dnd_eff),17);
    bufp->chgSData(oldp+626,(vlSelf->div_8_pipe__DOT__div_eff),9);
}

void div_8_pipe___024root__trace_cleanup(void* voidSelf, VerilatedVcd* /*unused*/) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    div_8_pipe___024root__trace_cleanup\n"); );
    // Init
    div_8_pipe___024root* const __restrict vlSelf VL_ATTR_UNUSED = static_cast<div_8_pipe___024root*>(voidSelf);
    div_8_pipe__Syms* const __restrict vlSymsp VL_ATTR_UNUSED = vlSelf->vlSymsp;
    // Body
    vlSymsp->__Vm_activity = false;
    vlSymsp->TOP.__Vm_traceActivity[0U] = 0U;
    vlSymsp->TOP.__Vm_traceActivity[1U] = 0U;
}
