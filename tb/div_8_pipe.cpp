#include <iostream>

#include <cstdlib>
#include <ctime>
#include <verilated.h>
#include <verilated_vcd_c.h>
#include "../inc_dir/div_8_pipe.h"

#define SIM_STEP 		20000
#define CLK_STEP 		SIM_STEP / 2
#define SIM_TIME_MAX 	SIM_STEP * 17

int main (int argc, char ** argv, char ** env)
{
	vluint64_t sim_time = 0;
	
	// Configure RNG for DUT's inputs.
	srand((unsigned) time(0));
	
	// Instantiate module classes.
	div_8_pipe * dut = new div_8_pipe;
	
	// Configure waveform tracing.
	Verilated::traceEverOn(true);
	VerilatedVcdC * dut_trace = new VerilatedVcdC;
	
	dut 		-> trace (dut_trace, 5);
	dut_trace 	-> open ("div_8_pipe.vcd");
	
	// Initialize inputs.
	dut -> clk 	= 0;
	dut -> nrst = 1;
	
	// Main test loop.
	while (sim_time < SIM_TIME_MAX)
	{
		// Deal with dut's reset.
		if (sim_time == CLK_STEP)
			dut -> nrst = 0;
		
		if (sim_time == CLK_STEP * 2)
		{
			dut -> nrst = 1;
			
			dut -> dnd 	= -125;
			dut -> div 	= -73;
			dut -> sgn 	= 1;
		}
			
		//if ((sim_time > 0) && (sim_time % SIM_STEP) == 0)
		//{
		//	dut -> a 	= (rand() % 255) + 1;
		//	dut -> b 	= (rand() % 255) + 1;
		//}
	
		// Deal with dut's clock and inputs.
		if (sim_time % (CLK_STEP + 1) == 0)
			dut -> clk = !(dut -> clk);
			
		// Feed the dut and save the results.
		dut 		-> eval ();
		dut_trace 	-> dump (sim_time);
		sim_time++;
	}
	
	// Clean-up and close simulation.
	dut_trace -> close ();
	delete dut;
	exit (EXIT_SUCCESS);		
}
